package com.colouredglaze.entity;
import java.io.Serializable;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.Map;
import java.util.List;
/**
*
*  @author author
*/
public class TbOrder implements Serializable {

    private static final long serialVersionUID = 1574592465108L;


    /**
    * 主键
    * 记录编号
    * isNullAble:0
    */
    private Integer rid;

    /**
    * 账号
    * isNullAble:0
    */
    private String uid;

    /**
    * 订单编号
    * isNullAble:0
    */
    private String oid;

    /**
    * 订单状态
    * isNullAble:0
    */
    private String statu;

    /**
    * 收货地址编号
    * isNullAble:1
    */
    private Integer addressId;

    /**
    * 支付金额
    * isNullAble:1
    */
    private Double payment;

    /**
    * 下单时间
    * isNullAble:1
    */
    private java.time.LocalDateTime placed;

    /**
    * 发货时间
    * isNullAble:1
    */
    private java.time.LocalDateTime deliver;

    /**
    * 送达时间
    * isNullAble:1
    */
    private java.time.LocalDateTime handover;

    /**
     *外键约束，地址
     */
    private TbAddress tbAddress;

    public TbAddress getTbAddress() {
        return tbAddress;
    }

    public void setTbAddress(TbAddress tbAddress) {
        this.tbAddress = tbAddress;
    }

    public void setRid(Integer rid){this.rid = rid;}

    public Integer getRid(){return this.rid;}

    public void setUid(String uid){this.uid = uid;}

    public String getUid(){return this.uid;}

    public void setOid(String oid){this.oid = oid;}

    public String getOid(){return this.oid;}

    public void setStatu(String statu){this.statu = statu;}

    public String getStatu(){return this.statu;}

    public void setAddressId(Integer addressId){this.addressId = addressId;}

    public Integer getAddressId(){return this.addressId;}

    public void setPayment(Double payment){this.payment = payment;}

    public Double getPayment(){return this.payment;}

    public void setPlaced(java.time.LocalDateTime placed){this.placed = placed;}

    public java.time.LocalDateTime getPlaced(){return this.placed;}

    public void setDeliver(java.time.LocalDateTime deliver){this.deliver = deliver;}

    public java.time.LocalDateTime getDeliver(){return this.deliver;}

    public void setHandover(java.time.LocalDateTime handover){this.handover = handover;}

    public java.time.LocalDateTime getHandover(){return this.handover;}

    @Override
    public String toString() {
        return "TbOrder{" +
                "rid=" + rid +
                ", uid='" + uid + '\'' +
                ", oid='" + oid + '\'' +
                ", statu='" + statu + '\'' +
                ", addressId=" + addressId +
                ", payment=" + payment +
                ", placed=" + placed +
                ", deliver=" + deliver +
                ", handover=" + handover +
                ", tbAddress=" + tbAddress +
                '}';
    }

    public static Builder Build(){return new Builder();}

    public static ConditionBuilder ConditionBuild(){return new ConditionBuilder();}

    public static UpdateBuilder UpdateBuild(){return new UpdateBuilder();}

    public static QueryBuilder QueryBuild(){return new QueryBuilder();}

    public static class UpdateBuilder {

        private TbOrder set;

        private ConditionBuilder where;

        public UpdateBuilder set(TbOrder set){
            this.set = set;
            return this;
        }

        public TbOrder getSet(){
            return this.set;
        }

        public UpdateBuilder where(ConditionBuilder where){
            this.where = where;
            return this;
        }

        public ConditionBuilder getWhere(){
            return this.where;
        }

        public UpdateBuilder build(){
            return this;
        }
    }

    public static class QueryBuilder extends TbOrder{
        /**
        * 需要返回的列
        */
        private Map<String,Object> fetchFields;

        public Map<String,Object> getFetchFields(){return this.fetchFields;}

        private List<Integer> ridList;

        public List<Integer> getRidList(){return this.ridList;}

        private Integer ridSt;

        private Integer ridEd;

        public Integer getRidSt(){return this.ridSt;}

        public Integer getRidEd(){return this.ridEd;}

        private List<String> uidList;

        public List<String> getUidList(){return this.uidList;}


        private List<String> fuzzyUid;

        public List<String> getFuzzyUid(){return this.fuzzyUid;}

        private List<String> rightFuzzyUid;

        public List<String> getRightFuzzyUid(){return this.rightFuzzyUid;}
        private List<String> oidList;

        public List<String> getOidList(){return this.oidList;}


        private List<String> fuzzyOid;

        public List<String> getFuzzyOid(){return this.fuzzyOid;}

        private List<String> rightFuzzyOid;

        public List<String> getRightFuzzyOid(){return this.rightFuzzyOid;}
        private List<String> statuList;

        public List<String> getStatuList(){return this.statuList;}


        private List<String> fuzzyStatu;

        public List<String> getFuzzyStatu(){return this.fuzzyStatu;}

        private List<String> rightFuzzyStatu;

        public List<String> getRightFuzzyStatu(){return this.rightFuzzyStatu;}
        private List<Integer> addressIdList;

        public List<Integer> getAddressIdList(){return this.addressIdList;}

        private Integer addressIdSt;

        private Integer addressIdEd;

        public Integer getAddressIdSt(){return this.addressIdSt;}

        public Integer getAddressIdEd(){return this.addressIdEd;}

        private List<Double> paymentList;

        public List<Double> getPaymentList(){return this.paymentList;}

        private Double paymentSt;

        private Double paymentEd;

        public Double getPaymentSt(){return this.paymentSt;}

        public Double getPaymentEd(){return this.paymentEd;}

        private List<java.time.LocalDateTime> placedList;

        public List<java.time.LocalDateTime> getPlacedList(){return this.placedList;}

        private java.time.LocalDateTime placedSt;

        private java.time.LocalDateTime placedEd;

        public java.time.LocalDateTime getPlacedSt(){return this.placedSt;}

        public java.time.LocalDateTime getPlacedEd(){return this.placedEd;}

        private List<java.time.LocalDateTime> deliverList;

        public List<java.time.LocalDateTime> getDeliverList(){return this.deliverList;}

        private java.time.LocalDateTime deliverSt;

        private java.time.LocalDateTime deliverEd;

        public java.time.LocalDateTime getDeliverSt(){return this.deliverSt;}

        public java.time.LocalDateTime getDeliverEd(){return this.deliverEd;}

        private List<java.time.LocalDateTime> handoverList;

        public List<java.time.LocalDateTime> getHandoverList(){return this.handoverList;}

        private java.time.LocalDateTime handoverSt;

        private java.time.LocalDateTime handoverEd;

        public java.time.LocalDateTime getHandoverSt(){return this.handoverSt;}

        public java.time.LocalDateTime getHandoverEd(){return this.handoverEd;}

        private QueryBuilder (){
            this.fetchFields = new HashMap<>();
        }

        public QueryBuilder ridBetWeen(Integer ridSt,Integer ridEd){
            this.ridSt = ridSt;
            this.ridEd = ridEd;
            return this;
        }

        public QueryBuilder ridGreaterEqThan(Integer ridSt){
            this.ridSt = ridSt;
            return this;
        }
        public QueryBuilder ridLessEqThan(Integer ridEd){
            this.ridEd = ridEd;
            return this;
        }


        public QueryBuilder rid(Integer rid){
            setRid(rid);
            return this;
        }

        public QueryBuilder ridList(Integer ... rid){
            this.ridList = solveNullList(rid);
            return this;
        }

        public QueryBuilder ridList(List<Integer> rid){
            this.ridList = rid;
            return this;
        }

        public QueryBuilder fetchRid(){
            setFetchFields("fetchFields","rid");
            return this;
        }

        public QueryBuilder excludeRid(){
            setFetchFields("excludeFields","rid");
            return this;
        }

        public QueryBuilder fuzzyUid (List<String> fuzzyUid){
            this.fuzzyUid = fuzzyUid;
            return this;
        }

        public QueryBuilder fuzzyUid (String ... fuzzyUid){
            this.fuzzyUid = solveNullList(fuzzyUid);
            return this;
        }

        public QueryBuilder rightFuzzyUid (List<String> rightFuzzyUid){
            this.rightFuzzyUid = rightFuzzyUid;
            return this;
        }

        public QueryBuilder rightFuzzyUid (String ... rightFuzzyUid){
            this.rightFuzzyUid = solveNullList(rightFuzzyUid);
            return this;
        }

        public QueryBuilder uid(String uid){
            setUid(uid);
            return this;
        }

        public QueryBuilder uidList(String ... uid){
            this.uidList = solveNullList(uid);
            return this;
        }

        public QueryBuilder uidList(List<String> uid){
            this.uidList = uid;
            return this;
        }

        public QueryBuilder fetchUid(){
            setFetchFields("fetchFields","uid");
            return this;
        }

        public QueryBuilder excludeUid(){
            setFetchFields("excludeFields","uid");
            return this;
        }

        public QueryBuilder fuzzyOid (List<String> fuzzyOid){
            this.fuzzyOid = fuzzyOid;
            return this;
        }

        public QueryBuilder fuzzyOid (String ... fuzzyOid){
            this.fuzzyOid = solveNullList(fuzzyOid);
            return this;
        }

        public QueryBuilder rightFuzzyOid (List<String> rightFuzzyOid){
            this.rightFuzzyOid = rightFuzzyOid;
            return this;
        }

        public QueryBuilder rightFuzzyOid (String ... rightFuzzyOid){
            this.rightFuzzyOid = solveNullList(rightFuzzyOid);
            return this;
        }

        public QueryBuilder oid(String oid){
            setOid(oid);
            return this;
        }

        public QueryBuilder oidList(String ... oid){
            this.oidList = solveNullList(oid);
            return this;
        }

        public QueryBuilder oidList(List<String> oid){
            this.oidList = oid;
            return this;
        }

        public QueryBuilder fetchOid(){
            setFetchFields("fetchFields","oid");
            return this;
        }

        public QueryBuilder excludeOid(){
            setFetchFields("excludeFields","oid");
            return this;
        }

        public QueryBuilder fuzzyStatu (List<String> fuzzyStatu){
            this.fuzzyStatu = fuzzyStatu;
            return this;
        }

        public QueryBuilder fuzzyStatu (String ... fuzzyStatu){
            this.fuzzyStatu = solveNullList(fuzzyStatu);
            return this;
        }

        public QueryBuilder rightFuzzyStatu (List<String> rightFuzzyStatu){
            this.rightFuzzyStatu = rightFuzzyStatu;
            return this;
        }

        public QueryBuilder rightFuzzyStatu (String ... rightFuzzyStatu){
            this.rightFuzzyStatu = solveNullList(rightFuzzyStatu);
            return this;
        }

        public QueryBuilder statu(String statu){
            setStatu(statu);
            return this;
        }

        public QueryBuilder statuList(String ... statu){
            this.statuList = solveNullList(statu);
            return this;
        }

        public QueryBuilder statuList(List<String> statu){
            this.statuList = statu;
            return this;
        }

        public QueryBuilder fetchStatu(){
            setFetchFields("fetchFields","statu");
            return this;
        }

        public QueryBuilder excludeStatu(){
            setFetchFields("excludeFields","statu");
            return this;
        }

        public QueryBuilder addressIdBetWeen(Integer addressIdSt,Integer addressIdEd){
            this.addressIdSt = addressIdSt;
            this.addressIdEd = addressIdEd;
            return this;
        }

        public QueryBuilder addressIdGreaterEqThan(Integer addressIdSt){
            this.addressIdSt = addressIdSt;
            return this;
        }
        public QueryBuilder addressIdLessEqThan(Integer addressIdEd){
            this.addressIdEd = addressIdEd;
            return this;
        }


        public QueryBuilder addressId(Integer addressId){
            setAddressId(addressId);
            return this;
        }

        public QueryBuilder addressIdList(Integer ... addressId){
            this.addressIdList = solveNullList(addressId);
            return this;
        }

        public QueryBuilder addressIdList(List<Integer> addressId){
            this.addressIdList = addressId;
            return this;
        }

        public QueryBuilder fetchAddressId(){
            setFetchFields("fetchFields","addressId");
            return this;
        }

        public QueryBuilder excludeAddressId(){
            setFetchFields("excludeFields","addressId");
            return this;
        }

        public QueryBuilder paymentBetWeen(Double paymentSt,Double paymentEd){
            this.paymentSt = paymentSt;
            this.paymentEd = paymentEd;
            return this;
        }

        public QueryBuilder paymentGreaterEqThan(Double paymentSt){
            this.paymentSt = paymentSt;
            return this;
        }
        public QueryBuilder paymentLessEqThan(Double paymentEd){
            this.paymentEd = paymentEd;
            return this;
        }


        public QueryBuilder payment(Double payment){
            setPayment(payment);
            return this;
        }

        public QueryBuilder paymentList(Double ... payment){
            this.paymentList = solveNullList(payment);
            return this;
        }

        public QueryBuilder paymentList(List<Double> payment){
            this.paymentList = payment;
            return this;
        }

        public QueryBuilder fetchPayment(){
            setFetchFields("fetchFields","payment");
            return this;
        }

        public QueryBuilder excludePayment(){
            setFetchFields("excludeFields","payment");
            return this;
        }

        public QueryBuilder placedBetWeen(java.time.LocalDateTime placedSt,java.time.LocalDateTime placedEd){
            this.placedSt = placedSt;
            this.placedEd = placedEd;
            return this;
        }

        public QueryBuilder placedGreaterEqThan(java.time.LocalDateTime placedSt){
            this.placedSt = placedSt;
            return this;
        }
        public QueryBuilder placedLessEqThan(java.time.LocalDateTime placedEd){
            this.placedEd = placedEd;
            return this;
        }


        public QueryBuilder placed(java.time.LocalDateTime placed){
            setPlaced(placed);
            return this;
        }

        public QueryBuilder placedList(java.time.LocalDateTime ... placed){
            this.placedList = solveNullList(placed);
            return this;
        }

        public QueryBuilder placedList(List<java.time.LocalDateTime> placed){
            this.placedList = placed;
            return this;
        }

        public QueryBuilder fetchPlaced(){
            setFetchFields("fetchFields","placed");
            return this;
        }

        public QueryBuilder excludePlaced(){
            setFetchFields("excludeFields","placed");
            return this;
        }

        public QueryBuilder deliverBetWeen(java.time.LocalDateTime deliverSt,java.time.LocalDateTime deliverEd){
            this.deliverSt = deliverSt;
            this.deliverEd = deliverEd;
            return this;
        }

        public QueryBuilder deliverGreaterEqThan(java.time.LocalDateTime deliverSt){
            this.deliverSt = deliverSt;
            return this;
        }
        public QueryBuilder deliverLessEqThan(java.time.LocalDateTime deliverEd){
            this.deliverEd = deliverEd;
            return this;
        }


        public QueryBuilder deliver(java.time.LocalDateTime deliver){
            setDeliver(deliver);
            return this;
        }

        public QueryBuilder deliverList(java.time.LocalDateTime ... deliver){
            this.deliverList = solveNullList(deliver);
            return this;
        }

        public QueryBuilder deliverList(List<java.time.LocalDateTime> deliver){
            this.deliverList = deliver;
            return this;
        }

        public QueryBuilder fetchDeliver(){
            setFetchFields("fetchFields","deliver");
            return this;
        }

        public QueryBuilder excludeDeliver(){
            setFetchFields("excludeFields","deliver");
            return this;
        }

        public QueryBuilder handoverBetWeen(java.time.LocalDateTime handoverSt,java.time.LocalDateTime handoverEd){
            this.handoverSt = handoverSt;
            this.handoverEd = handoverEd;
            return this;
        }

        public QueryBuilder handoverGreaterEqThan(java.time.LocalDateTime handoverSt){
            this.handoverSt = handoverSt;
            return this;
        }
        public QueryBuilder handoverLessEqThan(java.time.LocalDateTime handoverEd){
            this.handoverEd = handoverEd;
            return this;
        }


        public QueryBuilder handover(java.time.LocalDateTime handover){
            setHandover(handover);
            return this;
        }

        public QueryBuilder handoverList(java.time.LocalDateTime ... handover){
            this.handoverList = solveNullList(handover);
            return this;
        }

        public QueryBuilder handoverList(List<java.time.LocalDateTime> handover){
            this.handoverList = handover;
            return this;
        }

        public QueryBuilder fetchHandover(){
            setFetchFields("fetchFields","handover");
            return this;
        }

        public QueryBuilder excludeHandover(){
            setFetchFields("excludeFields","handover");
            return this;
        }
        private <T>List<T> solveNullList(T ... objs){
            if (objs != null){
            List<T> list = new ArrayList<>();
                for (T item : objs){
                    if (item != null){
                        list.add(item);
                    }
                }
                return list;
            }
            return null;
        }

        public QueryBuilder fetchAll(){
            this.fetchFields.put("AllFields",true);
            return this;
        }

        public QueryBuilder addField(String ... fields){
            List<String> list = new ArrayList<>();
            if (fields != null){
                for (String field : fields){
                    list.add(field);
                }
            }
            this.fetchFields.put("otherFields",list);
            return this;
        }
        @SuppressWarnings("unchecked")
        private void setFetchFields(String key,String val){
            Map<String,Boolean> fields= (Map<String, Boolean>) this.fetchFields.get(key);
            if (fields == null){
                fields = new HashMap<>();
            }
            fields.put(val,true);
            this.fetchFields.put(key,fields);
        }

        public TbOrder build(){return this;}
    }


    public static class ConditionBuilder{
        private List<Integer> ridList;

        public List<Integer> getRidList(){return this.ridList;}

        private Integer ridSt;

        private Integer ridEd;

        public Integer getRidSt(){return this.ridSt;}

        public Integer getRidEd(){return this.ridEd;}

        private List<String> uidList;

        public List<String> getUidList(){return this.uidList;}


        private List<String> fuzzyUid;

        public List<String> getFuzzyUid(){return this.fuzzyUid;}

        private List<String> rightFuzzyUid;

        public List<String> getRightFuzzyUid(){return this.rightFuzzyUid;}
        private List<String> oidList;

        public List<String> getOidList(){return this.oidList;}


        private List<String> fuzzyOid;

        public List<String> getFuzzyOid(){return this.fuzzyOid;}

        private List<String> rightFuzzyOid;

        public List<String> getRightFuzzyOid(){return this.rightFuzzyOid;}
        private List<String> statuList;

        public List<String> getStatuList(){return this.statuList;}


        private List<String> fuzzyStatu;

        public List<String> getFuzzyStatu(){return this.fuzzyStatu;}

        private List<String> rightFuzzyStatu;

        public List<String> getRightFuzzyStatu(){return this.rightFuzzyStatu;}
        private List<Integer> addressIdList;

        public List<Integer> getAddressIdList(){return this.addressIdList;}

        private Integer addressIdSt;

        private Integer addressIdEd;

        public Integer getAddressIdSt(){return this.addressIdSt;}

        public Integer getAddressIdEd(){return this.addressIdEd;}

        private List<Double> paymentList;

        public List<Double> getPaymentList(){return this.paymentList;}

        private Double paymentSt;

        private Double paymentEd;

        public Double getPaymentSt(){return this.paymentSt;}

        public Double getPaymentEd(){return this.paymentEd;}

        private List<java.time.LocalDateTime> placedList;

        public List<java.time.LocalDateTime> getPlacedList(){return this.placedList;}

        private java.time.LocalDateTime placedSt;

        private java.time.LocalDateTime placedEd;

        public java.time.LocalDateTime getPlacedSt(){return this.placedSt;}

        public java.time.LocalDateTime getPlacedEd(){return this.placedEd;}

        private List<java.time.LocalDateTime> deliverList;

        public List<java.time.LocalDateTime> getDeliverList(){return this.deliverList;}

        private java.time.LocalDateTime deliverSt;

        private java.time.LocalDateTime deliverEd;

        public java.time.LocalDateTime getDeliverSt(){return this.deliverSt;}

        public java.time.LocalDateTime getDeliverEd(){return this.deliverEd;}

        private List<java.time.LocalDateTime> handoverList;

        public List<java.time.LocalDateTime> getHandoverList(){return this.handoverList;}

        private java.time.LocalDateTime handoverSt;

        private java.time.LocalDateTime handoverEd;

        public java.time.LocalDateTime getHandoverSt(){return this.handoverSt;}

        public java.time.LocalDateTime getHandoverEd(){return this.handoverEd;}


        public ConditionBuilder ridBetWeen(Integer ridSt,Integer ridEd){
            this.ridSt = ridSt;
            this.ridEd = ridEd;
            return this;
        }

        public ConditionBuilder ridGreaterEqThan(Integer ridSt){
            this.ridSt = ridSt;
            return this;
        }
        public ConditionBuilder ridLessEqThan(Integer ridEd){
            this.ridEd = ridEd;
            return this;
        }


        public ConditionBuilder ridList(Integer ... rid){
            this.ridList = solveNullList(rid);
            return this;
        }

        public ConditionBuilder ridList(List<Integer> rid){
            this.ridList = rid;
            return this;
        }

        public ConditionBuilder fuzzyUid (List<String> fuzzyUid){
            this.fuzzyUid = fuzzyUid;
            return this;
        }

        public ConditionBuilder fuzzyUid (String ... fuzzyUid){
            this.fuzzyUid = solveNullList(fuzzyUid);
            return this;
        }

        public ConditionBuilder rightFuzzyUid (List<String> rightFuzzyUid){
            this.rightFuzzyUid = rightFuzzyUid;
            return this;
        }

        public ConditionBuilder rightFuzzyUid (String ... rightFuzzyUid){
            this.rightFuzzyUid = solveNullList(rightFuzzyUid);
            return this;
        }

        public ConditionBuilder uidList(String ... uid){
            this.uidList = solveNullList(uid);
            return this;
        }

        public ConditionBuilder uidList(List<String> uid){
            this.uidList = uid;
            return this;
        }

        public ConditionBuilder fuzzyOid (List<String> fuzzyOid){
            this.fuzzyOid = fuzzyOid;
            return this;
        }

        public ConditionBuilder fuzzyOid (String ... fuzzyOid){
            this.fuzzyOid = solveNullList(fuzzyOid);
            return this;
        }

        public ConditionBuilder rightFuzzyOid (List<String> rightFuzzyOid){
            this.rightFuzzyOid = rightFuzzyOid;
            return this;
        }

        public ConditionBuilder rightFuzzyOid (String ... rightFuzzyOid){
            this.rightFuzzyOid = solveNullList(rightFuzzyOid);
            return this;
        }

        public ConditionBuilder oidList(String ... oid){
            this.oidList = solveNullList(oid);
            return this;
        }

        public ConditionBuilder oidList(List<String> oid){
            this.oidList = oid;
            return this;
        }

        public ConditionBuilder fuzzyStatu (List<String> fuzzyStatu){
            this.fuzzyStatu = fuzzyStatu;
            return this;
        }

        public ConditionBuilder fuzzyStatu (String ... fuzzyStatu){
            this.fuzzyStatu = solveNullList(fuzzyStatu);
            return this;
        }

        public ConditionBuilder rightFuzzyStatu (List<String> rightFuzzyStatu){
            this.rightFuzzyStatu = rightFuzzyStatu;
            return this;
        }

        public ConditionBuilder rightFuzzyStatu (String ... rightFuzzyStatu){
            this.rightFuzzyStatu = solveNullList(rightFuzzyStatu);
            return this;
        }

        public ConditionBuilder statuList(String ... statu){
            this.statuList = solveNullList(statu);
            return this;
        }

        public ConditionBuilder statuList(List<String> statu){
            this.statuList = statu;
            return this;
        }

        public ConditionBuilder addressIdBetWeen(Integer addressIdSt,Integer addressIdEd){
            this.addressIdSt = addressIdSt;
            this.addressIdEd = addressIdEd;
            return this;
        }

        public ConditionBuilder addressIdGreaterEqThan(Integer addressIdSt){
            this.addressIdSt = addressIdSt;
            return this;
        }
        public ConditionBuilder addressIdLessEqThan(Integer addressIdEd){
            this.addressIdEd = addressIdEd;
            return this;
        }


        public ConditionBuilder addressIdList(Integer ... addressId){
            this.addressIdList = solveNullList(addressId);
            return this;
        }

        public ConditionBuilder addressIdList(List<Integer> addressId){
            this.addressIdList = addressId;
            return this;
        }

        public ConditionBuilder paymentBetWeen(Double paymentSt,Double paymentEd){
            this.paymentSt = paymentSt;
            this.paymentEd = paymentEd;
            return this;
        }

        public ConditionBuilder paymentGreaterEqThan(Double paymentSt){
            this.paymentSt = paymentSt;
            return this;
        }
        public ConditionBuilder paymentLessEqThan(Double paymentEd){
            this.paymentEd = paymentEd;
            return this;
        }


        public ConditionBuilder paymentList(Double ... payment){
            this.paymentList = solveNullList(payment);
            return this;
        }

        public ConditionBuilder paymentList(List<Double> payment){
            this.paymentList = payment;
            return this;
        }

        public ConditionBuilder placedBetWeen(java.time.LocalDateTime placedSt,java.time.LocalDateTime placedEd){
            this.placedSt = placedSt;
            this.placedEd = placedEd;
            return this;
        }

        public ConditionBuilder placedGreaterEqThan(java.time.LocalDateTime placedSt){
            this.placedSt = placedSt;
            return this;
        }
        public ConditionBuilder placedLessEqThan(java.time.LocalDateTime placedEd){
            this.placedEd = placedEd;
            return this;
        }


        public ConditionBuilder placedList(java.time.LocalDateTime ... placed){
            this.placedList = solveNullList(placed);
            return this;
        }

        public ConditionBuilder placedList(List<java.time.LocalDateTime> placed){
            this.placedList = placed;
            return this;
        }

        public ConditionBuilder deliverBetWeen(java.time.LocalDateTime deliverSt,java.time.LocalDateTime deliverEd){
            this.deliverSt = deliverSt;
            this.deliverEd = deliverEd;
            return this;
        }

        public ConditionBuilder deliverGreaterEqThan(java.time.LocalDateTime deliverSt){
            this.deliverSt = deliverSt;
            return this;
        }
        public ConditionBuilder deliverLessEqThan(java.time.LocalDateTime deliverEd){
            this.deliverEd = deliverEd;
            return this;
        }


        public ConditionBuilder deliverList(java.time.LocalDateTime ... deliver){
            this.deliverList = solveNullList(deliver);
            return this;
        }

        public ConditionBuilder deliverList(List<java.time.LocalDateTime> deliver){
            this.deliverList = deliver;
            return this;
        }

        public ConditionBuilder handoverBetWeen(java.time.LocalDateTime handoverSt,java.time.LocalDateTime handoverEd){
            this.handoverSt = handoverSt;
            this.handoverEd = handoverEd;
            return this;
        }

        public ConditionBuilder handoverGreaterEqThan(java.time.LocalDateTime handoverSt){
            this.handoverSt = handoverSt;
            return this;
        }
        public ConditionBuilder handoverLessEqThan(java.time.LocalDateTime handoverEd){
            this.handoverEd = handoverEd;
            return this;
        }


        public ConditionBuilder handoverList(java.time.LocalDateTime ... handover){
            this.handoverList = solveNullList(handover);
            return this;
        }

        public ConditionBuilder handoverList(List<java.time.LocalDateTime> handover){
            this.handoverList = handover;
            return this;
        }

        private <T>List<T> solveNullList(T ... objs){
            if (objs != null){
            List<T> list = new ArrayList<>();
                for (T item : objs){
                    if (item != null){
                        list.add(item);
                    }
                }
                return list;
            }
            return null;
        }

        public ConditionBuilder build(){return this;}
    }

    public static class Builder {

        private TbOrder obj;

        public Builder(){
            this.obj = new TbOrder();
        }

        public Builder rid(Integer rid){
            this.obj.setRid(rid);
            return this;
        }
        public Builder uid(String uid){
            this.obj.setUid(uid);
            return this;
        }
        public Builder oid(String oid){
            this.obj.setOid(oid);
            return this;
        }
        public Builder statu(String statu){
            this.obj.setStatu(statu);
            return this;
        }
        public Builder addressId(Integer addressId){
            this.obj.setAddressId(addressId);
            return this;
        }
        public Builder payment(Double payment){
            this.obj.setPayment(payment);
            return this;
        }
        public Builder placed(java.time.LocalDateTime placed){
            this.obj.setPlaced(placed);
            return this;
        }
        public Builder deliver(java.time.LocalDateTime deliver){
            this.obj.setDeliver(deliver);
            return this;
        }
        public Builder handover(java.time.LocalDateTime handover){
            this.obj.setHandover(handover);
            return this;
        }
        public TbOrder build(){return obj;}
    }

}
