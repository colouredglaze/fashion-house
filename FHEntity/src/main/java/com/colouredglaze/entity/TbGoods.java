package com.colouredglaze.entity;
import java.io.Serializable;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.Map;
import java.util.List;
/**
*
*  @author author
*/
public class TbGoods implements Serializable {

    private static final long serialVersionUID = 1574592461143L;


    /**
    * 主键
    * 商品编号
    * isNullAble:0
    */
    private String number;

    /**
    * 商品数量
    * isNullAble:0,defaultVal:0
    */
    private Integer count;

    /**
    * 商品价格
    * isNullAble:0
    */
    private Double price;

    /**
    * 商品名称
    * isNullAble:0
    */
    private String name;

    /**
    * 商品尺码
    * isNullAble:0
    */
    private List<String> size;

    /**
    * 品牌
    * isNullAble:0
    */
    private String brand;

    /**
    * 材质
    * isNullAble:0
    */
    private String texture;

    private String describe;

    @Override
    public String toString() {
        return "TbGoods{" +
                "number='" + number + '\'' +
                ", count=" + count +
                ", price=" + price +
                ", name='" + name + '\'' +
                ", size=" + size +
                ", brand='" + brand + '\'' +
                ", texture='" + texture + '\'' +
                ", describe='" + describe + '\'' +
                '}';
    }

    public String getDescribe() {
        return describe;
    }

    public void setDescribe(String describe) {
        this.describe = describe;
    }

    public void setNumber(String number){this.number = number;}

    public String getNumber(){return this.number;}

    public void setCount(Integer count){this.count = count;}

    public Integer getCount(){return this.count;}

    public void setPrice(Double price){this.price = price;}

    public Double getPrice(){return this.price;}

    public void setName(String name){this.name = name;}

    public String getName(){return this.name;}

    public void setSize(List<String> size){this.size = size;}

    public List<String> getSize(){return this.size;}

    public void setBrand(String brand){this.brand = brand;}

    public String getBrand(){return this.brand;}

    public void setTexture(String texture){this.texture = texture;}

    public String getTexture(){return this.texture;}

    public static Builder Build(){return new Builder();}

    public static ConditionBuilder ConditionBuild(){return new ConditionBuilder();}

    public static UpdateBuilder UpdateBuild(){return new UpdateBuilder();}

    public static QueryBuilder QueryBuild(){return new QueryBuilder();}

    public static class UpdateBuilder {

        private TbGoods set;

        private ConditionBuilder where;

        public UpdateBuilder set(TbGoods set){
            this.set = set;
            return this;
        }

        public TbGoods getSet(){
            return this.set;
        }

        public UpdateBuilder where(ConditionBuilder where){
            this.where = where;
            return this;
        }

        public ConditionBuilder getWhere(){
            return this.where;
        }

        public UpdateBuilder build(){
            return this;
        }
    }

    public static class QueryBuilder extends TbGoods{
        /**
        * 需要返回的列
        */
        private Map<String,Object> fetchFields;

        public Map<String,Object> getFetchFields(){return this.fetchFields;}

        private List<String> numberList;

        public List<String> getNumberList(){return this.numberList;}


        private List<String> fuzzyNumber;

        public List<String> getFuzzyNumber(){return this.fuzzyNumber;}

        private List<String> rightFuzzyNumber;

        public List<String> getRightFuzzyNumber(){return this.rightFuzzyNumber;}
        private List<Integer> countList;

        public List<Integer> getCountList(){return this.countList;}

        private Integer countSt;

        private Integer countEd;

        public Integer getCountSt(){return this.countSt;}

        public Integer getCountEd(){return this.countEd;}

        private List<Double> priceList;

        public List<Double> getPriceList(){return this.priceList;}

        private Double priceSt;

        private Double priceEd;

        public Double getPriceSt(){return this.priceSt;}

        public Double getPriceEd(){return this.priceEd;}

        private List<String> nameList;

        public List<String> getNameList(){return this.nameList;}


        private List<String> fuzzyName;

        public List<String> getFuzzyName(){return this.fuzzyName;}

        private List<String> rightFuzzyName;

        public List<String> getRightFuzzyName(){return this.rightFuzzyName;}
        private List<String> sizeList;

        public List<String> getSizeList(){return this.sizeList;}


        private List<String> fuzzySize;

        public List<String> getFuzzySize(){return this.fuzzySize;}

        private List<String> rightFuzzySize;

        public List<String> getRightFuzzySize(){return this.rightFuzzySize;}
        private List<String> brandList;

        public List<String> getBrandList(){return this.brandList;}


        private List<String> fuzzyBrand;

        public List<String> getFuzzyBrand(){return this.fuzzyBrand;}

        private List<String> rightFuzzyBrand;

        public List<String> getRightFuzzyBrand(){return this.rightFuzzyBrand;}
        private List<String> textureList;

        public List<String> getTextureList(){return this.textureList;}


        private List<String> fuzzyTexture;

        public List<String> getFuzzyTexture(){return this.fuzzyTexture;}

        private List<String> rightFuzzyTexture;

        public List<String> getRightFuzzyTexture(){return this.rightFuzzyTexture;}
        private QueryBuilder (){
            this.fetchFields = new HashMap<>();
        }

        public QueryBuilder fuzzyNumber (List<String> fuzzyNumber){
            this.fuzzyNumber = fuzzyNumber;
            return this;
        }

        public QueryBuilder fuzzyNumber (String ... fuzzyNumber){
            this.fuzzyNumber = solveNullList(fuzzyNumber);
            return this;
        }

        public QueryBuilder rightFuzzyNumber (List<String> rightFuzzyNumber){
            this.rightFuzzyNumber = rightFuzzyNumber;
            return this;
        }

        public QueryBuilder rightFuzzyNumber (String ... rightFuzzyNumber){
            this.rightFuzzyNumber = solveNullList(rightFuzzyNumber);
            return this;
        }

        public QueryBuilder number(String number){
            setNumber(number);
            return this;
        }

        public QueryBuilder numberList(String ... number){
            this.numberList = solveNullList(number);
            return this;
        }

        public QueryBuilder numberList(List<String> number){
            this.numberList = number;
            return this;
        }

        public QueryBuilder fetchNumber(){
            setFetchFields("fetchFields","number");
            return this;
        }

        public QueryBuilder excludeNumber(){
            setFetchFields("excludeFields","number");
            return this;
        }

        public QueryBuilder countBetWeen(Integer countSt,Integer countEd){
            this.countSt = countSt;
            this.countEd = countEd;
            return this;
        }

        public QueryBuilder countGreaterEqThan(Integer countSt){
            this.countSt = countSt;
            return this;
        }
        public QueryBuilder countLessEqThan(Integer countEd){
            this.countEd = countEd;
            return this;
        }


        public QueryBuilder count(Integer count){
            setCount(count);
            return this;
        }

        public QueryBuilder countList(Integer ... count){
            this.countList = solveNullList(count);
            return this;
        }

        public QueryBuilder countList(List<Integer> count){
            this.countList = count;
            return this;
        }

        public QueryBuilder fetchCount(){
            setFetchFields("fetchFields","count");
            return this;
        }

        public QueryBuilder excludeCount(){
            setFetchFields("excludeFields","count");
            return this;
        }

        public QueryBuilder priceBetWeen(Double priceSt,Double priceEd){
            this.priceSt = priceSt;
            this.priceEd = priceEd;
            return this;
        }

        public QueryBuilder priceGreaterEqThan(Double priceSt){
            this.priceSt = priceSt;
            return this;
        }
        public QueryBuilder priceLessEqThan(Double priceEd){
            this.priceEd = priceEd;
            return this;
        }


        public QueryBuilder price(Double price){
            setPrice(price);
            return this;
        }

        public QueryBuilder priceList(Double ... price){
            this.priceList = solveNullList(price);
            return this;
        }

        public QueryBuilder priceList(List<Double> price){
            this.priceList = price;
            return this;
        }

        public QueryBuilder fetchPrice(){
            setFetchFields("fetchFields","price");
            return this;
        }

        public QueryBuilder excludePrice(){
            setFetchFields("excludeFields","price");
            return this;
        }

        public QueryBuilder fuzzyName (List<String> fuzzyName){
            this.fuzzyName = fuzzyName;
            return this;
        }

        public QueryBuilder fuzzyName (String ... fuzzyName){
            this.fuzzyName = solveNullList(fuzzyName);
            return this;
        }

        public QueryBuilder rightFuzzyName (List<String> rightFuzzyName){
            this.rightFuzzyName = rightFuzzyName;
            return this;
        }

        public QueryBuilder rightFuzzyName (String ... rightFuzzyName){
            this.rightFuzzyName = solveNullList(rightFuzzyName);
            return this;
        }

        public QueryBuilder name(String name){
            setName(name);
            return this;
        }

        public QueryBuilder nameList(String ... name){
            this.nameList = solveNullList(name);
            return this;
        }

        public QueryBuilder nameList(List<String> name){
            this.nameList = name;
            return this;
        }

        public QueryBuilder fetchName(){
            setFetchFields("fetchFields","name");
            return this;
        }

        public QueryBuilder excludeName(){
            setFetchFields("excludeFields","name");
            return this;
        }

        public QueryBuilder fuzzySize (List<String> fuzzySize){
            this.fuzzySize = fuzzySize;
            return this;
        }

        public QueryBuilder fuzzySize (String ... fuzzySize){
            this.fuzzySize = solveNullList(fuzzySize);
            return this;
        }

        public QueryBuilder rightFuzzySize (List<String> rightFuzzySize){
            this.rightFuzzySize = rightFuzzySize;
            return this;
        }

        public QueryBuilder rightFuzzySize (String ... rightFuzzySize){
            this.rightFuzzySize = solveNullList(rightFuzzySize);
            return this;
        }

        public QueryBuilder size(List<String> size){
            setSize(size);
            return this;
        }

        public QueryBuilder sizeList(String ... size){
            this.sizeList = solveNullList(size);
            return this;
        }

        public QueryBuilder sizeList(List<String> size){
            this.sizeList = size;
            return this;
        }

        public QueryBuilder fetchSize(){
            setFetchFields("fetchFields","size");
            return this;
        }

        public QueryBuilder excludeSize(){
            setFetchFields("excludeFields","size");
            return this;
        }

        public QueryBuilder fuzzyBrand (List<String> fuzzyBrand){
            this.fuzzyBrand = fuzzyBrand;
            return this;
        }

        public QueryBuilder fuzzyBrand (String ... fuzzyBrand){
            this.fuzzyBrand = solveNullList(fuzzyBrand);
            return this;
        }

        public QueryBuilder rightFuzzyBrand (List<String> rightFuzzyBrand){
            this.rightFuzzyBrand = rightFuzzyBrand;
            return this;
        }

        public QueryBuilder rightFuzzyBrand (String ... rightFuzzyBrand){
            this.rightFuzzyBrand = solveNullList(rightFuzzyBrand);
            return this;
        }

        public QueryBuilder brand(String brand){
            setBrand(brand);
            return this;
        }

        public QueryBuilder brandList(String ... brand){
            this.brandList = solveNullList(brand);
            return this;
        }

        public QueryBuilder brandList(List<String> brand){
            this.brandList = brand;
            return this;
        }

        public QueryBuilder fetchBrand(){
            setFetchFields("fetchFields","brand");
            return this;
        }

        public QueryBuilder excludeBrand(){
            setFetchFields("excludeFields","brand");
            return this;
        }

        public QueryBuilder fuzzyTexture (List<String> fuzzyTexture){
            this.fuzzyTexture = fuzzyTexture;
            return this;
        }

        public QueryBuilder fuzzyTexture (String ... fuzzyTexture){
            this.fuzzyTexture = solveNullList(fuzzyTexture);
            return this;
        }

        public QueryBuilder rightFuzzyTexture (List<String> rightFuzzyTexture){
            this.rightFuzzyTexture = rightFuzzyTexture;
            return this;
        }

        public QueryBuilder rightFuzzyTexture (String ... rightFuzzyTexture){
            this.rightFuzzyTexture = solveNullList(rightFuzzyTexture);
            return this;
        }

        public QueryBuilder texture(String texture){
            setTexture(texture);
            return this;
        }

        public QueryBuilder describe(String describe){
            setDescribe(describe);
            return this;
        }

        public QueryBuilder textureList(String ... texture){
            this.textureList = solveNullList(texture);
            return this;
        }

        public QueryBuilder textureList(List<String> texture){
            this.textureList = texture;
            return this;
        }

        public QueryBuilder fetchTexture(){
            setFetchFields("fetchFields","texture");
            return this;
        }

        public QueryBuilder excludeTexture(){
            setFetchFields("excludeFields","texture");
            return this;
        }
        private <T>List<T> solveNullList(T ... objs){
            if (objs != null){
            List<T> list = new ArrayList<>();
                for (T item : objs){
                    if (item != null){
                        list.add(item);
                    }
                }
                return list;
            }
            return null;
        }

        public QueryBuilder fetchAll(){
            this.fetchFields.put("AllFields",true);
            return this;
        }

        public QueryBuilder addField(String ... fields){
            List<String> list = new ArrayList<>();
            if (fields != null){
                for (String field : fields){
                    list.add(field);
                }
            }
            this.fetchFields.put("otherFields",list);
            return this;
        }
        @SuppressWarnings("unchecked")
        private void setFetchFields(String key,String val){
            Map<String,Boolean> fields= (Map<String, Boolean>) this.fetchFields.get(key);
            if (fields == null){
                fields = new HashMap<>();
            }
            fields.put(val,true);
            this.fetchFields.put(key,fields);
        }

        public TbGoods build(){return this;}
    }


    public static class ConditionBuilder{
        private List<String> numberList;

        public List<String> getNumberList(){return this.numberList;}


        private List<String> fuzzyNumber;

        public List<String> getFuzzyNumber(){return this.fuzzyNumber;}

        private List<String> rightFuzzyNumber;

        public List<String> getRightFuzzyNumber(){return this.rightFuzzyNumber;}
        private List<Integer> countList;

        public List<Integer> getCountList(){return this.countList;}

        private Integer countSt;

        private Integer countEd;

        public Integer getCountSt(){return this.countSt;}

        public Integer getCountEd(){return this.countEd;}

        private List<Double> priceList;

        public List<Double> getPriceList(){return this.priceList;}

        private Double priceSt;

        private Double priceEd;

        public Double getPriceSt(){return this.priceSt;}

        public Double getPriceEd(){return this.priceEd;}

        private List<String> nameList;

        public List<String> getNameList(){return this.nameList;}


        private List<String> fuzzyName;

        public List<String> getFuzzyName(){return this.fuzzyName;}

        private List<String> rightFuzzyName;

        public List<String> getRightFuzzyName(){return this.rightFuzzyName;}
        private List<String> sizeList;

        public List<String> getSizeList(){return this.sizeList;}


        private List<String> fuzzySize;

        public List<String> getFuzzySize(){return this.fuzzySize;}

        private List<String> rightFuzzySize;

        public List<String> getRightFuzzySize(){return this.rightFuzzySize;}
        private List<String> brandList;

        public List<String> getBrandList(){return this.brandList;}


        private List<String> fuzzyBrand;

        public List<String> getFuzzyBrand(){return this.fuzzyBrand;}

        private List<String> rightFuzzyBrand;

        public List<String> getRightFuzzyBrand(){return this.rightFuzzyBrand;}
        private List<String> textureList;

        public List<String> getTextureList(){return this.textureList;}


        private List<String> fuzzyTexture;

        public List<String> getFuzzyTexture(){return this.fuzzyTexture;}

        private List<String> rightFuzzyTexture;

        public List<String> getRightFuzzyTexture(){return this.rightFuzzyTexture;}

        public ConditionBuilder fuzzyNumber (List<String> fuzzyNumber){
            this.fuzzyNumber = fuzzyNumber;
            return this;
        }

        public ConditionBuilder fuzzyNumber (String ... fuzzyNumber){
            this.fuzzyNumber = solveNullList(fuzzyNumber);
            return this;
        }

        public ConditionBuilder rightFuzzyNumber (List<String> rightFuzzyNumber){
            this.rightFuzzyNumber = rightFuzzyNumber;
            return this;
        }

        public ConditionBuilder rightFuzzyNumber (String ... rightFuzzyNumber){
            this.rightFuzzyNumber = solveNullList(rightFuzzyNumber);
            return this;
        }

        public ConditionBuilder numberList(String ... number){
            this.numberList = solveNullList(number);
            return this;
        }

        public ConditionBuilder numberList(List<String> number){
            this.numberList = number;
            return this;
        }

        public ConditionBuilder countBetWeen(Integer countSt,Integer countEd){
            this.countSt = countSt;
            this.countEd = countEd;
            return this;
        }

        public ConditionBuilder countGreaterEqThan(Integer countSt){
            this.countSt = countSt;
            return this;
        }
        public ConditionBuilder countLessEqThan(Integer countEd){
            this.countEd = countEd;
            return this;
        }


        public ConditionBuilder countList(Integer ... count){
            this.countList = solveNullList(count);
            return this;
        }

        public ConditionBuilder countList(List<Integer> count){
            this.countList = count;
            return this;
        }

        public ConditionBuilder priceBetWeen(Double priceSt,Double priceEd){
            this.priceSt = priceSt;
            this.priceEd = priceEd;
            return this;
        }

        public ConditionBuilder priceGreaterEqThan(Double priceSt){
            this.priceSt = priceSt;
            return this;
        }
        public ConditionBuilder priceLessEqThan(Double priceEd){
            this.priceEd = priceEd;
            return this;
        }


        public ConditionBuilder priceList(Double ... price){
            this.priceList = solveNullList(price);
            return this;
        }

        public ConditionBuilder priceList(List<Double> price){
            this.priceList = price;
            return this;
        }

        public ConditionBuilder fuzzyName (List<String> fuzzyName){
            this.fuzzyName = fuzzyName;
            return this;
        }

        public ConditionBuilder fuzzyName (String ... fuzzyName){
            this.fuzzyName = solveNullList(fuzzyName);
            return this;
        }

        public ConditionBuilder rightFuzzyName (List<String> rightFuzzyName){
            this.rightFuzzyName = rightFuzzyName;
            return this;
        }

        public ConditionBuilder rightFuzzyName (String ... rightFuzzyName){
            this.rightFuzzyName = solveNullList(rightFuzzyName);
            return this;
        }

        public ConditionBuilder nameList(String ... name){
            this.nameList = solveNullList(name);
            return this;
        }

        public ConditionBuilder nameList(List<String> name){
            this.nameList = name;
            return this;
        }

        public ConditionBuilder fuzzySize (List<String> fuzzySize){
            this.fuzzySize = fuzzySize;
            return this;
        }

        public ConditionBuilder fuzzySize (String ... fuzzySize){
            this.fuzzySize = solveNullList(fuzzySize);
            return this;
        }

        public ConditionBuilder rightFuzzySize (List<String> rightFuzzySize){
            this.rightFuzzySize = rightFuzzySize;
            return this;
        }

        public ConditionBuilder rightFuzzySize (String ... rightFuzzySize){
            this.rightFuzzySize = solveNullList(rightFuzzySize);
            return this;
        }

        public ConditionBuilder sizeList(String ... size){
            this.sizeList = solveNullList(size);
            return this;
        }

        public ConditionBuilder sizeList(List<String> size){
            this.sizeList = size;
            return this;
        }

        public ConditionBuilder fuzzyBrand (List<String> fuzzyBrand){
            this.fuzzyBrand = fuzzyBrand;
            return this;
        }

        public ConditionBuilder fuzzyBrand (String ... fuzzyBrand){
            this.fuzzyBrand = solveNullList(fuzzyBrand);
            return this;
        }

        public ConditionBuilder rightFuzzyBrand (List<String> rightFuzzyBrand){
            this.rightFuzzyBrand = rightFuzzyBrand;
            return this;
        }

        public ConditionBuilder rightFuzzyBrand (String ... rightFuzzyBrand){
            this.rightFuzzyBrand = solveNullList(rightFuzzyBrand);
            return this;
        }

        public ConditionBuilder brandList(String ... brand){
            this.brandList = solveNullList(brand);
            return this;
        }

        public ConditionBuilder brandList(List<String> brand){
            this.brandList = brand;
            return this;
        }

        public ConditionBuilder fuzzyTexture (List<String> fuzzyTexture){
            this.fuzzyTexture = fuzzyTexture;
            return this;
        }

        public ConditionBuilder fuzzyTexture (String ... fuzzyTexture){
            this.fuzzyTexture = solveNullList(fuzzyTexture);
            return this;
        }

        public ConditionBuilder rightFuzzyTexture (List<String> rightFuzzyTexture){
            this.rightFuzzyTexture = rightFuzzyTexture;
            return this;
        }

        public ConditionBuilder rightFuzzyTexture (String ... rightFuzzyTexture){
            this.rightFuzzyTexture = solveNullList(rightFuzzyTexture);
            return this;
        }

        public ConditionBuilder textureList(String ... texture){
            this.textureList = solveNullList(texture);
            return this;
        }

        public ConditionBuilder textureList(List<String> texture){
            this.textureList = texture;
            return this;
        }

        private <T>List<T> solveNullList(T ... objs){
            if (objs != null){
            List<T> list = new ArrayList<>();
                for (T item : objs){
                    if (item != null){
                        list.add(item);
                    }
                }
                return list;
            }
            return null;
        }

        public ConditionBuilder build(){return this;}
    }

    public static class Builder {

        private TbGoods obj;

        public Builder(){
            this.obj = new TbGoods();
        }

        public Builder number(String number){
            this.obj.setNumber(number);
            return this;
        }
        public Builder count(Integer count){
            this.obj.setCount(count);
            return this;
        }
        public Builder price(Double price){
            this.obj.setPrice(price);
            return this;
        }
        public Builder name(String name){
            this.obj.setName(name);
            return this;
        }
        public Builder size(List<String> size){
            this.obj.setSize(size);
            return this;
        }
        public Builder brand(String brand){
            this.obj.setBrand(brand);
            return this;
        }
        public Builder texture(String texture){
            this.obj.setTexture(texture);
            return this;
        }
        public TbGoods build(){return obj;}
    }

}
