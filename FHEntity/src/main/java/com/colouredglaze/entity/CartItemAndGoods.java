package com.colouredglaze.entity;

import java.util.Objects;

/**
 * @author ZhangLin
 * @version 1.0
 * @date 2020/11/26 21:17
 */
public class CartItemAndGoods {

    public Integer getRid() {
        return rid;
    }

    public void setRid(Integer rid) {
        this.rid = rid;
    }

    public String getNumber() {
        return number;
    }

    public void setNumber(String number) {
        this.number = number;
    }

    public Double getPrice() {
        return price;
    }

    public void setPrice(Double price) {
        this.price = price;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public Integer getPcount() {
        return pcount;
    }

    public void setPcount(Integer pcount) {
        this.pcount = pcount;
    }

    /**
     * 主键
     * 记录编号
     * isNullAble:0
     */
    private Integer rid;

    /**
     * 主键
     * 商品编号
     * isNullAble:0
     */
    private String number;

    /**
     * 商品价格
     * isNullAble:0
     */
    private Double price;

    /**
     * 商品名称
     * isNullAble:0
     */
    private String name;

    /**
     * 数量
     * isNullAble:0
     */
    private Integer pcount;

    private String size;

    @Override
    public String toString() {
        return "CartItemAndGoods{" +
                "rid=" + rid +
                ", number='" + number + '\'' +
                ", price=" + price +
                ", name='" + name + '\'' +
                ", pcount=" + pcount +
                ", size='" + size + '\'' +
                '}';
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (!(o instanceof CartItemAndGoods)) return false;
        CartItemAndGoods that = (CartItemAndGoods) o;
        return getRid().equals(that.getRid()) && getNumber().equals(that.getNumber()) && getPrice().equals(that.getPrice()) && getName().equals(that.getName()) && getPcount().equals(that.getPcount()) && getSize().equals(that.getSize());
    }

    @Override
    public int hashCode() {
        return Objects.hash(getRid(), getNumber(), getPrice(), getName(), getPcount(), getSize());
    }

    public String getSize() {
        return size;
    }

    public void setSize(String size) {
        this.size = size;
    }
}
