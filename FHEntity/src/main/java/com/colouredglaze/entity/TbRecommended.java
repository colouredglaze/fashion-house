package com.colouredglaze.entity;
import java.io.Serializable;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.Map;
import java.util.List;
/**
*
*  @author author
*/
public class TbRecommended implements Serializable {

    private static final long serialVersionUID = 1574592471599L;


    /**
    * 主键
    * 记录编号
    * isNullAble:0
    */
    private Integer rid;

    /**
    * 商品编号
    * isNullAble:0
    */
    private String number;


    public void setRid(Integer rid){this.rid = rid;}

    public Integer getRid(){return this.rid;}

    public void setNumber(String number){this.number = number;}

    public String getNumber(){return this.number;}
    @Override
    public String toString() {
        return "TbRecommended{" +
                "rid='" + rid + '\'' +
                "number='" + number + '\'' +
            '}';
    }

    public static Builder Build(){return new Builder();}

    public static ConditionBuilder ConditionBuild(){return new ConditionBuilder();}

    public static UpdateBuilder UpdateBuild(){return new UpdateBuilder();}

    public static QueryBuilder QueryBuild(){return new QueryBuilder();}

    public static class UpdateBuilder {

        private TbRecommended set;

        private ConditionBuilder where;

        public UpdateBuilder set(TbRecommended set){
            this.set = set;
            return this;
        }

        public TbRecommended getSet(){
            return this.set;
        }

        public UpdateBuilder where(ConditionBuilder where){
            this.where = where;
            return this;
        }

        public ConditionBuilder getWhere(){
            return this.where;
        }

        public UpdateBuilder build(){
            return this;
        }
    }

    public static class QueryBuilder extends TbRecommended{
        /**
        * 需要返回的列
        */
        private Map<String,Object> fetchFields;

        public Map<String,Object> getFetchFields(){return this.fetchFields;}

        private List<Integer> ridList;

        public List<Integer> getRidList(){return this.ridList;}

        private Integer ridSt;

        private Integer ridEd;

        public Integer getRidSt(){return this.ridSt;}

        public Integer getRidEd(){return this.ridEd;}

        private List<String> numberList;

        public List<String> getNumberList(){return this.numberList;}


        private List<String> fuzzyNumber;

        public List<String> getFuzzyNumber(){return this.fuzzyNumber;}

        private List<String> rightFuzzyNumber;

        public List<String> getRightFuzzyNumber(){return this.rightFuzzyNumber;}
        private QueryBuilder (){
            this.fetchFields = new HashMap<>();
        }

        public QueryBuilder ridBetWeen(Integer ridSt,Integer ridEd){
            this.ridSt = ridSt;
            this.ridEd = ridEd;
            return this;
        }

        public QueryBuilder ridGreaterEqThan(Integer ridSt){
            this.ridSt = ridSt;
            return this;
        }
        public QueryBuilder ridLessEqThan(Integer ridEd){
            this.ridEd = ridEd;
            return this;
        }


        public QueryBuilder rid(Integer rid){
            setRid(rid);
            return this;
        }

        public QueryBuilder ridList(Integer ... rid){
            this.ridList = solveNullList(rid);
            return this;
        }

        public QueryBuilder ridList(List<Integer> rid){
            this.ridList = rid;
            return this;
        }

        public QueryBuilder fetchRid(){
            setFetchFields("fetchFields","rid");
            return this;
        }

        public QueryBuilder excludeRid(){
            setFetchFields("excludeFields","rid");
            return this;
        }

        public QueryBuilder fuzzyNumber (List<String> fuzzyNumber){
            this.fuzzyNumber = fuzzyNumber;
            return this;
        }

        public QueryBuilder fuzzyNumber (String ... fuzzyNumber){
            this.fuzzyNumber = solveNullList(fuzzyNumber);
            return this;
        }

        public QueryBuilder rightFuzzyNumber (List<String> rightFuzzyNumber){
            this.rightFuzzyNumber = rightFuzzyNumber;
            return this;
        }

        public QueryBuilder rightFuzzyNumber (String ... rightFuzzyNumber){
            this.rightFuzzyNumber = solveNullList(rightFuzzyNumber);
            return this;
        }

        public QueryBuilder number(String number){
            setNumber(number);
            return this;
        }

        public QueryBuilder numberList(String ... number){
            this.numberList = solveNullList(number);
            return this;
        }

        public QueryBuilder numberList(List<String> number){
            this.numberList = number;
            return this;
        }

        public QueryBuilder fetchNumber(){
            setFetchFields("fetchFields","number");
            return this;
        }

        public QueryBuilder excludeNumber(){
            setFetchFields("excludeFields","number");
            return this;
        }
        private <T>List<T> solveNullList(T ... objs){
            if (objs != null){
            List<T> list = new ArrayList<>();
                for (T item : objs){
                    if (item != null){
                        list.add(item);
                    }
                }
                return list;
            }
            return null;
        }

        public QueryBuilder fetchAll(){
            this.fetchFields.put("AllFields",true);
            return this;
        }

        public QueryBuilder addField(String ... fields){
            List<String> list = new ArrayList<>();
            if (fields != null){
                for (String field : fields){
                    list.add(field);
                }
            }
            this.fetchFields.put("otherFields",list);
            return this;
        }
        @SuppressWarnings("unchecked")
        private void setFetchFields(String key,String val){
            Map<String,Boolean> fields= (Map<String, Boolean>) this.fetchFields.get(key);
            if (fields == null){
                fields = new HashMap<>();
            }
            fields.put(val,true);
            this.fetchFields.put(key,fields);
        }

        public TbRecommended build(){return this;}
    }


    public static class ConditionBuilder{
        private List<Integer> ridList;

        public List<Integer> getRidList(){return this.ridList;}

        private Integer ridSt;

        private Integer ridEd;

        public Integer getRidSt(){return this.ridSt;}

        public Integer getRidEd(){return this.ridEd;}

        private List<String> numberList;

        public List<String> getNumberList(){return this.numberList;}


        private List<String> fuzzyNumber;

        public List<String> getFuzzyNumber(){return this.fuzzyNumber;}

        private List<String> rightFuzzyNumber;

        public List<String> getRightFuzzyNumber(){return this.rightFuzzyNumber;}

        public ConditionBuilder ridBetWeen(Integer ridSt,Integer ridEd){
            this.ridSt = ridSt;
            this.ridEd = ridEd;
            return this;
        }

        public ConditionBuilder ridGreaterEqThan(Integer ridSt){
            this.ridSt = ridSt;
            return this;
        }
        public ConditionBuilder ridLessEqThan(Integer ridEd){
            this.ridEd = ridEd;
            return this;
        }


        public ConditionBuilder ridList(Integer ... rid){
            this.ridList = solveNullList(rid);
            return this;
        }

        public ConditionBuilder ridList(List<Integer> rid){
            this.ridList = rid;
            return this;
        }

        public ConditionBuilder fuzzyNumber (List<String> fuzzyNumber){
            this.fuzzyNumber = fuzzyNumber;
            return this;
        }

        public ConditionBuilder fuzzyNumber (String ... fuzzyNumber){
            this.fuzzyNumber = solveNullList(fuzzyNumber);
            return this;
        }

        public ConditionBuilder rightFuzzyNumber (List<String> rightFuzzyNumber){
            this.rightFuzzyNumber = rightFuzzyNumber;
            return this;
        }

        public ConditionBuilder rightFuzzyNumber (String ... rightFuzzyNumber){
            this.rightFuzzyNumber = solveNullList(rightFuzzyNumber);
            return this;
        }

        public ConditionBuilder numberList(String ... number){
            this.numberList = solveNullList(number);
            return this;
        }

        public ConditionBuilder numberList(List<String> number){
            this.numberList = number;
            return this;
        }

        private <T>List<T> solveNullList(T ... objs){
            if (objs != null){
            List<T> list = new ArrayList<>();
                for (T item : objs){
                    if (item != null){
                        list.add(item);
                    }
                }
                return list;
            }
            return null;
        }

        public ConditionBuilder build(){return this;}
    }

    public static class Builder {

        private TbRecommended obj;

        public Builder(){
            this.obj = new TbRecommended();
        }

        public Builder rid(Integer rid){
            this.obj.setRid(rid);
            return this;
        }
        public Builder number(String number){
            this.obj.setNumber(number);
            return this;
        }
        public TbRecommended build(){return obj;}
    }

}
