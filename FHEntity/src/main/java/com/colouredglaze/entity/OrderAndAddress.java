package com.colouredglaze.entity;

/**
 * @author ZhangLin
 * @version 1.0
 * @date 2020/11/28 23:04
 */
public class OrderAndAddress {

    /**
     * 订单编号
     * isNullAble:0
     */
    private String oid;

    /**
     * 订单状态
     * isNullAble:0
     */
    private String statu;

    /**
     * 收货地址编号
     * isNullAble:1
     */
    private Integer addressId;

    /**
     * 地址
     * isNullAble:0
     */
    private String address;

    /**
     * 收件姓名
     * isNullAble:0
     */
    private String rname;

    /**
     * 支付金额
     * isNullAble:1
     */
    private Double payment;

    public String getOid() {
        return oid;
    }

    @Override
    public String toString() {
        return "OrderAndAddress{" +
                "oid='" + oid + '\'' +
                ", statu='" + statu + '\'' +
                ", addressId=" + addressId +
                ", address='" + address + '\'' +
                ", rname='" + rname + '\'' +
                ", payment=" + payment +
                '}';
    }

    public void setOid(String oid) {
        this.oid = oid;
    }

    public String getStatu() {
        return statu;
    }

    public void setStatu(String statu) {
        this.statu = statu;
    }

    public Integer getAddressId() {
        return addressId;
    }

    public void setAddressId(Integer addressId) {
        this.addressId = addressId;
    }

    public String getAddress() {
        return address;
    }

    public void setAddress(String address) {
        this.address = address;
    }

    public String getRname() {
        return rname;
    }

    public void setRname(String rname) {
        this.rname = rname;
    }

    public Double getPayment() {
        return payment;
    }

    public void setPayment(Double payment) {
        this.payment = payment;
    }
}
