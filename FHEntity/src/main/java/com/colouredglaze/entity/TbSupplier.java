package com.colouredglaze.entity;
import java.io.Serializable;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.Map;
import java.util.List;
/**
*
*  @author author
*/
public class TbSupplier implements Serializable {

    private static final long serialVersionUID = 1574592474695L;


    /**
    * 主键
    * 记录编号
    * isNullAble:0
    */
    private Integer rid;

    /**
    * 商品编号
    * isNullAble:0
    */
    private String brand;


    public void setRid(Integer rid){this.rid = rid;}

    public Integer getRid(){return this.rid;}

    public void setBrand(String brand){this.brand = brand;}

    public String getBrand(){return this.brand;}
    @Override
    public String toString() {
        return "TbSupplier{" +
                "rid='" + rid + '\'' +
                "brand='" + brand + '\'' +
            '}';
    }

    public static Builder Build(){return new Builder();}

    public static ConditionBuilder ConditionBuild(){return new ConditionBuilder();}

    public static UpdateBuilder UpdateBuild(){return new UpdateBuilder();}

    public static QueryBuilder QueryBuild(){return new QueryBuilder();}

    public static class UpdateBuilder {

        private TbSupplier set;

        private ConditionBuilder where;

        public UpdateBuilder set(TbSupplier set){
            this.set = set;
            return this;
        }

        public TbSupplier getSet(){
            return this.set;
        }

        public UpdateBuilder where(ConditionBuilder where){
            this.where = where;
            return this;
        }

        public ConditionBuilder getWhere(){
            return this.where;
        }

        public UpdateBuilder build(){
            return this;
        }
    }

    public static class QueryBuilder extends TbSupplier{
        /**
        * 需要返回的列
        */
        private Map<String,Object> fetchFields;

        public Map<String,Object> getFetchFields(){return this.fetchFields;}

        private List<Integer> ridList;

        public List<Integer> getRidList(){return this.ridList;}

        private Integer ridSt;

        private Integer ridEd;

        public Integer getRidSt(){return this.ridSt;}

        public Integer getRidEd(){return this.ridEd;}

        private List<String> brandList;

        public List<String> getBrandList(){return this.brandList;}


        private List<String> fuzzyBrand;

        public List<String> getFuzzyBrand(){return this.fuzzyBrand;}

        private List<String> rightFuzzyBrand;

        public List<String> getRightFuzzyBrand(){return this.rightFuzzyBrand;}
        private QueryBuilder (){
            this.fetchFields = new HashMap<>();
        }

        public QueryBuilder ridBetWeen(Integer ridSt,Integer ridEd){
            this.ridSt = ridSt;
            this.ridEd = ridEd;
            return this;
        }

        public QueryBuilder ridGreaterEqThan(Integer ridSt){
            this.ridSt = ridSt;
            return this;
        }
        public QueryBuilder ridLessEqThan(Integer ridEd){
            this.ridEd = ridEd;
            return this;
        }


        public QueryBuilder rid(Integer rid){
            setRid(rid);
            return this;
        }

        public QueryBuilder ridList(Integer ... rid){
            this.ridList = solveNullList(rid);
            return this;
        }

        public QueryBuilder ridList(List<Integer> rid){
            this.ridList = rid;
            return this;
        }

        public QueryBuilder fetchRid(){
            setFetchFields("fetchFields","rid");
            return this;
        }

        public QueryBuilder excludeRid(){
            setFetchFields("excludeFields","rid");
            return this;
        }

        public QueryBuilder fuzzyBrand (List<String> fuzzyBrand){
            this.fuzzyBrand = fuzzyBrand;
            return this;
        }

        public QueryBuilder fuzzyBrand (String ... fuzzyBrand){
            this.fuzzyBrand = solveNullList(fuzzyBrand);
            return this;
        }

        public QueryBuilder rightFuzzyBrand (List<String> rightFuzzyBrand){
            this.rightFuzzyBrand = rightFuzzyBrand;
            return this;
        }

        public QueryBuilder rightFuzzyBrand (String ... rightFuzzyBrand){
            this.rightFuzzyBrand = solveNullList(rightFuzzyBrand);
            return this;
        }

        public QueryBuilder brand(String brand){
            setBrand(brand);
            return this;
        }

        public QueryBuilder brandList(String ... brand){
            this.brandList = solveNullList(brand);
            return this;
        }

        public QueryBuilder brandList(List<String> brand){
            this.brandList = brand;
            return this;
        }

        public QueryBuilder fetchBrand(){
            setFetchFields("fetchFields","brand");
            return this;
        }

        public QueryBuilder excludeBrand(){
            setFetchFields("excludeFields","brand");
            return this;
        }
        private <T>List<T> solveNullList(T ... objs){
            if (objs != null){
            List<T> list = new ArrayList<>();
                for (T item : objs){
                    if (item != null){
                        list.add(item);
                    }
                }
                return list;
            }
            return null;
        }

        public QueryBuilder fetchAll(){
            this.fetchFields.put("AllFields",true);
            return this;
        }

        public QueryBuilder addField(String ... fields){
            List<String> list = new ArrayList<>();
            if (fields != null){
                for (String field : fields){
                    list.add(field);
                }
            }
            this.fetchFields.put("otherFields",list);
            return this;
        }
        @SuppressWarnings("unchecked")
        private void setFetchFields(String key,String val){
            Map<String,Boolean> fields= (Map<String, Boolean>) this.fetchFields.get(key);
            if (fields == null){
                fields = new HashMap<>();
            }
            fields.put(val,true);
            this.fetchFields.put(key,fields);
        }

        public TbSupplier build(){return this;}
    }


    public static class ConditionBuilder{
        private List<Integer> ridList;

        public List<Integer> getRidList(){return this.ridList;}

        private Integer ridSt;

        private Integer ridEd;

        public Integer getRidSt(){return this.ridSt;}

        public Integer getRidEd(){return this.ridEd;}

        private List<String> brandList;

        public List<String> getBrandList(){return this.brandList;}


        private List<String> fuzzyBrand;

        public List<String> getFuzzyBrand(){return this.fuzzyBrand;}

        private List<String> rightFuzzyBrand;

        public List<String> getRightFuzzyBrand(){return this.rightFuzzyBrand;}

        public ConditionBuilder ridBetWeen(Integer ridSt,Integer ridEd){
            this.ridSt = ridSt;
            this.ridEd = ridEd;
            return this;
        }

        public ConditionBuilder ridGreaterEqThan(Integer ridSt){
            this.ridSt = ridSt;
            return this;
        }
        public ConditionBuilder ridLessEqThan(Integer ridEd){
            this.ridEd = ridEd;
            return this;
        }


        public ConditionBuilder ridList(Integer ... rid){
            this.ridList = solveNullList(rid);
            return this;
        }

        public ConditionBuilder ridList(List<Integer> rid){
            this.ridList = rid;
            return this;
        }

        public ConditionBuilder fuzzyBrand (List<String> fuzzyBrand){
            this.fuzzyBrand = fuzzyBrand;
            return this;
        }

        public ConditionBuilder fuzzyBrand (String ... fuzzyBrand){
            this.fuzzyBrand = solveNullList(fuzzyBrand);
            return this;
        }

        public ConditionBuilder rightFuzzyBrand (List<String> rightFuzzyBrand){
            this.rightFuzzyBrand = rightFuzzyBrand;
            return this;
        }

        public ConditionBuilder rightFuzzyBrand (String ... rightFuzzyBrand){
            this.rightFuzzyBrand = solveNullList(rightFuzzyBrand);
            return this;
        }

        public ConditionBuilder brandList(String ... brand){
            this.brandList = solveNullList(brand);
            return this;
        }

        public ConditionBuilder brandList(List<String> brand){
            this.brandList = brand;
            return this;
        }

        private <T>List<T> solveNullList(T ... objs){
            if (objs != null){
            List<T> list = new ArrayList<>();
                for (T item : objs){
                    if (item != null){
                        list.add(item);
                    }
                }
                return list;
            }
            return null;
        }

        public ConditionBuilder build(){return this;}
    }

    public static class Builder {

        private TbSupplier obj;

        public Builder(){
            this.obj = new TbSupplier();
        }

        public Builder rid(Integer rid){
            this.obj.setRid(rid);
            return this;
        }
        public Builder brand(String brand){
            this.obj.setBrand(brand);
            return this;
        }
        public TbSupplier build(){return obj;}
    }

}
