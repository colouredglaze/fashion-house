package com.colouredglaze.entity;

public class TbSize {

    private String goods;

    private String size;

    @Override
    public String toString() {
        return "TbSize{" +
                "goods='" + goods + '\'' +
                ", size='" + size + '\'' +
                '}';
    }

    public TbSize setGoods(String goods) {
        this.goods = goods;
        return this;
    }

    public TbSize setSize(String size) {
        this.size = size;
        return this;
    }

    public String getGoods() {
        return goods;
    }

    public String getSize() {
        return size;
    }

}
