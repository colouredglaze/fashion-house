package com.colouredglaze.entity;
import java.io.Serializable;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.Map;
import java.util.List;
/**
*
*  @author author
*/
public class TbAddress implements Serializable {

    private static final long serialVersionUID = 1574592450430L;


    /**
    * 主键
    * 记录编号
    * isNullAble:0
    */
    private Integer rid;

    /**
    * 账号
    * isNullAble:0
    */
    private String uid;

    /**
    * 地址
    * isNullAble:0
    */
    private String address;

    /**
    * 收件姓名
    * isNullAble:0
    */
    private String rname;

    /**
    * 收件电话
    * isNullAble:0
    */
    private String rphone;


    public void setRid(Integer rid){this.rid = rid;}

    public Integer getRid(){return this.rid;}

    public void setUid(String uid){this.uid = uid;}

    public String getUid(){return this.uid;}

    public void setAddress(String address){this.address = address;}

    public String getAddress(){return this.address;}

    public void setRname(String rname){this.rname = rname;}

    public String getRname(){return this.rname;}

    public void setRphone(String rphone){this.rphone = rphone;}

    public String getRphone(){return this.rphone;}
    @Override
    public String toString() {
        return "TbAddress{" +
                "rid='" + rid + '\'' +
                "uid='" + uid + '\'' +
                "address='" + address + '\'' +
                "rname='" + rname + '\'' +
                "rphone='" + rphone + '\'' +
            '}';
    }

    public static Builder Build(){return new Builder();}

    public static ConditionBuilder ConditionBuild(){return new ConditionBuilder();}

    public static UpdateBuilder UpdateBuild(){return new UpdateBuilder();}

    public static QueryBuilder QueryBuild(){return new QueryBuilder();}

    public static class UpdateBuilder {

        private TbAddress set;

        private ConditionBuilder where;

        public UpdateBuilder set(TbAddress set){
            this.set = set;
            return this;
        }

        public TbAddress getSet(){
            return this.set;
        }

        public UpdateBuilder where(ConditionBuilder where){
            this.where = where;
            return this;
        }

        public ConditionBuilder getWhere(){
            return this.where;
        }

        public UpdateBuilder build(){
            return this;
        }
    }

    public static class QueryBuilder extends TbAddress{
        /**
        * 需要返回的列
        */
        private Map<String,Object> fetchFields;

        public Map<String,Object> getFetchFields(){return this.fetchFields;}

        private List<Integer> ridList;

        public List<Integer> getRidList(){return this.ridList;}

        private Integer ridSt;

        private Integer ridEd;

        public Integer getRidSt(){return this.ridSt;}

        public Integer getRidEd(){return this.ridEd;}

        private List<String> uidList;

        public List<String> getUidList(){return this.uidList;}


        private List<String> fuzzyUid;

        public List<String> getFuzzyUid(){return this.fuzzyUid;}

        private List<String> rightFuzzyUid;

        public List<String> getRightFuzzyUid(){return this.rightFuzzyUid;}
        private List<String> addressList;

        public List<String> getAddressList(){return this.addressList;}


        private List<String> fuzzyAddress;

        public List<String> getFuzzyAddress(){return this.fuzzyAddress;}

        private List<String> rightFuzzyAddress;

        public List<String> getRightFuzzyAddress(){return this.rightFuzzyAddress;}
        private List<String> rnameList;

        public List<String> getRnameList(){return this.rnameList;}


        private List<String> fuzzyRname;

        public List<String> getFuzzyRname(){return this.fuzzyRname;}

        private List<String> rightFuzzyRname;

        public List<String> getRightFuzzyRname(){return this.rightFuzzyRname;}
        private List<String> rphoneList;

        public List<String> getRphoneList(){return this.rphoneList;}


        private List<String> fuzzyRphone;

        public List<String> getFuzzyRphone(){return this.fuzzyRphone;}

        private List<String> rightFuzzyRphone;

        public List<String> getRightFuzzyRphone(){return this.rightFuzzyRphone;}
        private QueryBuilder (){
            this.fetchFields = new HashMap<>();
        }

        public QueryBuilder ridBetWeen(Integer ridSt,Integer ridEd){
            this.ridSt = ridSt;
            this.ridEd = ridEd;
            return this;
        }

        public QueryBuilder ridGreaterEqThan(Integer ridSt){
            this.ridSt = ridSt;
            return this;
        }
        public QueryBuilder ridLessEqThan(Integer ridEd){
            this.ridEd = ridEd;
            return this;
        }


        public QueryBuilder rid(Integer rid){
            setRid(rid);
            return this;
        }

        public QueryBuilder ridList(Integer ... rid){
            this.ridList = solveNullList(rid);
            return this;
        }

        public QueryBuilder ridList(List<Integer> rid){
            this.ridList = rid;
            return this;
        }

        public QueryBuilder fetchRid(){
            setFetchFields("fetchFields","rid");
            return this;
        }

        public QueryBuilder excludeRid(){
            setFetchFields("excludeFields","rid");
            return this;
        }

        public QueryBuilder fuzzyUid (List<String> fuzzyUid){
            this.fuzzyUid = fuzzyUid;
            return this;
        }

        public QueryBuilder fuzzyUid (String ... fuzzyUid){
            this.fuzzyUid = solveNullList(fuzzyUid);
            return this;
        }

        public QueryBuilder rightFuzzyUid (List<String> rightFuzzyUid){
            this.rightFuzzyUid = rightFuzzyUid;
            return this;
        }

        public QueryBuilder rightFuzzyUid (String ... rightFuzzyUid){
            this.rightFuzzyUid = solveNullList(rightFuzzyUid);
            return this;
        }

        public QueryBuilder uid(String uid){
            setUid(uid);
            return this;
        }

        public QueryBuilder uidList(String ... uid){
            this.uidList = solveNullList(uid);
            return this;
        }

        public QueryBuilder uidList(List<String> uid){
            this.uidList = uid;
            return this;
        }

        public QueryBuilder fetchUid(){
            setFetchFields("fetchFields","uid");
            return this;
        }

        public QueryBuilder excludeUid(){
            setFetchFields("excludeFields","uid");
            return this;
        }

        public QueryBuilder fuzzyAddress (List<String> fuzzyAddress){
            this.fuzzyAddress = fuzzyAddress;
            return this;
        }

        public QueryBuilder fuzzyAddress (String ... fuzzyAddress){
            this.fuzzyAddress = solveNullList(fuzzyAddress);
            return this;
        }

        public QueryBuilder rightFuzzyAddress (List<String> rightFuzzyAddress){
            this.rightFuzzyAddress = rightFuzzyAddress;
            return this;
        }

        public QueryBuilder rightFuzzyAddress (String ... rightFuzzyAddress){
            this.rightFuzzyAddress = solveNullList(rightFuzzyAddress);
            return this;
        }

        public QueryBuilder address(String address){
            setAddress(address);
            return this;
        }

        public QueryBuilder addressList(String ... address){
            this.addressList = solveNullList(address);
            return this;
        }

        public QueryBuilder addressList(List<String> address){
            this.addressList = address;
            return this;
        }

        public QueryBuilder fetchAddress(){
            setFetchFields("fetchFields","address");
            return this;
        }

        public QueryBuilder excludeAddress(){
            setFetchFields("excludeFields","address");
            return this;
        }

        public QueryBuilder fuzzyRname (List<String> fuzzyRname){
            this.fuzzyRname = fuzzyRname;
            return this;
        }

        public QueryBuilder fuzzyRname (String ... fuzzyRname){
            this.fuzzyRname = solveNullList(fuzzyRname);
            return this;
        }

        public QueryBuilder rightFuzzyRname (List<String> rightFuzzyRname){
            this.rightFuzzyRname = rightFuzzyRname;
            return this;
        }

        public QueryBuilder rightFuzzyRname (String ... rightFuzzyRname){
            this.rightFuzzyRname = solveNullList(rightFuzzyRname);
            return this;
        }

        public QueryBuilder rname(String rname){
            setRname(rname);
            return this;
        }

        public QueryBuilder rnameList(String ... rname){
            this.rnameList = solveNullList(rname);
            return this;
        }

        public QueryBuilder rnameList(List<String> rname){
            this.rnameList = rname;
            return this;
        }

        public QueryBuilder fetchRname(){
            setFetchFields("fetchFields","rname");
            return this;
        }

        public QueryBuilder excludeRname(){
            setFetchFields("excludeFields","rname");
            return this;
        }

        public QueryBuilder fuzzyRphone (List<String> fuzzyRphone){
            this.fuzzyRphone = fuzzyRphone;
            return this;
        }

        public QueryBuilder fuzzyRphone (String ... fuzzyRphone){
            this.fuzzyRphone = solveNullList(fuzzyRphone);
            return this;
        }

        public QueryBuilder rightFuzzyRphone (List<String> rightFuzzyRphone){
            this.rightFuzzyRphone = rightFuzzyRphone;
            return this;
        }

        public QueryBuilder rightFuzzyRphone (String ... rightFuzzyRphone){
            this.rightFuzzyRphone = solveNullList(rightFuzzyRphone);
            return this;
        }

        public QueryBuilder rphone(String rphone){
            setRphone(rphone);
            return this;
        }

        public QueryBuilder rphoneList(String ... rphone){
            this.rphoneList = solveNullList(rphone);
            return this;
        }

        public QueryBuilder rphoneList(List<String> rphone){
            this.rphoneList = rphone;
            return this;
        }

        public QueryBuilder fetchRphone(){
            setFetchFields("fetchFields","rphone");
            return this;
        }

        public QueryBuilder excludeRphone(){
            setFetchFields("excludeFields","rphone");
            return this;
        }
        private <T>List<T> solveNullList(T ... objs){
            if (objs != null){
            List<T> list = new ArrayList<>();
                for (T item : objs){
                    if (item != null){
                        list.add(item);
                    }
                }
                return list;
            }
            return null;
        }

        public QueryBuilder fetchAll(){
            this.fetchFields.put("AllFields",true);
            return this;
        }

        public QueryBuilder addField(String ... fields){
            List<String> list = new ArrayList<>();
            if (fields != null){
                for (String field : fields){
                    list.add(field);
                }
            }
            this.fetchFields.put("otherFields",list);
            return this;
        }
        @SuppressWarnings("unchecked")
        private void setFetchFields(String key,String val){
            Map<String,Boolean> fields= (Map<String, Boolean>) this.fetchFields.get(key);
            if (fields == null){
                fields = new HashMap<>();
            }
            fields.put(val,true);
            this.fetchFields.put(key,fields);
        }

        public TbAddress build(){return this;}
    }


    public static class ConditionBuilder{
        private List<Integer> ridList;

        public List<Integer> getRidList(){return this.ridList;}

        private Integer ridSt;

        private Integer ridEd;

        public Integer getRidSt(){return this.ridSt;}

        public Integer getRidEd(){return this.ridEd;}

        private List<String> uidList;

        public List<String> getUidList(){return this.uidList;}


        private List<String> fuzzyUid;

        public List<String> getFuzzyUid(){return this.fuzzyUid;}

        private List<String> rightFuzzyUid;

        public List<String> getRightFuzzyUid(){return this.rightFuzzyUid;}
        private List<String> addressList;

        public List<String> getAddressList(){return this.addressList;}


        private List<String> fuzzyAddress;

        public List<String> getFuzzyAddress(){return this.fuzzyAddress;}

        private List<String> rightFuzzyAddress;

        public List<String> getRightFuzzyAddress(){return this.rightFuzzyAddress;}
        private List<String> rnameList;

        public List<String> getRnameList(){return this.rnameList;}


        private List<String> fuzzyRname;

        public List<String> getFuzzyRname(){return this.fuzzyRname;}

        private List<String> rightFuzzyRname;

        public List<String> getRightFuzzyRname(){return this.rightFuzzyRname;}
        private List<String> rphoneList;

        public List<String> getRphoneList(){return this.rphoneList;}


        private List<String> fuzzyRphone;

        public List<String> getFuzzyRphone(){return this.fuzzyRphone;}

        private List<String> rightFuzzyRphone;

        public List<String> getRightFuzzyRphone(){return this.rightFuzzyRphone;}

        public ConditionBuilder ridBetWeen(Integer ridSt,Integer ridEd){
            this.ridSt = ridSt;
            this.ridEd = ridEd;
            return this;
        }

        public ConditionBuilder ridGreaterEqThan(Integer ridSt){
            this.ridSt = ridSt;
            return this;
        }
        public ConditionBuilder ridLessEqThan(Integer ridEd){
            this.ridEd = ridEd;
            return this;
        }


        public ConditionBuilder ridList(Integer ... rid){
            this.ridList = solveNullList(rid);
            return this;
        }

        public ConditionBuilder ridList(List<Integer> rid){
            this.ridList = rid;
            return this;
        }

        public ConditionBuilder fuzzyUid (List<String> fuzzyUid){
            this.fuzzyUid = fuzzyUid;
            return this;
        }

        public ConditionBuilder fuzzyUid (String ... fuzzyUid){
            this.fuzzyUid = solveNullList(fuzzyUid);
            return this;
        }

        public ConditionBuilder rightFuzzyUid (List<String> rightFuzzyUid){
            this.rightFuzzyUid = rightFuzzyUid;
            return this;
        }

        public ConditionBuilder rightFuzzyUid (String ... rightFuzzyUid){
            this.rightFuzzyUid = solveNullList(rightFuzzyUid);
            return this;
        }

        public ConditionBuilder uidList(String ... uid){
            this.uidList = solveNullList(uid);
            return this;
        }

        public ConditionBuilder uidList(List<String> uid){
            this.uidList = uid;
            return this;
        }

        public ConditionBuilder fuzzyAddress (List<String> fuzzyAddress){
            this.fuzzyAddress = fuzzyAddress;
            return this;
        }

        public ConditionBuilder fuzzyAddress (String ... fuzzyAddress){
            this.fuzzyAddress = solveNullList(fuzzyAddress);
            return this;
        }

        public ConditionBuilder rightFuzzyAddress (List<String> rightFuzzyAddress){
            this.rightFuzzyAddress = rightFuzzyAddress;
            return this;
        }

        public ConditionBuilder rightFuzzyAddress (String ... rightFuzzyAddress){
            this.rightFuzzyAddress = solveNullList(rightFuzzyAddress);
            return this;
        }

        public ConditionBuilder addressList(String ... address){
            this.addressList = solveNullList(address);
            return this;
        }

        public ConditionBuilder addressList(List<String> address){
            this.addressList = address;
            return this;
        }

        public ConditionBuilder fuzzyRname (List<String> fuzzyRname){
            this.fuzzyRname = fuzzyRname;
            return this;
        }

        public ConditionBuilder fuzzyRname (String ... fuzzyRname){
            this.fuzzyRname = solveNullList(fuzzyRname);
            return this;
        }

        public ConditionBuilder rightFuzzyRname (List<String> rightFuzzyRname){
            this.rightFuzzyRname = rightFuzzyRname;
            return this;
        }

        public ConditionBuilder rightFuzzyRname (String ... rightFuzzyRname){
            this.rightFuzzyRname = solveNullList(rightFuzzyRname);
            return this;
        }

        public ConditionBuilder rnameList(String ... rname){
            this.rnameList = solveNullList(rname);
            return this;
        }

        public ConditionBuilder rnameList(List<String> rname){
            this.rnameList = rname;
            return this;
        }

        public ConditionBuilder fuzzyRphone (List<String> fuzzyRphone){
            this.fuzzyRphone = fuzzyRphone;
            return this;
        }

        public ConditionBuilder fuzzyRphone (String ... fuzzyRphone){
            this.fuzzyRphone = solveNullList(fuzzyRphone);
            return this;
        }

        public ConditionBuilder rightFuzzyRphone (List<String> rightFuzzyRphone){
            this.rightFuzzyRphone = rightFuzzyRphone;
            return this;
        }

        public ConditionBuilder rightFuzzyRphone (String ... rightFuzzyRphone){
            this.rightFuzzyRphone = solveNullList(rightFuzzyRphone);
            return this;
        }

        public ConditionBuilder rphoneList(String ... rphone){
            this.rphoneList = solveNullList(rphone);
            return this;
        }

        public ConditionBuilder rphoneList(List<String> rphone){
            this.rphoneList = rphone;
            return this;
        }

        private <T>List<T> solveNullList(T ... objs){
            if (objs != null){
            List<T> list = new ArrayList<>();
                for (T item : objs){
                    if (item != null){
                        list.add(item);
                    }
                }
                return list;
            }
            return null;
        }

        public ConditionBuilder build(){return this;}
    }

    public static class Builder {

        private TbAddress obj;

        public Builder(){
            this.obj = new TbAddress();
        }

        public Builder rid(Integer rid){
            this.obj.setRid(rid);
            return this;
        }
        public Builder uid(String uid){
            this.obj.setUid(uid);
            return this;
        }
        public Builder address(String address){
            this.obj.setAddress(address);
            return this;
        }
        public Builder rname(String rname){
            this.obj.setRname(rname);
            return this;
        }
        public Builder rphone(String rphone){
            this.obj.setRphone(rphone);
            return this;
        }
        public TbAddress build(){return obj;}
    }

}
