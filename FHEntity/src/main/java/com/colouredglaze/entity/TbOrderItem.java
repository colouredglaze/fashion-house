package com.colouredglaze.entity;
import java.io.Serializable;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.Map;
import java.util.List;
/**
*
*  @author author
*/
public class TbOrderItem implements Serializable {

    private static final long serialVersionUID = 1574592468369L;


    /**
    * 主键
    * 记录编号
    * isNullAble:0
    */
    private Integer rid;

    /**
    * 商品编号
    * isNullAble:0
    */
    private String number;

    /**
    * 商品价格
    * isNullAble:0
    */
    private Double price;

    /**
    * 数量
    * isNullAble:0
    */
    private Integer pcount;

    /**
    * 订单编号
    * isNullAble:0
    */
    private String oid;

    private String size;

    @Override
    public String toString() {
        return "TbOrderItem{" +
                "rid=" + rid +
                ", number='" + number + '\'' +
                ", price=" + price +
                ", pcount=" + pcount +
                ", oid='" + oid + '\'' +
                ", size='" + size + '\'' +
                '}';
    }

    public String getSize() {
        return size;
    }

    public void setSize(String size) {
        this.size = size;
    }

    public void setRid(Integer rid){this.rid = rid;}

    public Integer getRid(){return this.rid;}

    public void setNumber(String number){this.number = number;}

    public String getNumber(){return this.number;}

    public void setPrice(Double price){this.price = price;}

    public Double getPrice(){return this.price;}

    public void setPcount(Integer pcount){this.pcount = pcount;}

    public Integer getPcount(){return this.pcount;}

    public void setOid(String oid){this.oid = oid;}

    public String getOid(){return this.oid;}

    public static Builder Build(){return new Builder();}

    public static ConditionBuilder ConditionBuild(){return new ConditionBuilder();}

    public static UpdateBuilder UpdateBuild(){return new UpdateBuilder();}

    public static QueryBuilder QueryBuild(){return new QueryBuilder();}

    public static class UpdateBuilder {

        private TbOrderItem set;

        private ConditionBuilder where;

        public UpdateBuilder set(TbOrderItem set){
            this.set = set;
            return this;
        }

        public TbOrderItem getSet(){
            return this.set;
        }

        public UpdateBuilder where(ConditionBuilder where){
            this.where = where;
            return this;
        }

        public ConditionBuilder getWhere(){
            return this.where;
        }

        public UpdateBuilder build(){
            return this;
        }
    }

    public static class QueryBuilder extends TbOrderItem{
        /**
        * 需要返回的列
        */
        private Map<String,Object> fetchFields;

        public Map<String,Object> getFetchFields(){return this.fetchFields;}

        private List<Integer> ridList;

        public List<Integer> getRidList(){return this.ridList;}

        private Integer ridSt;

        private Integer ridEd;

        public Integer getRidSt(){return this.ridSt;}

        public Integer getRidEd(){return this.ridEd;}

        private List<String> numberList;

        public List<String> getNumberList(){return this.numberList;}


        private List<String> fuzzyNumber;

        public List<String> getFuzzyNumber(){return this.fuzzyNumber;}

        private List<String> rightFuzzyNumber;

        public List<String> getRightFuzzyNumber(){return this.rightFuzzyNumber;}
        private List<Double> priceList;

        public List<Double> getPriceList(){return this.priceList;}

        private Double priceSt;

        private Double priceEd;

        public Double getPriceSt(){return this.priceSt;}

        public Double getPriceEd(){return this.priceEd;}

        private List<Integer> pcountList;

        public List<Integer> getPcountList(){return this.pcountList;}

        private Integer pcountSt;

        private Integer pcountEd;

        public Integer getPcountSt(){return this.pcountSt;}

        public Integer getPcountEd(){return this.pcountEd;}

        private List<String> oidList;

        public List<String> getOidList(){return this.oidList;}


        private List<String> fuzzyOid;

        public List<String> getFuzzyOid(){return this.fuzzyOid;}

        private List<String> rightFuzzyOid;

        public List<String> getRightFuzzyOid(){return this.rightFuzzyOid;}
        private QueryBuilder (){
            this.fetchFields = new HashMap<>();
        }

        public QueryBuilder ridBetWeen(Integer ridSt,Integer ridEd){
            this.ridSt = ridSt;
            this.ridEd = ridEd;
            return this;
        }

        public QueryBuilder ridGreaterEqThan(Integer ridSt){
            this.ridSt = ridSt;
            return this;
        }
        public QueryBuilder ridLessEqThan(Integer ridEd){
            this.ridEd = ridEd;
            return this;
        }


        public QueryBuilder rid(Integer rid){
            setRid(rid);
            return this;
        }

        public QueryBuilder ridList(Integer ... rid){
            this.ridList = solveNullList(rid);
            return this;
        }

        public QueryBuilder ridList(List<Integer> rid){
            this.ridList = rid;
            return this;
        }

        public QueryBuilder fetchRid(){
            setFetchFields("fetchFields","rid");
            return this;
        }

        public QueryBuilder excludeRid(){
            setFetchFields("excludeFields","rid");
            return this;
        }

        public QueryBuilder fuzzyNumber (List<String> fuzzyNumber){
            this.fuzzyNumber = fuzzyNumber;
            return this;
        }

        public QueryBuilder fuzzyNumber (String ... fuzzyNumber){
            this.fuzzyNumber = solveNullList(fuzzyNumber);
            return this;
        }

        public QueryBuilder rightFuzzyNumber (List<String> rightFuzzyNumber){
            this.rightFuzzyNumber = rightFuzzyNumber;
            return this;
        }

        public QueryBuilder rightFuzzyNumber (String ... rightFuzzyNumber){
            this.rightFuzzyNumber = solveNullList(rightFuzzyNumber);
            return this;
        }

        public QueryBuilder number(String number){
            setNumber(number);
            return this;
        }

        public QueryBuilder numberList(String ... number){
            this.numberList = solveNullList(number);
            return this;
        }

        public QueryBuilder numberList(List<String> number){
            this.numberList = number;
            return this;
        }

        public QueryBuilder fetchNumber(){
            setFetchFields("fetchFields","number");
            return this;
        }

        public QueryBuilder excludeNumber(){
            setFetchFields("excludeFields","number");
            return this;
        }

        public QueryBuilder priceBetWeen(Double priceSt,Double priceEd){
            this.priceSt = priceSt;
            this.priceEd = priceEd;
            return this;
        }

        public QueryBuilder priceGreaterEqThan(Double priceSt){
            this.priceSt = priceSt;
            return this;
        }
        public QueryBuilder priceLessEqThan(Double priceEd){
            this.priceEd = priceEd;
            return this;
        }


        public QueryBuilder price(Double price){
            setPrice(price);
            return this;
        }

        public QueryBuilder priceList(Double ... price){
            this.priceList = solveNullList(price);
            return this;
        }

        public QueryBuilder priceList(List<Double> price){
            this.priceList = price;
            return this;
        }

        public QueryBuilder fetchPrice(){
            setFetchFields("fetchFields","price");
            return this;
        }

        public QueryBuilder excludePrice(){
            setFetchFields("excludeFields","price");
            return this;
        }

        public QueryBuilder pcountBetWeen(Integer pcountSt,Integer pcountEd){
            this.pcountSt = pcountSt;
            this.pcountEd = pcountEd;
            return this;
        }

        public QueryBuilder pcountGreaterEqThan(Integer pcountSt){
            this.pcountSt = pcountSt;
            return this;
        }
        public QueryBuilder pcountLessEqThan(Integer pcountEd){
            this.pcountEd = pcountEd;
            return this;
        }


        public QueryBuilder pcount(Integer pcount){
            setPcount(pcount);
            return this;
        }

        public QueryBuilder pcountList(Integer ... pcount){
            this.pcountList = solveNullList(pcount);
            return this;
        }

        public QueryBuilder pcountList(List<Integer> pcount){
            this.pcountList = pcount;
            return this;
        }

        public QueryBuilder fetchPcount(){
            setFetchFields("fetchFields","pcount");
            return this;
        }

        public QueryBuilder excludePcount(){
            setFetchFields("excludeFields","pcount");
            return this;
        }

        public QueryBuilder fuzzyOid (List<String> fuzzyOid){
            this.fuzzyOid = fuzzyOid;
            return this;
        }

        public QueryBuilder fuzzyOid (String ... fuzzyOid){
            this.fuzzyOid = solveNullList(fuzzyOid);
            return this;
        }

        public QueryBuilder rightFuzzyOid (List<String> rightFuzzyOid){
            this.rightFuzzyOid = rightFuzzyOid;
            return this;
        }

        public QueryBuilder rightFuzzyOid (String ... rightFuzzyOid){
            this.rightFuzzyOid = solveNullList(rightFuzzyOid);
            return this;
        }

        public QueryBuilder oid(String oid){
            setOid(oid);
            return this;
        }

        public QueryBuilder oidList(String ... oid){
            this.oidList = solveNullList(oid);
            return this;
        }

        public QueryBuilder oidList(List<String> oid){
            this.oidList = oid;
            return this;
        }

        public QueryBuilder fetchOid(){
            setFetchFields("fetchFields","oid");
            return this;
        }

        public QueryBuilder excludeOid(){
            setFetchFields("excludeFields","oid");
            return this;
        }
        private <T>List<T> solveNullList(T ... objs){
            if (objs != null){
            List<T> list = new ArrayList<>();
                for (T item : objs){
                    if (item != null){
                        list.add(item);
                    }
                }
                return list;
            }
            return null;
        }

        public QueryBuilder fetchAll(){
            this.fetchFields.put("AllFields",true);
            return this;
        }

        public QueryBuilder addField(String ... fields){
            List<String> list = new ArrayList<>();
            if (fields != null){
                for (String field : fields){
                    list.add(field);
                }
            }
            this.fetchFields.put("otherFields",list);
            return this;
        }
        @SuppressWarnings("unchecked")
        private void setFetchFields(String key,String val){
            Map<String,Boolean> fields= (Map<String, Boolean>) this.fetchFields.get(key);
            if (fields == null){
                fields = new HashMap<>();
            }
            fields.put(val,true);
            this.fetchFields.put(key,fields);
        }

        public TbOrderItem build(){return this;}
    }


    public static class ConditionBuilder{
        private List<Integer> ridList;

        public List<Integer> getRidList(){return this.ridList;}

        private Integer ridSt;

        private Integer ridEd;

        public Integer getRidSt(){return this.ridSt;}

        public Integer getRidEd(){return this.ridEd;}

        private List<String> numberList;

        public List<String> getNumberList(){return this.numberList;}


        private List<String> fuzzyNumber;

        public List<String> getFuzzyNumber(){return this.fuzzyNumber;}

        private List<String> rightFuzzyNumber;

        public List<String> getRightFuzzyNumber(){return this.rightFuzzyNumber;}
        private List<Double> priceList;

        public List<Double> getPriceList(){return this.priceList;}

        private Double priceSt;

        private Double priceEd;

        public Double getPriceSt(){return this.priceSt;}

        public Double getPriceEd(){return this.priceEd;}

        private List<Integer> pcountList;

        public List<Integer> getPcountList(){return this.pcountList;}

        private Integer pcountSt;

        private Integer pcountEd;

        public Integer getPcountSt(){return this.pcountSt;}

        public Integer getPcountEd(){return this.pcountEd;}

        private List<String> oidList;

        public List<String> getOidList(){return this.oidList;}


        private List<String> fuzzyOid;

        public List<String> getFuzzyOid(){return this.fuzzyOid;}

        private List<String> rightFuzzyOid;

        public List<String> getRightFuzzyOid(){return this.rightFuzzyOid;}

        public ConditionBuilder ridBetWeen(Integer ridSt,Integer ridEd){
            this.ridSt = ridSt;
            this.ridEd = ridEd;
            return this;
        }

        public ConditionBuilder ridGreaterEqThan(Integer ridSt){
            this.ridSt = ridSt;
            return this;
        }
        public ConditionBuilder ridLessEqThan(Integer ridEd){
            this.ridEd = ridEd;
            return this;
        }


        public ConditionBuilder ridList(Integer ... rid){
            this.ridList = solveNullList(rid);
            return this;
        }

        public ConditionBuilder ridList(List<Integer> rid){
            this.ridList = rid;
            return this;
        }

        public ConditionBuilder fuzzyNumber (List<String> fuzzyNumber){
            this.fuzzyNumber = fuzzyNumber;
            return this;
        }

        public ConditionBuilder fuzzyNumber (String ... fuzzyNumber){
            this.fuzzyNumber = solveNullList(fuzzyNumber);
            return this;
        }

        public ConditionBuilder rightFuzzyNumber (List<String> rightFuzzyNumber){
            this.rightFuzzyNumber = rightFuzzyNumber;
            return this;
        }

        public ConditionBuilder rightFuzzyNumber (String ... rightFuzzyNumber){
            this.rightFuzzyNumber = solveNullList(rightFuzzyNumber);
            return this;
        }

        public ConditionBuilder numberList(String ... number){
            this.numberList = solveNullList(number);
            return this;
        }

        public ConditionBuilder numberList(List<String> number){
            this.numberList = number;
            return this;
        }

        public ConditionBuilder priceBetWeen(Double priceSt,Double priceEd){
            this.priceSt = priceSt;
            this.priceEd = priceEd;
            return this;
        }

        public ConditionBuilder priceGreaterEqThan(Double priceSt){
            this.priceSt = priceSt;
            return this;
        }
        public ConditionBuilder priceLessEqThan(Double priceEd){
            this.priceEd = priceEd;
            return this;
        }


        public ConditionBuilder priceList(Double ... price){
            this.priceList = solveNullList(price);
            return this;
        }

        public ConditionBuilder priceList(List<Double> price){
            this.priceList = price;
            return this;
        }

        public ConditionBuilder pcountBetWeen(Integer pcountSt,Integer pcountEd){
            this.pcountSt = pcountSt;
            this.pcountEd = pcountEd;
            return this;
        }

        public ConditionBuilder pcountGreaterEqThan(Integer pcountSt){
            this.pcountSt = pcountSt;
            return this;
        }
        public ConditionBuilder pcountLessEqThan(Integer pcountEd){
            this.pcountEd = pcountEd;
            return this;
        }


        public ConditionBuilder pcountList(Integer ... pcount){
            this.pcountList = solveNullList(pcount);
            return this;
        }

        public ConditionBuilder pcountList(List<Integer> pcount){
            this.pcountList = pcount;
            return this;
        }

        public ConditionBuilder fuzzyOid (List<String> fuzzyOid){
            this.fuzzyOid = fuzzyOid;
            return this;
        }

        public ConditionBuilder fuzzyOid (String ... fuzzyOid){
            this.fuzzyOid = solveNullList(fuzzyOid);
            return this;
        }

        public ConditionBuilder rightFuzzyOid (List<String> rightFuzzyOid){
            this.rightFuzzyOid = rightFuzzyOid;
            return this;
        }

        public ConditionBuilder rightFuzzyOid (String ... rightFuzzyOid){
            this.rightFuzzyOid = solveNullList(rightFuzzyOid);
            return this;
        }

        public ConditionBuilder oidList(String ... oid){
            this.oidList = solveNullList(oid);
            return this;
        }

        public ConditionBuilder oidList(List<String> oid){
            this.oidList = oid;
            return this;
        }

        private <T>List<T> solveNullList(T ... objs){
            if (objs != null){
            List<T> list = new ArrayList<>();
                for (T item : objs){
                    if (item != null){
                        list.add(item);
                    }
                }
                return list;
            }
            return null;
        }

        public ConditionBuilder build(){return this;}
    }

    public static class Builder {

        private TbOrderItem obj;

        public Builder(){
            this.obj = new TbOrderItem();
        }

        public Builder rid(Integer rid){
            this.obj.setRid(rid);
            return this;
        }
        public Builder number(String number){
            this.obj.setNumber(number);
            return this;
        }
        public Builder price(Double price){
            this.obj.setPrice(price);
            return this;
        }
        public Builder pcount(Integer pcount){
            this.obj.setPcount(pcount);
            return this;
        }
        public Builder oid(String oid){
            this.obj.setOid(oid);
            return this;
        }
        public TbOrderItem build(){return obj;}
    }

}
