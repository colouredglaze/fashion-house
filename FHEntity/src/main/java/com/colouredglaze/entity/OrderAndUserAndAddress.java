package com.colouredglaze.entity;

import java.time.LocalDateTime;

/**
 * @author ZhangLin
 * @version 1.0
 * @date 2020/11/29 3:45
 */
public class OrderAndUserAndAddress {

    /**
     * 用户名
     * isNullAble:0
     */
    private String uname;

    /**
     * 地址
     * isNullAble:0
     */
    private String address;

    /**
     * 支付金额
     * isNullAble:1
     */
    private Double payment;

    /**
     * 下单时间
     * isNullAble:1
     */
    private java.time.LocalDateTime placed;

    /**
     * 发货时间
     * isNullAble:1
     */
    private java.time.LocalDateTime deliver;

    /**
     * 送达时间
     * isNullAble:1
     */
    private java.time.LocalDateTime handover;

    /**
     * 订单状态
     * isNullAble:0
     */
    private String statu;

    /**
     * 收件姓名
     * isNullAble:0
     */
    private String rname;

    /**
     * 收件电话
     * isNullAble:0
     */
    private String rphone;

    /**
     * 订单编号
     * isNullAble:0
     */
    private String oid;

    @Override
    public String toString() {
        return "OrderAndUserAndAddress{" +
                "uname='" + uname + '\'' +
                ", address='" + address + '\'' +
                ", payment=" + payment +
                ", placed=" + placed +
                ", deliver=" + deliver +
                ", handover=" + handover +
                ", statu='" + statu + '\'' +
                ", rname='" + rname + '\'' +
                ", rphone='" + rphone + '\'' +
                ", oid='" + oid + '\'' +
                '}';
    }

    public String getOid() {
        return oid;
    }

    public void setOid(String oid) {
        this.oid = oid;
    }

    public String getUname() {
        return uname;
    }

    public void setUname(String uname) {
        this.uname = uname;
    }

    public String getAddress() {
        return address;
    }

    public void setAddress(String address) {
        this.address = address;
    }

    public Double getPayment() {
        return payment;
    }

    public void setPayment(Double payment) {
        this.payment = payment;
    }

    public LocalDateTime getPlaced() {
        return placed;
    }

    public void setPlaced(LocalDateTime placed) {
        this.placed = placed;
    }

    public LocalDateTime getDeliver() {
        return deliver;
    }

    public void setDeliver(LocalDateTime deliver) {
        this.deliver = deliver;
    }

    public LocalDateTime getHandover() {
        return handover;
    }

    public void setHandover(LocalDateTime handover) {
        this.handover = handover;
    }

    public String getStatu() {
        return statu;
    }

    public void setStatu(String statu) {
        this.statu = statu;
    }

    public String getRname() {
        return rname;
    }

    public void setRname(String rname) {
        this.rname = rname;
    }

    public String getRphone() {
        return rphone;
    }

    public void setRphone(String rphone) {
        this.rphone = rphone;
    }
}
