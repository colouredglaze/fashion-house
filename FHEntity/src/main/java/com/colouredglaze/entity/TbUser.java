package com.colouredglaze.entity;
import java.io.Serializable;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.Map;
import java.util.List;
/**
*
*  @author author
*/
public class TbUser implements Serializable {

    private static final long serialVersionUID = 1574592478727L;


    /**
    * 主键
    * 账号（电话号码）
    * isNullAble:0
    */
    private String uid;

    /**
    * 密码
    * isNullAble:0
    */
    private String password;

    /**
    * 用户名
    * isNullAble:0
    */
    private String uname;

    /**
    * 角色(1为管理员)
    * isNullAble:0,defaultVal:0
    */
    private Integer role;


    public void setUid(String uid){this.uid = uid;}

    public String getUid(){return this.uid;}

    public void setPassword(String password){this.password = password;}

    public String getPassword(){return this.password;}

    public void setUname(String uname){this.uname = uname;}

    public String getUname(){return this.uname;}

    public void setRole(Integer role){this.role = role;}

    public Integer getRole(){return this.role;}
    @Override
    public String toString() {
        return "TbUser{" +
                "uid='" + uid + '\'' +
                "password='" + password + '\'' +
                "uname='" + uname + '\'' +
                "role='" + role + '\'' +
            '}';
    }

    public static Builder Build(){return new Builder();}

    public static ConditionBuilder ConditionBuild(){return new ConditionBuilder();}

    public static UpdateBuilder UpdateBuild(){return new UpdateBuilder();}

    public static QueryBuilder QueryBuild(){return new QueryBuilder();}

    public static class UpdateBuilder {

        private TbUser set;

        private ConditionBuilder where;

        public UpdateBuilder set(TbUser set){
            this.set = set;
            return this;
        }

        public TbUser getSet(){
            return this.set;
        }

        public UpdateBuilder where(ConditionBuilder where){
            this.where = where;
            return this;
        }

        public ConditionBuilder getWhere(){
            return this.where;
        }

        public UpdateBuilder build(){
            return this;
        }
    }

    public static class QueryBuilder extends TbUser{
        /**
        * 需要返回的列
        */
        private Map<String,Object> fetchFields;

        public Map<String,Object> getFetchFields(){return this.fetchFields;}

        private List<String> uidList;

        public List<String> getUidList(){return this.uidList;}


        private List<String> fuzzyUid;

        public List<String> getFuzzyUid(){return this.fuzzyUid;}

        private List<String> rightFuzzyUid;

        public List<String> getRightFuzzyUid(){return this.rightFuzzyUid;}
        private List<String> passwordList;

        public List<String> getPasswordList(){return this.passwordList;}


        private List<String> fuzzyPassword;

        public List<String> getFuzzyPassword(){return this.fuzzyPassword;}

        private List<String> rightFuzzyPassword;

        public List<String> getRightFuzzyPassword(){return this.rightFuzzyPassword;}
        private List<String> unameList;

        public List<String> getUnameList(){return this.unameList;}


        private List<String> fuzzyUname;

        public List<String> getFuzzyUname(){return this.fuzzyUname;}

        private List<String> rightFuzzyUname;

        public List<String> getRightFuzzyUname(){return this.rightFuzzyUname;}
        private List<Integer> roleList;

        public List<Integer> getRoleList(){return this.roleList;}

        private Integer roleSt;

        private Integer roleEd;

        public Integer getRoleSt(){return this.roleSt;}

        public Integer getRoleEd(){return this.roleEd;}

        private QueryBuilder (){
            this.fetchFields = new HashMap<>();
        }

        public QueryBuilder fuzzyUid (List<String> fuzzyUid){
            this.fuzzyUid = fuzzyUid;
            return this;
        }

        public QueryBuilder fuzzyUid (String ... fuzzyUid){
            this.fuzzyUid = solveNullList(fuzzyUid);
            return this;
        }

        public QueryBuilder rightFuzzyUid (List<String> rightFuzzyUid){
            this.rightFuzzyUid = rightFuzzyUid;
            return this;
        }

        public QueryBuilder rightFuzzyUid (String ... rightFuzzyUid){
            this.rightFuzzyUid = solveNullList(rightFuzzyUid);
            return this;
        }

        public QueryBuilder uid(String uid){
            setUid(uid);
            return this;
        }

        public QueryBuilder uidList(String ... uid){
            this.uidList = solveNullList(uid);
            return this;
        }

        public QueryBuilder uidList(List<String> uid){
            this.uidList = uid;
            return this;
        }

        public QueryBuilder fetchUid(){
            setFetchFields("fetchFields","uid");
            return this;
        }

        public QueryBuilder excludeUid(){
            setFetchFields("excludeFields","uid");
            return this;
        }

        public QueryBuilder fuzzyPassword (List<String> fuzzyPassword){
            this.fuzzyPassword = fuzzyPassword;
            return this;
        }

        public QueryBuilder fuzzyPassword (String ... fuzzyPassword){
            this.fuzzyPassword = solveNullList(fuzzyPassword);
            return this;
        }

        public QueryBuilder rightFuzzyPassword (List<String> rightFuzzyPassword){
            this.rightFuzzyPassword = rightFuzzyPassword;
            return this;
        }

        public QueryBuilder rightFuzzyPassword (String ... rightFuzzyPassword){
            this.rightFuzzyPassword = solveNullList(rightFuzzyPassword);
            return this;
        }

        public QueryBuilder password(String password){
            setPassword(password);
            return this;
        }

        public QueryBuilder passwordList(String ... password){
            this.passwordList = solveNullList(password);
            return this;
        }

        public QueryBuilder passwordList(List<String> password){
            this.passwordList = password;
            return this;
        }

        public QueryBuilder fetchPassword(){
            setFetchFields("fetchFields","password");
            return this;
        }

        public QueryBuilder excludePassword(){
            setFetchFields("excludeFields","password");
            return this;
        }

        public QueryBuilder fuzzyUname (List<String> fuzzyUname){
            this.fuzzyUname = fuzzyUname;
            return this;
        }

        public QueryBuilder fuzzyUname (String ... fuzzyUname){
            this.fuzzyUname = solveNullList(fuzzyUname);
            return this;
        }

        public QueryBuilder rightFuzzyUname (List<String> rightFuzzyUname){
            this.rightFuzzyUname = rightFuzzyUname;
            return this;
        }

        public QueryBuilder rightFuzzyUname (String ... rightFuzzyUname){
            this.rightFuzzyUname = solveNullList(rightFuzzyUname);
            return this;
        }

        public QueryBuilder uname(String uname){
            setUname(uname);
            return this;
        }

        public QueryBuilder unameList(String ... uname){
            this.unameList = solveNullList(uname);
            return this;
        }

        public QueryBuilder unameList(List<String> uname){
            this.unameList = uname;
            return this;
        }

        public QueryBuilder fetchUname(){
            setFetchFields("fetchFields","uname");
            return this;
        }

        public QueryBuilder excludeUname(){
            setFetchFields("excludeFields","uname");
            return this;
        }

        public QueryBuilder roleBetWeen(Integer roleSt,Integer roleEd){
            this.roleSt = roleSt;
            this.roleEd = roleEd;
            return this;
        }

        public QueryBuilder roleGreaterEqThan(Integer roleSt){
            this.roleSt = roleSt;
            return this;
        }
        public QueryBuilder roleLessEqThan(Integer roleEd){
            this.roleEd = roleEd;
            return this;
        }


        public QueryBuilder role(Integer role){
            setRole(role);
            return this;
        }

        public QueryBuilder roleList(Integer ... role){
            this.roleList = solveNullList(role);
            return this;
        }

        public QueryBuilder roleList(List<Integer> role){
            this.roleList = role;
            return this;
        }

        public QueryBuilder fetchRole(){
            setFetchFields("fetchFields","role");
            return this;
        }

        public QueryBuilder excludeRole(){
            setFetchFields("excludeFields","role");
            return this;
        }
        private <T>List<T> solveNullList(T ... objs){
            if (objs != null){
            List<T> list = new ArrayList<>();
                for (T item : objs){
                    if (item != null){
                        list.add(item);
                    }
                }
                return list;
            }
            return null;
        }

        public QueryBuilder fetchAll(){
            this.fetchFields.put("AllFields",true);
            return this;
        }

        public QueryBuilder addField(String ... fields){
            List<String> list = new ArrayList<>();
            if (fields != null){
                for (String field : fields){
                    list.add(field);
                }
            }
            this.fetchFields.put("otherFields",list);
            return this;
        }
        @SuppressWarnings("unchecked")
        private void setFetchFields(String key,String val){
            Map<String,Boolean> fields= (Map<String, Boolean>) this.fetchFields.get(key);
            if (fields == null){
                fields = new HashMap<>();
            }
            fields.put(val,true);
            this.fetchFields.put(key,fields);
        }

        public TbUser build(){return this;}
    }


    public static class ConditionBuilder{
        private List<String> uidList;

        public List<String> getUidList(){return this.uidList;}


        private List<String> fuzzyUid;

        public List<String> getFuzzyUid(){return this.fuzzyUid;}

        private List<String> rightFuzzyUid;

        public List<String> getRightFuzzyUid(){return this.rightFuzzyUid;}
        private List<String> passwordList;

        public List<String> getPasswordList(){return this.passwordList;}


        private List<String> fuzzyPassword;

        public List<String> getFuzzyPassword(){return this.fuzzyPassword;}

        private List<String> rightFuzzyPassword;

        public List<String> getRightFuzzyPassword(){return this.rightFuzzyPassword;}
        private List<String> unameList;

        public List<String> getUnameList(){return this.unameList;}


        private List<String> fuzzyUname;

        public List<String> getFuzzyUname(){return this.fuzzyUname;}

        private List<String> rightFuzzyUname;

        public List<String> getRightFuzzyUname(){return this.rightFuzzyUname;}
        private List<Integer> roleList;

        public List<Integer> getRoleList(){return this.roleList;}

        private Integer roleSt;

        private Integer roleEd;

        public Integer getRoleSt(){return this.roleSt;}

        public Integer getRoleEd(){return this.roleEd;}


        public ConditionBuilder fuzzyUid (List<String> fuzzyUid){
            this.fuzzyUid = fuzzyUid;
            return this;
        }

        public ConditionBuilder fuzzyUid (String ... fuzzyUid){
            this.fuzzyUid = solveNullList(fuzzyUid);
            return this;
        }

        public ConditionBuilder rightFuzzyUid (List<String> rightFuzzyUid){
            this.rightFuzzyUid = rightFuzzyUid;
            return this;
        }

        public ConditionBuilder rightFuzzyUid (String ... rightFuzzyUid){
            this.rightFuzzyUid = solveNullList(rightFuzzyUid);
            return this;
        }

        public ConditionBuilder uidList(String ... uid){
            this.uidList = solveNullList(uid);
            return this;
        }

        public ConditionBuilder uidList(List<String> uid){
            this.uidList = uid;
            return this;
        }

        public ConditionBuilder fuzzyPassword (List<String> fuzzyPassword){
            this.fuzzyPassword = fuzzyPassword;
            return this;
        }

        public ConditionBuilder fuzzyPassword (String ... fuzzyPassword){
            this.fuzzyPassword = solveNullList(fuzzyPassword);
            return this;
        }

        public ConditionBuilder rightFuzzyPassword (List<String> rightFuzzyPassword){
            this.rightFuzzyPassword = rightFuzzyPassword;
            return this;
        }

        public ConditionBuilder rightFuzzyPassword (String ... rightFuzzyPassword){
            this.rightFuzzyPassword = solveNullList(rightFuzzyPassword);
            return this;
        }

        public ConditionBuilder passwordList(String ... password){
            this.passwordList = solveNullList(password);
            return this;
        }

        public ConditionBuilder passwordList(List<String> password){
            this.passwordList = password;
            return this;
        }

        public ConditionBuilder fuzzyUname (List<String> fuzzyUname){
            this.fuzzyUname = fuzzyUname;
            return this;
        }

        public ConditionBuilder fuzzyUname (String ... fuzzyUname){
            this.fuzzyUname = solveNullList(fuzzyUname);
            return this;
        }

        public ConditionBuilder rightFuzzyUname (List<String> rightFuzzyUname){
            this.rightFuzzyUname = rightFuzzyUname;
            return this;
        }

        public ConditionBuilder rightFuzzyUname (String ... rightFuzzyUname){
            this.rightFuzzyUname = solveNullList(rightFuzzyUname);
            return this;
        }

        public ConditionBuilder unameList(String ... uname){
            this.unameList = solveNullList(uname);
            return this;
        }

        public ConditionBuilder unameList(List<String> uname){
            this.unameList = uname;
            return this;
        }

        public ConditionBuilder roleBetWeen(Integer roleSt,Integer roleEd){
            this.roleSt = roleSt;
            this.roleEd = roleEd;
            return this;
        }

        public ConditionBuilder roleGreaterEqThan(Integer roleSt){
            this.roleSt = roleSt;
            return this;
        }
        public ConditionBuilder roleLessEqThan(Integer roleEd){
            this.roleEd = roleEd;
            return this;
        }


        public ConditionBuilder roleList(Integer ... role){
            this.roleList = solveNullList(role);
            return this;
        }

        public ConditionBuilder roleList(List<Integer> role){
            this.roleList = role;
            return this;
        }

        private <T>List<T> solveNullList(T ... objs){
            if (objs != null){
            List<T> list = new ArrayList<>();
                for (T item : objs){
                    if (item != null){
                        list.add(item);
                    }
                }
                return list;
            }
            return null;
        }

        public ConditionBuilder build(){return this;}
    }

    public static class Builder {

        private TbUser obj;

        public Builder(){
            this.obj = new TbUser();
        }

        public Builder uid(String uid){
            this.obj.setUid(uid);
            return this;
        }
        public Builder password(String password){
            this.obj.setPassword(password);
            return this;
        }
        public Builder uname(String uname){
            this.obj.setUname(uname);
            return this;
        }
        public Builder role(Integer role){
            this.obj.setRole(role);
            return this;
        }
        public TbUser build(){return obj;}
    }

}
