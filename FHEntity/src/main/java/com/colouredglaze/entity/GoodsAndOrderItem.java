package com.colouredglaze.entity;

/**
 * @author ZhangLin
 * @version 1.0
 * @date 2020/11/29 3:42
 */
public class GoodsAndOrderItem {

    /**
     * 主键
     * 商品编号
     * isNullAble:0
     */
    private String number;

    /**
     * 商品名称
     * isNullAble:0
     */
    private String name;

    /**
     * 商品价格
     * isNullAble:0
     */
    private Double price;

    /**
     * 数量
     * isNullAble:0
     */
    private Integer pcount;

    /**
     * 品牌
     * isNullAble:0
     */
    private String brand;

    /**
     * 材质
     * isNullAble:0
     */
    private String texture;

    private String size;

    @Override
    public String toString() {
        return "GoodsAndOrderItem{" +
                "number='" + number + '\'' +
                ", name='" + name + '\'' +
                ", price=" + price +
                ", pcount=" + pcount +
                ", brand='" + brand + '\'' +
                ", texture='" + texture + '\'' +
                ", size='" + size + '\'' +
                '}';
    }

    public String getSize() {
        return size;
    }

    public void setSize(String size) {
        this.size = size;
    }

    public String getNumber() {
        return number;
    }

    public void setNumber(String number) {
        this.number = number;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public Double getPrice() {
        return price;
    }

    public void setPrice(Double price) {
        this.price = price;
    }

    public String getBrand() {
        return brand;
    }

    public void setBrand(String brand) {
        this.brand = brand;
    }

    public String getTexture() {
        return texture;
    }

    public void setTexture(String texture) {
        this.texture = texture;
    }

    public Integer getPcount() {
        return pcount;
    }

    public void setPcount(Integer pcount) {
        this.pcount = pcount;
    }



}
