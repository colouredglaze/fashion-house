# FashionHouse

#### 介绍
  简单的购物网站 功能不多 如果有好的建议可以提交 或者自己下载改进 谢谢！

#### 软件架构
软件架构说明
  本设计是使用ssm框架设计而成，其中包括商品、管理员、和普通用户。

#### 安装教程

1.  下载该项目
    从git：https://gitee.com/colouredglaze/fashion-house.git
    
2.  配置tomcat9.0以上,需要在 tomcat 的webapps\下新建一个文件夹目录 img\user\product 和 img\user\slider
    ![img.png](FHCommon/img/img.png)
    
3.  最后配置图片路径
    ![img_1.png](FHCommon/img/img_1.png) ![img_2.png](FHCommon/img/img_2.png) ![img_3.png](FHCommon/img/img_3.png)![img_4.png](FHCommon/img/img_4.png)![img_5.png](FHCommon/img/img_5.png)

   4.  新建数据库连接最后再运行本项目中的.sql文件搭建数据库（本项目用的是mysql数据库）
       ![img_14.png](FHCommon/img/img_14.png)
