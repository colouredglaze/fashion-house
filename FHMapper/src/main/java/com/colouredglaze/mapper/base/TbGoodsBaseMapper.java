package com.colouredglaze.mapper.base;

import java.util.List;

import com.colouredglaze.entity.TbGoods;
import com.colouredglaze.entity.TbSize;

/**
*  @author author
*/
public interface TbGoodsBaseMapper {

    int insertTbGoods(TbGoods object);

    int insertTbSize(TbSize object);

    int updateTbGoods(TbGoods object);

    int update(TbGoods.UpdateBuilder object);

    List<TbGoods> queryTbGoods(TbGoods object);

    TbGoods queryTbGoodsLimit1(TbGoods object);

    TbGoods queryTbGoodsLimit(String number);

    int selectGoodsNumber();

}
