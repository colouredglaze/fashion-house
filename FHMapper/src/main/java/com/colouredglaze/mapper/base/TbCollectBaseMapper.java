package com.colouredglaze.mapper.base;

import java.util.List;

import com.colouredglaze.entity.TbCollect;
/**
*  @author author
*/
public interface TbCollectBaseMapper {

    int insertTbCollect(TbCollect object);

    int updateTbCollect(TbCollect object);

    int update(TbCollect.UpdateBuilder object);

    List<TbCollect> queryTbCollect(TbCollect object);

    TbCollect queryTbCollectLimit1(TbCollect object);

}
