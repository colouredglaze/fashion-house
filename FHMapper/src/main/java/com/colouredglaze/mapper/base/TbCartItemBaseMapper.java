package com.colouredglaze.mapper.base;

import com.colouredglaze.entity.CartItemAndGoods;
import com.colouredglaze.entity.TbCartItem;
import org.apache.ibatis.annotations.Param;

import java.util.List;
/**
*  @author author
*/
public interface TbCartItemBaseMapper {

    int insertTbCartItem(TbCartItem object);

    int updateTbCartItem(TbCartItem object);

    int update(TbCartItem.UpdateBuilder object);

    List<TbCartItem> queryTbCartItem(TbCartItem object);

    //查询购物车且商品信息
    List<CartItemAndGoods> queryCartItemAndGoods(String uid);

    CartItemAndGoods queryCartItemAndGoodsByGoods(@Param("uid") String uid,@Param("number") String number);

    //根据rid删除某条记录
    int deleteCartItem(Integer rid);

    //根据uid删除某个用户所有记录
    int deleteUserCartItem(String uid);

    TbCartItem queryTbCartItemLimit1(TbCartItem object);

}
