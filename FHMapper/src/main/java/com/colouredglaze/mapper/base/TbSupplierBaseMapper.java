package com.colouredglaze.mapper.base;

import com.colouredglaze.entity.TbSupplier;

import java.util.List;
/**
*  @author author
*/
public interface TbSupplierBaseMapper {

    int insertTbSupplier(TbSupplier object);

    int updateTbSupplier(TbSupplier object);

    int update(TbSupplier.UpdateBuilder object);

    List<TbSupplier> queryTbSupplier(TbSupplier object);

    TbSupplier queryTbSupplierLimit1(TbSupplier object);

}
