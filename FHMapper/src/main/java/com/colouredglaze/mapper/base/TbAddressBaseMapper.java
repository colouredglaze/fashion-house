package com.colouredglaze.mapper.base;

import com.colouredglaze.entity.TbAddress;

import java.util.List;

/**
*  @author author
*/
public interface TbAddressBaseMapper {

    int insertTbAddress(TbAddress object);

    int updateTbAddress(TbAddress object);

    int update(TbAddress.UpdateBuilder object);

    //删除一条收货地址
    int deleteAddress(Integer rid);

    List<TbAddress> queryTbAddress(TbAddress object);

    TbAddress queryTbAddressLimit1(TbAddress object);

}
