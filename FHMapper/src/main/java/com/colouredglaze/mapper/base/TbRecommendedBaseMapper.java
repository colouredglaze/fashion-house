package com.colouredglaze.mapper.base;

import java.util.List;

import com.colouredglaze.entity.TbRecommended;
/**
*  @author author
*/
public interface TbRecommendedBaseMapper {

    int insertTbRecommended(TbRecommended object);

    int updateTbRecommended(TbRecommended object);

    int update(TbRecommended.UpdateBuilder object);

    List<TbRecommended> queryTbRecommended(TbRecommended object);

    TbRecommended queryTbRecommendedLimit1(TbRecommended object);

}
