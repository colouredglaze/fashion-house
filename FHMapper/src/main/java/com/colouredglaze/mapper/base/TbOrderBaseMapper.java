package com.colouredglaze.mapper.base;

import com.colouredglaze.entity.OrderAndAddress;
import com.colouredglaze.entity.OrderAndUserAndAddress;
import com.colouredglaze.entity.TbOrder;

import java.util.List;
/**
*  @author author
*/
public interface TbOrderBaseMapper {

    int insertTbOrder(TbOrder object);

    int updateTbOrder(TbOrder object);

    int update(TbOrder.UpdateBuilder object);

    List<TbOrder> queryTbOrder(TbOrder object);

    //查询订单信息附带地址信息
    List<OrderAndAddress> selectOrderAndAddress(String uid);
    //List<TbOrder> selectOrderAndAddress2();

    //删除某条订单
    int deleteOrder(String oid);

    //查询带用户和地址的订单信息
    OrderAndUserAndAddress selectOrderAndUserAndAddress(String oid);

    //查询某条订单及其信息

    TbOrder queryTbOrderLimit1(TbOrder object);

}
