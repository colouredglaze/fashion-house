package com.colouredglaze.mapper.base;

import com.colouredglaze.entity.GoodsAndOrderItem;
import com.colouredglaze.entity.TbOrderItem;

import java.util.List;

/**
*  @author author
*/
public interface TbOrderItemBaseMapper {

    int insertTbOrderItem(TbOrderItem object);

    int updateTbOrderItem(TbOrderItem object);

    int update(TbOrderItem.UpdateBuilder object);

    List<TbOrderItem> queryTbOrderItem(TbOrderItem object);

    //查询订单项目对应的商品
    List<GoodsAndOrderItem> selectOrderItemAndGoods(String oid);

    TbOrderItem queryTbOrderItemLimit1(TbOrderItem object);

}
