package com.colouredglaze.mapper.base;

import java.util.List;

import com.colouredglaze.entity.TbUser;
/**
*  @author author
*/
public interface TbUserBaseMapper {

    int insertTbUser(TbUser object);

    int updateTbUser(TbUser object);

    int update(TbUser.UpdateBuilder object);

    List<TbUser> queryTbUser(TbUser object);

    TbUser queryTbUserLimit1(TbUser object);

}
