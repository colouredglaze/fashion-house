<%--
  Created by IntelliJ IDEA.
  User: ZhangLin
  Date: 2020/11/30
  Time: 19:36
--%>
<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<%@include file="../header.jsp"%>
<!DOCTYPE html>
<html>
<head>
    <meta charset="utf-8">
    <meta content="IE=edge" http-equiv="X-UA-Compatible">
    <title>订单处理</title><link rel="icon" href="<%=ctxPath%>/static/favicon.ico" mce_href="<%=ctxPath%>/static/favicon.ico" type="image/x-icon" />
    <!-- Tell the browser to be responsive to screen width -->
    <meta content="width=device-width, initial-scale=1, maximum-scale=1, user-scalable=no" name="viewport">
    <!-- page style-->
    <style>
        .my_itemtitle {
            font-size: 20px;
        }

        .my_info {
            width: 50% !important;
        }

        .my_radio {
            display: inline-block !important;
        }

        .my_formgroup {
            display: inline-block !important;
        }
    </style>
    <!-- Bootstrap 3.3.7 -->
    <link href="<%=ctxPath%>/admin/assets/css/bootstrap/bootstrap.min.css" rel="stylesheet">
    <!-- Font Awesome -->
    <link href="<%=ctxPath%>/admin/assets/css/font-awesome/font-awesome.min.css" rel="stylesheet">
    <!-- Ionicons -->
    <link href="<%=ctxPath%>/admin/assets/css/Ionicons/ionicons.min.css" rel="stylesheet">
    <!-- Theme style -->
    <link href="<%=ctxPath%>/admin/assets/css/AdminLTE/AdminLTE.min.css" rel="stylesheet">
    <!-- AdminLTE Skin -->
    <link href="<%=ctxPath%>/admin/assets/css/AdminLTE/skin/skin-blue.min.css" rel="stylesheet">
    <!-- Google Font -->
    <!-- <link rel="stylesheet" href="https://fonts.googleapis.com/css?family=Source+Sans+Pro:300,400,600,700,300italic,400italic,600italic"> -->
</head>
<body class="hold-transition skin-blue sidebar-mini">
<div class="wrapper">

    <!-- Main Header -->
    <header class="main-header">

        <!-- Logo -->
        <a class="logo" href="#">
            <!-- mini logo for sidebar mini 50x50 pixels -->
            <span class="logo-mini"><b>购物</b></span>
            <!-- logo for regular state and mobile devices -->
            <span class="logo-lg">时装邮购管理系统</span>
        </a>

        <!-- Header Navbar -->
        <nav class="navbar navbar-static-top" role="navigation">
            <!-- Sidebar toggle button-->
            <a class="sidebar-toggle" data-toggle="push-menu" href="#" role="button">
                <span class="sr-only">导航切换</span>
            </a>
            <!-- Navbar Right Menu -->
            <div class="navbar-custom-menu">
                <ul class="nav navbar-nav">
                    <!-- Messages: style can be found in dropdown.less-->
                    <!-- /.messages-menu -->
                    <!-- User Account Menu -->
                    <li class="dropdown user user-menu">
                        <!-- Menu Toggle Button -->
                        <a class="dropdown-toggle" data-toggle="dropdown" href="#">
                            <!-- The user image in the navbar-->
                            <img alt="User Image" class="user-image" src="<%=ctxPath%>/admin/assets/img/setting.png">
                            <!-- hidden-xs hides the username on small devices so only the image appears. -->
                            <span class="hidden-xs">${user.uname}</span>
                        </a>
                        <ul class="dropdown-menu">
                            <!-- The user image in the menu -->
                            <li class="user-header">
                                <img alt="User Image" class="img-circle" src="<%=ctxPath%>/admin/assets/img/word.jpg">
                                <p>让学习成为一种习惯</p>
                            </li>
                            <!-- Menu Footer-->
                            <li class="user-footer">
                                <div class="pull-left">
                                    <a class="btn btn-default btn-flat"
                                       href="<%=ctxPath%>/jsp/admin/password-change.jsp">修改密码</a>
                                </div>
                                <div class="pull-right">
                                    <a class="btn btn-default btn-flat" href="<%=ctxPath%>/logout.do">退出</a>
                                </div>
                            </li>
                        </ul>
                    </li>

                </ul>
            </div>
        </nav>
    </header>
    <!-- Left side column. contains the logo and sidebar -->
    <aside class="main-sidebar">

        <!-- sidebar: style can be found in sidebar.less -->
        <section class="sidebar">
            <!-- Sidebar Menu -->
            <ul class="sidebar-menu" data-widget="tree">
                <li class="header">&nbsp;</li>
                <!-- Optionally, you can add icons to the links -->
                <li class="treeview">
                    <a href="#"><i class="fa fa-link"></i> <span>订单管理</span>
                        <span class="pull-right-container">
                <i class="fa fa-angle-left pull-right"></i>
              </span>
                    </a>
                    <ul class="treeview-menu">
                        <li><a href="<%=ctxPath%>/Order/">所有订单</a></li>
                        <li><a href="<%=ctxPath%>/Order/?statu=待发货">待发货</a></li>
                        <li><a href="<%=ctxPath%>/Order/?statu=已发货">已发货</a></li>
                        <li><a href="<%=ctxPath%>/Order/?statu=已送达">已送达</a></li>
                    </ul>
                </li>
                <li class="treeview">
                    <a href="#"><i class="fa fa-link"></i> <span>商品管理</span>
                        <span class="pull-right-container">
                <i class="fa fa-angle-left pull-right"></i>
              </span>
                    </a>
                    <ul class="treeview-menu">
                        <li><a href="<%=ctxPath%>/Goods/AllGoods">所有商品</a></li>
                        <li><a href="<%=ctxPath%>/Goods/productAdd">添加商品</a></li>
                    </ul>
                </li>
            </ul>
            <!-- /.sidebar-menu -->
        </section>
        <!-- /.sidebar -->
    </aside>

    <!-- Content Wrapper. Contains page content -->
    <div class="content-wrapper">
        <!-- Content Header (Page header) -->
        <section class="content-header">
            <h1><small></small></h1>
            <ol class="breadcrumb">
                <li><i class="fa fa-dashboard">&nbsp;订单管理</i></li>
                <li><i class="fa"></i>所有订单</li>
                <li class="active">处理订单</li>
            </ol>
        </section>
        <!-- Main content -->
        <section class="invoice">
            <!-- title row -->
            <div class="row">
                <div class="col-xs-12">
                    <h2 class="page-header">
                        <i class="fa"></i>订单详情
                        <small class="pull-right">日期: 2017-10-10</small>
                    </h2>
                </div>
                <!-- /.col -->
            </div>
            <!-- info row -->
            <div class="row invoice-info">
                <div class="col-sm-4 invoice-col my_info">
                    <b class="my_itemtitle">收件人</b><br>
                    <br>
                    <b>姓名：</b>${list.get(0).rname}<br>
                    <b>地址：</b>${list.get(0).address}<br>
                    <b>电话：</b>${list.get(0).rphone}<br>
                </div>
                <!-- /.col -->
                <div class="col-sm-4 invoice-col my_info">
                    <b class="my_itemtitle">#${list.get(0).oid}</b><br>
                    <br>
                    <b>创建时间：</b>
                    <div id="rep1" style="display: inline-block;">${list.get(0).placed}</div>
                    <br>
                    <b>订单总额：</b>￥${list.get(0).payment}<br>
                    <b>订单状态：</b>
                    <!-- radio -->
                    <div class="form-group my_formgroup">
                        <form action="<%=ctxPath%>/Order/orderProcess" method="post">
                            <input type="hidden" name="oid" value="${list.get(0).oid}"/>
                            <div class="radio my_radio">
                                <label>
                                    <input id="statu1" name="statu" type="radio"
                                           <c:if test="${list.get(0).statu eq '待发货'}">checked</c:if>
                                           <c:if test="${list.get(0).statu eq '已发货'}">disabled</c:if>
                                           <c:if test="${list.get(0).statu eq '已送达'}">disabled</c:if>
                                           value="待发货">待发货
                                </label>
                            </div>
                            <div class="radio my_radio">
                                <label>
                                    <input id="statu2" name="statu" type="radio"
                                    <c:if test="${list.get(0).statu eq '待发货'}"></c:if>
                                           <c:if test="${list.get(0).statu eq '已发货'}">checked</c:if>
                                           <c:if test="${list.get(0).statu eq '已送达'}">disabled</c:if>
                                           value="已发货">已发货
                                </label>
                            </div>
                            <div class="radio my_radio">
                                <label>
                                    <input id="statu3" name="statu" type="radio"
                                           <c:if test="${list.get(0).statu eq '待发货'}">disabled</c:if>
                                    <c:if test="${list.get(0).statu eq '已发货'}"></c:if>
                                           <c:if test="${list.get(0).statu eq '已送达'}">checked</c:if>
                                           value="已送达">已送达
                                </label>
                            </div>
                            <div class="radio my_radio">
                                <button class="btn btn-default" type="submit">保存</button>
                            </div>
                        </form>
                    </div>
                </div>
                <!-- /.col -->
            </div>
            <hr>
            <!-- /.row -->
            <!-- Table row -->
            <div class="row">
                <div class="col-xs-12 table-responsive">
                    <table class="table table-striped">
                        <thead>
                        <tr>
                            <th>数量</th>
                            <th>名称</th>
                            <th>商品编号</th>
                            <th>供应商</th>
                            <th>材质</th>
                            <th>尺寸</th>
                            <th>单价</th>
                        </tr>
                        </thead>
                        <tbody>
                        <c:forEach items="${list.get(1)}" var="goods">
                            <tr>
                                <td>${goods.pcount}</td>
                                <td>${goods.name}</td>
                                <td>${goods.number}</td>
                                <td>${goods.brand}</td>
                                <td>${goods.texture}</td>
                                <td>${goods.size}</td>
                                <td>￥${goods.price}</td>
                            </tr>
                        </c:forEach>
                        </tbody>
                    </table>
                </div>
                <!-- /.col -->
            </div>
            <!-- /.row -->
        </section>
        <!-- /.content -->
        <div class="clearfix"></div>
    </div>
    <!-- /.content-wrapper -->

    <!-- Main Footer -->
    <footer class="main-footer">
        <!-- To the right -->
        <div class="pull-right hidden-xs">
            缔造年轻人的中国梦
        </div>
        <!-- Default to the left -->
        <strong>Copyright &copy; 2021 <a href="#">colouredglaze</a>.</strong> All rights reserved.
    </footer>

    <!-- /.control-sidebar -->
    <!-- Add the sidebar's background. This div must be placed
    immediately after the control sidebar -->
    <div class="control-sidebar-bg"></div>
</div>
<!-- ./wrapper -->

<!-- jQuery 3 -->
<script src="<%=ctxPath%>/admin/assets/js/jquery/jquery.min.js"></script>
<!-- Bootstrap 3.3.7 -->
<script src="<%=ctxPath%>/admin/assets/js/bootstrap/bootstrap.min.js"></script>
<!-- DataTables -->
<script src="<%=ctxPath%>/admin/assets/js/datatables.net/jquery.dataTables.min.js"></script>
<script src="<%=ctxPath%>/admin/assets/js/datatables.net-bs/dataTables.bootstrap.min.js"></script>
<!-- FastClick -->
<script src="<%=ctxPath%>/admin/assets/js/fastclick/fastclick.js"></script>
<!-- AdminLTE App -->
<script src="<%=ctxPath%>/admin/assets/js/AdminLTE/adminlte.min.js"></script>
<!-- page script -->
<script>

    //改变时间格式
    window.onload = function () {
        $("#rep1").text($("#rep1").text().replace("T", " "));
    };

</script>
</body>
</html>

