<%--
  Created by IntelliJ IDEA.
  User: ZhangLin
  Date: 2020/11/30
  Time: 19:40
--%>
<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<%@include file="../header.jsp"%>
<!DOCTYPE html>
<html>
<head>
    <meta charset="utf-8">
    <meta content="IE=edge" http-equiv="X-UA-Compatible">
    <title>商品编辑</title><link rel="icon" href="<%=ctxPath%>/static/favicon.ico" mce_href="<%=ctxPath%>/static/favicon.ico" type="image/x-icon" />
    <!-- Tell the browser to be responsive to screen width -->
    <meta content="width=device-width, initial-scale=1, maximum-scale=1, user-scalable=no" name="viewport">
    <!-- page style -->
    <style>
        .my-input {
            width: 22.77777778% !important;
        }

        .my-label {
            width: 10.55555555% !important;
        }

        .my-img {
            padding-right: 2.0% !important;
            padding-left: 8.0% !important;
        }

        .datepicker {
            z-index: 9999 !important;
        }
    </style>
    <!-- Bootstrap 3.3.7 -->
    <link href="<%=ctxPath%>/admin/assets/css/bootstrap/bootstrap.min.css" rel="stylesheet">
    <!-- Font Awesome -->
    <link href="<%=ctxPath%>/admin/assets/css/font-awesome/font-awesome.min.css" rel="stylesheet">
    <!-- Ionicons -->
    <link href="<%=ctxPath%>/admin/assets/css/Ionicons/ionicons.min.css" rel="stylesheet">
    <!-- Theme style -->
    <link href="<%=ctxPath%>/admin/assets/css/AdminLTE/AdminLTE.min.css" rel="stylesheet">
    <!-- AdminLTE Skin -->
    <link href="<%=ctxPath%>/admin/assets/css/AdminLTE/skin/skin-blue.min.css" rel="stylesheet">
    <!-- Google Font -->
    <!-- <link rel="stylesheet" href="https://fonts.googleapis.com/css?family=Source+Sans+Pro:300,400,600,700,300italic,400italic,600italic"> -->
</head>
<body class="hold-transition skin-blue sidebar-mini">
<div class="wrapper">

    <!-- Main Header -->
    <header class="main-header">

        <!-- Logo -->
        <a class="logo" href="#">
            <!-- mini logo for sidebar mini 50x50 pixels -->
            <span class="logo-mini"><b>购物</b></span>
            <!-- logo for regular state and mobile devices -->
            <span class="logo-lg">时装邮购管理系统</span>
        </a>

        <!-- Header Navbar -->
        <nav class="navbar navbar-static-top" role="navigation">
            <!-- Sidebar toggle button-->
            <a class="sidebar-toggle" data-toggle="push-menu" href="#" role="button">
                <span class="sr-only">导航切换</span>
            </a>
            <!-- Navbar Right Menu -->
            <div class="navbar-custom-menu">
                <ul class="nav navbar-nav">
                    <!-- Messages: style can be found in dropdown.less-->
                    <!-- /.messages-menu -->
                    <!-- User Account Menu -->
                    <li class="dropdown user user-menu">
                        <!-- Menu Toggle Button -->
                        <a class="dropdown-toggle" data-toggle="dropdown" href="#">
                            <!-- The user image in the navbar-->
                            <img alt="User Image" class="user-image" src="<%=ctxPath%>/admin/assets/img/setting.png">
                            <!-- hidden-xs hides the username on small devices so only the image appears. -->
                            <span class="hidden-xs">${user.uname}</span>
                        </a>
                        <ul class="dropdown-menu">
                            <!-- The user image in the menu -->
                            <li class="user-header">
                                <img alt="User Image" class="img-circle" src="<%=ctxPath%>/admin/assets/img/word.jpg">
                                <p>让学习成为一种习惯</p>
                            </li>
                            <!-- Menu Footer-->
                            <li class="user-footer">
                                <div class="pull-left">
                                    <a class="btn btn-default btn-flat"
                                       href="<%=ctxPath%>/jsp/admin/password-change.jsp">修改密码</a>
                                </div>
                                <div class="pull-right">
                                    <a class="btn btn-default btn-flat" href="<%=ctxPath%>/logout.do">退出</a>
                                </div>
                            </li>
                        </ul>
                    </li>
                </ul>
            </div>
        </nav>
    </header>
    <!-- Left side column. contains the logo and sidebar -->
    <aside class="main-sidebar">

        <!-- sidebar: style can be found in sidebar.less -->
        <section class="sidebar">
            <!-- Sidebar Menu -->
            <ul class="sidebar-menu" data-widget="tree">
                <li class="header">&nbsp;</li>
                <!-- Optionally, you can add icons to the links -->
                <li class="treeview">
                    <a href="#"><i class="fa fa-link"></i> <span>订单管理</span>
                        <span class="pull-right-container">
                <i class="fa fa-angle-left pull-right"></i>
              </span>
                    </a>
                    <ul class="treeview-menu">
                        <li><a href="<%=ctxPath%>/Order/">所有订单</a></li>
                        <li><a href="<%=ctxPath%>/Order/?statu=待发货">待发货</a></li>
                        <li><a href="<%=ctxPath%>/Order/?statu=已发货">已发货</a></li>
                        <li><a href="<%=ctxPath%>/Order/?statu=已送达">已送达</a></li>
                    </ul>
                </li>
                <li class="treeview">
                    <a href="#"><i class="fa fa-link"></i> <span>商品管理</span>
                        <span class="pull-right-container">
                <i class="fa fa-angle-left pull-right"></i>
              </span>
                    </a>
                    <ul class="treeview-menu">
                        <li><a href="<%=ctxPath%>/Goods/AllGoods">所有商品</a></li>
                        <li><a href="<%=ctxPath%>/Goods/productAdd">添加商品</a></li>
                    </ul>
                </li>
            </ul>
            <!-- /.sidebar-menu -->
        </section>
        <!-- /.sidebar -->
    </aside>

    <!-- Content Wrapper. Contains page content -->
    <div class="content-wrapper">
        <!-- Content Header (Page header) -->
        <section class="content-header">
            <h1><small></small></h1>
            <ol class="breadcrumb">
                <li><i class="fa fa-dashboard">&nbsp;商品管理</i></li>
                <li><i class="fa"></i>所有商品</li>
                <li class="active">商品编辑</li>
            </ol>
        </section>
        <!-- Main content -->
        <section class="content container-fluid">
            <!-- Horizontal Form -->
            <div class="box box-primary">
                <div class="box-header with-border">
                    <h3 class="box-title">详细编辑</h3>
                </div>
                <!-- /.box-header -->
                <!-- form start -->
                <form action="<%=ctxPath%>/Goods/goodsEdit" class="form-horizontal" role="form" method="post">
                    <div class="box-body">
                        <div class="form-group">
                            <label class="col-sm-2 control-label my-label" for="number">商品编号</label>
                            <div class="col-sm-10 my-input">
                                <input class="form-control" id="number" name="number" readonly type="text"
                                       value="${goods.number}">
                            </div>
                            <label class="col-sm-2 control-label my-label" for="title">商品名</label>
                            <div class="col-sm-10 my-input">
                                <input class="form-control" id="title" name="title" readonly type="text"
                                       value="${goods.name}">
                            </div>
                            <label class="col-sm-2 control-label my-label" for="count">余量</label>
                            <div class="col-sm-10 my-input">
                                <input class="form-control" id="count" required name="count" type="text"
                                       value="${goods.count}">
                            </div>
                        </div>
                        <div class="form-group">
                            <label class="col-sm-2 control-label my-label" for="price">价格</label>
                            <div class="col-sm-10 my-input">
                                <input class="form-control" id="price" required name="price" type="text"
                                       value="${goods.price}">
                            </div>
                            <label class="col-sm-2 control-label my-label" for="pages">供应商</label>
                            <div class="col-sm-10 my-input">
                                <input class="form-control" id="pages" name="pages" readonly type="text"
                                       value="${goods.brand}">
                            </div>
                            <label class="col-sm-2 control-label my-label" for="words">材质</label>
                            <div class="col-sm-10 my-input">
                                <input class="form-control" id="words" name="words" readonly type="text"
                                       value="${goods.texture}">
                            </div>
                        </div>
                        </div>
                    <div class="form-group">
                        <div class="col-sm-4 my-img">
                            <img alt="Detail 2 Photo is missing"
                                 height="384px" width="216px"
                                 class="img-responsive"
                                 src="/img/user/product/${goods.number}.jpg">
                        </div>
                        <div class="col-sm-4 my-img">
                        </div>
                        <label class="col-sm-2 control-label my-label" for="describe">描述</label>
                        <div class="col-sm-10 my-input">
                            <input class="form-control" id="describe" name="describe"  type="text"
                                   value="${goods.describe}">
                        </div>
                    </div>
                    <!-- /.box-body -->
                    <div class="box-footer">
                        <button class="btn btn-primary pull-right" type="submit">提交</button>
                    </div>
                    <!-- /.box-footer -->
                </form>
            </div>
        </section>
        <!-- /.content -->
    </div>
    <!-- /.content-wrapper -->

    <!-- Main Footer -->
    <footer class="main-footer">
        <!-- To the right -->
        <div class="pull-right hidden-xs">
            缔造年轻人的中国梦
        </div>
        <!-- Default to the left -->
        <strong>Copyright &copy; 2021 <a href="#">colouredglaze</a>.</strong> All rights reserved.
    </footer>

    <!-- /.control-sidebar -->
    <!-- Add the sidebar's background. This div must be placed
    immediately after the control sidebar -->
    <div class="control-sidebar-bg"></div>
</div>
<!-- ./wrapper -->

<!-- jQuery 3 -->
<script src="<%=ctxPath%>/admin/assets/js/jquery/jquery.min.js"></script>
<!-- Bootstrap 3.3.7 -->
<script src="<%=ctxPath%>/admin/assets/js/bootstrap/bootstrap.min.js"></script>
<!-- AdminLTE App -->
<script src="<%=ctxPath%>/admin/assets/js/AdminLTE/adminlte.min.js"></script>
<!-- page script -->
<script>

</script>
</body>
</html>

