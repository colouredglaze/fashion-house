<%--
  Created by IntelliJ IDEA.
  User: ZhangLin
  Date: 2020/11/30
  Time: 19:35
--%>
<%@ page contentType="text/html;charset=UTF-8" language="java" pageEncoding="UTF-8"%>
<%@include file="../header.jsp"%>
<!DOCTYPE html>
<html>
<head>
    <meta charset="utf-8">
    <meta content="IE=edge" http-equiv="X-UA-Compatible">
    <title>所有订单</title><link rel="icon" href="<%=ctxPath%>/static/favicon.ico" mce_href="<%=ctxPath%>/static/favicon.ico" type="image/x-icon" />
    <!-- Tell the browser to be responsive to screen width -->
    <meta content="width=device-width, initial-scale=1, maximum-scale=1, user-scalable=no" name="viewport">
    <!-- Bootstrap 3.3.7 -->
    <link href="<%=ctxPath%>/admin/assets/css/bootstrap/bootstrap.min.css" rel="stylesheet">
    <!-- Font Awesome -->
    <link href="<%=ctxPath%>/admin/assets/css/font-awesome/font-awesome.min.css" rel="stylesheet">
    <!-- Ionicons -->
    <link href="<%=ctxPath%>/admin/assets/css/Ionicons/ionicons.min.css" rel="stylesheet">
    <!-- DataTables -->
    <link href="<%=ctxPath%>/admin/assets/css/datatables.net-bs/dataTables.bootstrap.min.css" rel="stylesheet">
    <!-- Theme style -->
    <link href="<%=ctxPath%>/admin/assets/css/AdminLTE/AdminLTE.min.css" rel="stylesheet">
    <!-- AdminLTE Skin -->
    <link href="<%=ctxPath%>/admin/assets/css/AdminLTE/skin/skin-blue.min.css" rel="stylesheet">
    <!-- Google Font -->
    <!-- <link rel="stylesheet" href="https://fonts.googleapis.com/css?family=Source+Sans+Pro:300,400,600,700,300italic,400italic,600italic"> -->
</head>
<body class="hold-transition skin-blue sidebar-mini">
<div class="wrapper">

    <!-- Main Header -->
    <header class="main-header">

        <!-- Logo -->
        <a class="logo" href="#">
            <!-- mini logo for sidebar mini 50x50 pixels -->
            <span class="logo-mini"><b>购物</b></span>
            <!-- logo for regular state and mobile devices -->
            <span class="logo-lg">时装邮购管理系统</span>
        </a>

        <!-- Header Navbar -->
        <nav class="navbar navbar-static-top" role="navigation">
            <!-- Sidebar toggle button-->
            <a class="sidebar-toggle" data-toggle="push-menu" href="#" role="button">
                <span class="sr-only">导航切换</span>
            </a>
            <!-- Navbar Right Menu -->
            <div class="navbar-custom-menu">
                <ul class="nav navbar-nav">
                    <!-- Messages: style can be found in dropdown.less-->
                    <!-- /.messages-menu -->
                    <!-- User Account Menu -->
                    <li class="dropdown user user-menu">
                        <!-- Menu Toggle Button -->
                        <a class="dropdown-toggle" data-toggle="dropdown" href="#">
                            <!-- The user image in the navbar-->
                            <img alt="User Image" class="user-image" src="<%=ctxPath%>/admin/assets/img/setting.png">
                            <!-- hidden-xs hides the username on small devices so only the image appears. -->
                            <span class="hidden-xs">${user.uname}</span>
                        </a>
                        <ul class="dropdown-menu">
                            <!-- The user image in the menu -->
                            <li class="user-header">
                                <img alt="User Image" class="img-circle" src="<%=ctxPath%>/admin/assets/img/word.jpg">
                                <p>让学习成为一种习惯</p>
                            </li>
                            <!-- Menu Footer-->
                            <li class="user-footer">
                                <div class="pull-left">
                                    <a class="btn btn-default btn-flat"
                                       href="<%=ctxPath%>/jsp/admin/password-change.jsp">修改密码</a>
                                </div>
                                <div class="pull-right">
                                    <a class="btn btn-default btn-flat" href="<%=ctxPath%>/logout.do">退出</a>
                                </div>
                            </li>
                        </ul>
                    </li>
                </ul>
            </div>
        </nav>
    </header>
    <!-- Left side column. contains the logo and sidebar -->
    <aside class="main-sidebar">

        <!-- sidebar: style can be found in sidebar.less -->
        <section class="sidebar">
            <!-- Sidebar Menu -->
            <ul class="sidebar-menu" data-widget="tree">
                <li class="header">&nbsp;</li>
                <!-- Optionally, you can add icons to the links -->
                <li class="treeview">
                    <a href="#"><i class="fa fa-link"></i> <span>订单管理</span>
                        <span class="pull-right-container">
                <i class="fa fa-angle-left pull-right"></i>
              </span>
                    </a>
                    <ul class="treeview-menu">
                        <li <c:if test="${statu eq null}">class="active"</c:if>><a href="<%=ctxPath%>/Order/">所有订单</a>
                        </li>
                        <li <c:if test="${statu eq '待发货'}">class="active"</c:if>><a
                                href="<%=ctxPath%>/Order/?statu=待发货">待发货</a></li>
                        <li <c:if test="${statu eq '已发货'}">class="active"</c:if>><a
                                href="<%=ctxPath%>/Order/?statu=已发货">已发货</a></li>
                        <li <c:if test="${statu eq '已送达'}">class="active"</c:if>><a
                                href="<%=ctxPath%>/Order/?statu=已送达">已送达</a></li>
                    </ul>
                </li>
                <li class="treeview">
                    <a href="#"><i class="fa fa-link"></i> <span>商品管理</span>
                        <span class="pull-right-container">
                <i class="fa fa-angle-left pull-right"></i>
              </span>
                    </a>
                    <ul class="treeview-menu">
                        <li><a href="<%=ctxPath%>/Goods/AllGoods">所有商品</a></li>
                        <li><a href="<%=ctxPath%>/Goods/productAdd">添加商品</a></li>
                    </ul>
                </li>
            </ul>
            <!-- /.sidebar-menu -->
        </section>
        <!-- /.sidebar -->
    </aside>

    <!-- Content Wrapper. Contains page content -->
    <div class="content-wrapper">
        <!-- Content Header (Page header) -->
        <section class="content-header">
            <h1><small></small></h1>
            <ol class="breadcrumb">
                <li><i class="fa fa-dashboard">&nbsp;订单管理</i></li>
                <li class="active">所有订单</li>
            </ol>
        </section>
        <!-- Main content -->
        <section class="content container-fluid">
            <div class="box box-primary">
                <div class="box-body">
                    <table class="table table-bordered table-striped" id="my_order">
                        <thead>
                        <tr>
                            <th>订单编号</th>
                            <th>订单状态</th>
                            <th>创建时间</th>
                            <th>订单总额</th>
                            <th>可选操作</th>
                        </tr>
                        </thead>
                        <c:forEach items="${orderList}" var="order">
                        <tbody>
                        <tr>
                            <td>${order.oid}</td>
                            <td><span
                                    class="label <c:if test="${order.statu eq '待发货'}">bg-yellow</c:if> <c:if test="${order.statu eq '已发货'}">bg-aqua</c:if> <c:if test="${order.statu eq '已送达'}">bg-green</c:if>">${order.statu}</span>
                            </td>
                            <td id="rep1">${order.placed}</td>
                            <td>${order.payment}</td>
                            <td><a href="<%=ctxPath%>/Order/selectOrderDetail?oid=${order.oid}">查看</a>丨<a
                                    href="<%=ctxPath%>/Order/orderProcess?oid=${order.oid}">处理</a></td>
                        </tr>
                        </tbody>
                        </c:forEach>
                        <tfoot>
                        <tr>
                            <th>订单编号</th>
                            <th>订单状态</th>
                            <th>创建时间</th>
                            <th>订单总额</th>
                            <th>可选操作</th>
                        </tr>
                        </tfoot>
                    </table>
                </div>
                <!-- /.box-body -->
            </div>
        </section>
        <!-- /.content -->
    </div>
    <!-- /.content-wrapper -->

    <!-- Main Footer -->
    <footer class="main-footer">
        <!-- To the right -->
        <div class="pull-right hidden-xs">
            缔造年轻人的中国梦
        </div>
        <!-- Default to the left -->
        <strong>Copyright &copy; 2021 <a href="#">colouredglaze</a>.</strong> All rights reserved.
    </footer>

    <!-- /.control-sidebar -->
    <!-- Add the sidebar's background. This div must be placed
    immediately after the control sidebar -->
    <div class="control-sidebar-bg"></div>
</div>
</body>
<!-- ./wrapper -->

<!-- jQuery 3 -->
<script type="text/javascript" charset="UTF-8" src="<%=ctxPath%>/admin/assets/js/jquery/jquery.min.js"></script>
<!-- Bootstrap 3.3.7 -->
<script type="text/javascript" charset="UTF-8" src="<%=ctxPath%>/admin/assets/js/bootstrap/bootstrap.min.js"></script>
<!-- DataTables -->
<script type="text/javascript" charset="UTF-8" src="<%=ctxPath%>/admin/assets/js/datatables.net/jquery.dataTables.min.js"></script>
<script type="text/javascript" charset="UTF-8" src="<%=ctxPath%>/admin/assets/js/datatables.net-bs/dataTables.bootstrap.min.js"></script>
<!-- AdminLTE App -->
<script type="text/javascript" charset="UTF-8" src="<%=ctxPath%>/admin/assets/js/AdminLTE/adminlte.min.js"></script><!-- moment -->
<!-- <script src="../js/moment/moment.min.js"></script> -->
<!-- page script -->
<script type="text/javascript" charset="UTF-8">
    //批量改变时间格式
    window.onload = function () {
        var list = $('td');
        for (var i = 0; i < list.length; i++) {
            if (list[i].getAttribute("id") === "rep1") {
                list[i].innerHTML = list[i].innerHTML.replace("T", " ");
            }
        }
    };

    $(function () {
        $('#my_order').DataTable({
            paging: true,
            lengthChange: false,
            searching: false,
            ordering: false,
            info: true,
            autoWidth: false,
            pagingType: 'full_numbers'
        });
    });
</script>

</html>
