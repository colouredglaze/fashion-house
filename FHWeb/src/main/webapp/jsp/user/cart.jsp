<%--
  Created by IntelliJ IDEA.
  User: ZhangLin
  Date: 2020/11/25
  Time: 12:41
--%>
<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<%@include file="../header.jsp"%>
<!DOCTYPE html>
<html class="no-js" lang="en">
<head>
    <meta charset="utf-8">
    <meta content="IE=edge" http-equiv="X-UA-Compatible">
    <meta content="width=device-width, initial-scale=1" name="viewport">
    <meta content="meta description" name="description">

    <!-- Site title -->
    <title>购物车</title>
     <!-- Favicon -->
<link rel="icon" href="<%=ctxPath%>/static/favicon.ico" mce_href="<%=ctxPath%>/static/favicon.ico" type="image/x-icon" />

    <!-- Bootstrap CSS -->
    <link href="<%=ctxPath%>/user/assets/css/bootstrap.min.css" rel="stylesheet">
    <!-- Font-Awesome CSS -->
    <link href="<%=ctxPath%>/user/assets/css/font-awesome.min.css" rel="stylesheet">
    <!-- IonIcon CSS -->
    <link href="<%=ctxPath%>/user/assets/css/ionicons.min.css" rel="stylesheet">
    <!-- Plugins CSS -->
    <link href="<%=ctxPath%>/user/assets/css/plugins.css" rel="stylesheet">
    <!-- Main Style CSS -->
    <link href="<%=ctxPath%>/user/assets/css/style.css" rel="stylesheet">
</head>

<body>
<!-- header area start -->
<header>
    <!-- main menu area start -->
    <div class="header-main sticky">
        <div class="container custom-container">
            <div class="row align-items-center position-relative">
                <div class="col-lg-2 col-md-6 col-6 position-static">
                    <div class="logo">
                        <a href="<%=ctxPath%>/jsp/user/index.jsp">
                            <img alt="Brand logo" src="<%=ctxPath%>/user/assets/img/logo/gues_welcome.jpg">
                        </a>
                    </div>
                </div>
                <div class="col-lg-8 d-none d-lg-block position-static">
                    <div class="main-header-inner">
                        <div class="main-menu">
                            <nav id="mobile-menu">
                                <ul>
                                    <li><a href="<%=ctxPath%>/for">首页</a></li>
                                    <li><a href="<%=ctxPath%>/forA">商品</a></li>
                                    <li><a href="<%=ctxPath%>/Cart/userCartItem">购物车</a></li>
                                    <li><a href="<%=ctxPath%>/Address/allAddress">地址</a></li>
                                    <li><a href="<%=ctxPath%>/userInfo">个人信息</a></li>
                                    <li><a href="<%=ctxPath%>/Order/">订单</a></li>
                                    <li><a href="<%=ctxPath%>/logout.do">退出</a></li>
                                </ul>
                            </nav>
                        </div>
                    </div>
                </div>
                <div class="col-12 d-block d-lg-none">
                    <div class="mobile-menu"></div>
                </div>
            </div>
        </div>
    </div>
    <!-- main menu area end -->
</header>
<!-- header area end -->

<!-- cart main wrapper start -->
<div class="cart-main-wrapper section-padding" style="height: 800px;">
    <div class="container custom-container">
        <div class="row">
            <div class="col-lg-12">
                <!-- Cart Table Area -->
                <div class="cart-table table-responsive">
                    <table class="table table-bordered">
                        <thead>
                        <tr>
                            <th class="pro-thumbnail">缩略图</th>
                            <th class="pro-title">产品</th>
                            <th class="pro-price">大小</th>
                            <th class="pro-price">价格</th>
                            <th class="pro-quantity">数量</th>
                            <th class="pro-subtotal">删除</th>
                        </tr>
                        <!--计算总值-->

                        <c:set value="0" var="sum"/>
                        <!--计算总数设置id-->
                        <c:forEach items="${cartItemList}" var="cart" varStatus="gs">
                            <c:set value="${sum+cart.price*cart.pcount}" var="sum"/>
                        <tbody>
                        <tr>
                            <td class="pro-thumbnail">
                                <img alt="Product" class="img-fluid" src="/img/user/product/${cart.number}.jpg"/></td>
                            <td class="pro-title">${cart.name}</td>
                            <td class="pro-price">${cart.size}</td>
                            <td class="pro-price"><span >￥${cart.price}</span></td>
                            <td class="pro-quantity">
                                    <div style="color: red;">${cart.pcount}</div>
                            </td>
                            <td class="pro-remove">
                                <a id="${cart.rid}" onclick="deleteCartItem(this)" href="javascript:void(0);">
                                <i class="fa fa-trash-o"></i>
                                </a>
                            </td>
                        </tr>
                        </tbody>
                        </c:forEach>
                    </table>
                </div>
            </div>
        </div>
        <div class="row">
            <div class="col-lg-12 ml-auto">
                <!-- Cart Calculation Area -->
                <div class="cart-calculator-wrapper">
                    <div class="cart-calculate-items">
                        <div class="table-responsive">
                            <table class="table">
                                <tr class="total">
                                    <td><h3>购物车总计：
                                        <div id="AllPrice" style="color: red;display: inline-block">￥${sum}</div>
                                    </h3></td>
                                </tr>
                            </table>
                        </div>
                    </div>
                    <a class="sqr-btn d-block" onclick="haveCartItem()" href="javascript:void(0);">继续结账</a>
                </div>
            </div>
        </div>
    </div>
</div>
<!-- cart main wrapper end -->

<!-- footer area start -->
<footer>
    <!-- footer botton area start -->
    <div class="footer-bottom-area">
        <div class="container custom-container">
            <div class="row align-items-center">
                <div class="col-md-4 order-1">
                    <div class="footer-social-link">
                        <a href="#"><i class="fa fa-wechat"></i></a>
                        <a href="tencent://AddContact/?fromId=45&fromSubId=1&subcmd=all&uin=506031313"><i class="fa fa-qq"></i></a>
                        <a href="#"><i class="fa fa-weibo"></i></a>
                    </div>
                </div>
                <div class="col-md-4 order-3 order-md-2">
                    <div class="copyright-text text-center">
                        <p>版权<a href="#" title="self">@colouredglaze</a>. 保留所有权利</p>
                    </div>
                </div>
                <div class="col-md-4 ml-auto order-2 order-md-3">
                    <div class="footer-payment">
                        <img alt="" src="<%=ctxPath%>/user/assets/img/payment.png">
                    </div>
                </div>
            </div>
        </div>
    </div>
    <!-- footer botton area end -->
</footer>
<!-- footer area end -->

<!--All jQuery, Third Party Plugins & Activation (main.js) Files-->
<script src="<%=ctxPath%>/user/assets/js/vendor/modernizr-3.6.0.min.js"></script>
<!-- Jquery Min Js -->
<script src="<%=ctxPath%>/user/assets/js/vendor/jquery-3.3.1.min.js"></script>
<!-- Popper Min Js -->
<script src="<%=ctxPath%>/user/assets/js/vendor/popper.min.js"></script>
<!-- Bootstrap Min Js -->
<script src="<%=ctxPath%>/user/assets/js/vendor/bootstrap.min.js"></script>
<!-- Plugins Js-->
<script src="<%=ctxPath%>/user/assets/js/plugins.js"></script>
<!-- Ajax Mail Js -->
<script src="<%=ctxPath%>/user/assets/js/ajax-mail.js"></script>
<!-- Active Js -->
<script src="<%=ctxPath%>/user/assets/js/main.js"></script>

<script>
    //删除购物车中一条记录
    function deleteCartItem(event) {
        if (confirm("确定删除这条地址吗？")){
            location.href="<%=ctxPath%>/Cart/deleteCartItem?rid="+event.getAttribute("id");
        }else{
            alert("恭喜你做了正确的决定");
        }
    }

    //购物车是否有记录
    function haveCartItem() {
        if(${cartItemList.size()}>0){
            location.href="<%=ctxPath%>/Cart/forCart";
        }else{
            alert("请先添加商品");
            location.href="<%=ctxPath%>/forA";
        }
    }

    <%--//改变页面的总价--%>
    <%--$('.totalPrice').click(function () {--%>
    <%--    var totalPrice=0;--%>
    <%--    for (var i=1;i<${Temp};i++){--%>
    <%--        var a=$('#Price'+i).text().replace("￥","");--%>
    <%--        var b=$('#Number'+i).val();--%>
    <%--        totalPrice+=a*b;--%>
    <%--    }--%>
    <%--    $('#AllPrice').text(totalPrice);--%>
    <%--})--%>

</script>

</body>

</html>

