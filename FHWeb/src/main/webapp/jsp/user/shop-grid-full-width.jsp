<%--
  Created by IntelliJ IDEA.
  User: ZhangLin
  Date: 2020/11/25
  Time: 12:37
--%>
<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<%@include file="../header.jsp"%>
<!DOCTYPE html>
<html class="no-js" lang="zxx">

<head>
    <meta charset="utf-8">
    <meta content="IE=edge" http-equiv="X-UA-Compatible">
    <meta content="width=device-width, initial-scale=1" name="viewport">
    <meta content="meta description" name="description">

    <!-- Site title -->
    <title>商品</title>
    <!-- Favicon -->
    <link rel="icon" href="<%=ctxPath%>/static/favicon.ico" mce_href="<%=ctxPath%>/static/favicon.ico" type="image/x-icon" />

    <!-- Bootstrap CSS -->
    <link href="<%=ctxPath%>/user/assets/css/bootstrap.min.css" rel="stylesheet">
    <!-- Font-Awesome CSS -->
    <link href="<%=ctxPath%>/user/assets/css/font-awesome.min.css" rel="stylesheet">
    <!-- IonIcon CSS -->
    <link href="<%=ctxPath%>/user/assets/css/ionicons.min.css" rel="stylesheet">
    <!-- Plugins CSS -->
    <link href="<%=ctxPath%>/user/assets/css/plugins.css" rel="stylesheet">
    <!-- Main Style CSS -->
    <link href="<%=ctxPath%>/user/assets/css/style.css" rel="stylesheet">
</head>

<body>
<!-- header area start -->
<header>
    <!-- main menu area start -->
    <div class="header-main sticky">
        <div class="container custom-container">
            <div class="row align-items-center position-relative">
                <div class="col-lg-2 col-md-6 col-6 position-static">
                    <div class="logo">
                        <a href="<%=ctxPath%>/jsp/user/index.jsp">
                            <img alt="Brand logo" src="<%=ctxPath%>/user/assets/img/logo/gues_welcome.jpg">
                        </a>
                    </div>
                </div>
                <div class="col-lg-8 d-none d-lg-block position-static">
                    <div class="main-header-inner">
                        <div class="main-menu">
                            <nav id="mobile-menu">
                                <ul>
                                    <li><a href="<%=ctxPath%>/for">首页</a></li>
                                    <li><a href="<%=ctxPath%>/forA">商品</a></li>
                                    <li><a href="<%=ctxPath%>/Cart/userCartItem">购物车</a></li>
                                    <li><a href="<%=ctxPath%>/Address/allAddress">地址</a></li>
                                    <li><a href="<%=ctxPath%>/userInfo">个人信息</a></li>
                                    <li><a href="<%=ctxPath%>/Order/">订单</a></li>
                                    <li><a href="<%=ctxPath%>/logout.do">退出</a></li>
                                </ul>
                            </nav>
                        </div>
                    </div>
                </div>
                <div class="col-12 d-block d-lg-none">
                    <div class="mobile-menu"></div>
                </div>
            </div>
        </div>
    </div>
    <!-- main menu area end -->
</header>
<!-- header area end -->

<!-- page main wrapper start -->
<div class="shop-main-wrapper section-padding">
    <div class="container custom-container">
        <div class="row">
            <!-- product view wrapper area start -->
            <div class="col-12 order-1 order-lg-2">
                <div class="shop-product-wrapper">
                    <!-- shop product top wrap start -->
<%--                    <div class="shop-top-bar">--%>
<%--                        <div class="row">--%>
<%--                            <div class="col-lg-7 col-md-7">--%>
<%--                                <div class="top-bar-left">--%>
<%--                                    <div class="product-view-mode">--%>
<%--                                        <a class="active" data-target="grid" href="#"><i class="fa fa-th"></i></a>--%>
<%--                                    </div>--%>
<%--                                    <div class="product-amount">--%>
<%--                                        <p>显示${goodsList.size()}个结果</p>--%>
<%--                                    </div>--%>
<%--                                </div>--%>
<%--                            </div>--%>
<%--                        </div>--%>
<%--                    </div>--%>
                    <!-- shop product top wrap start -->
                    <!-- product view mode wrapper start -->
                    <div class="shop-product-wrap grid row">
                        <c:forEach items="${goodsList}" var="goods" varStatus="gs">
                        <div class="col-xl-3 col-lg-6 col-md-4 col-sm-6">
                            <!-- product grid item start -->
                            <div class="product-item mb-30">
                                <div class="product-thumb">
                                    <a href="<%=ctxPath%>/Goods/detail.do?number=${goods.number}">
                                        <img alt="${goods.name}" src="/img/user/product/${goods.number}.jpg">
                                    </a>
                                </div>
                                <div class="product-description text-center">
                                    <div class="product-name">
                                        <h3><a href="<%=ctxPath%>/Goods/detail.do?number=${goods.number}">${goods.name}</a></h3>
                                    </div>
                                    <div class="price-box">
                                        <span class="regular-price">￥${goods.price}</span>
                                    </div>
                                    <div class="product-btn">
                                        <a href="<%=ctxPath%>/Cart/goodsAdd?uid=${sessionScope.user.uid}&number=${goods.number}&pcount=1&size=S">添加到购物车</a>
                                    </div>
                                </div>
                            </div>
                            <!-- product grid item end -->
                        </div>
                        </c:forEach>
                    </div>
                    <!-- product view mode wrapper start -->
                </div>
            </div>
        </div>
    </div>
</div>
<!-- page main wrapper end -->

<!-- footer area start -->
<footer>
    <!-- footer botton area start -->
    <div class="footer-bottom-area">
        <div class="container custom-container">
            <div class="row align-items-center">
                <div class="col-md-4 order-1">
                    <div class="footer-social-link">
                        <a href="#"><i class="fa fa-wechat"></i></a>
                        <a href="tencent://AddContact/?fromId=45&fromSubId=1&subcmd=all&uin=506031313"><i class="fa fa-qq"></i></a>
                        <a href="#"><i class="fa fa-weibo"></i></a>
                    </div>
                </div>
                <div class="col-md-4 order-3 order-md-2">
                    <div class="copyright-text text-center">
                        <p>版权 <a href="#" title="self">@colouredglaze</a>保留所有权利</p>
                    </div>
                </div>
                <div class="col-md-4 ml-auto order-2 order-md-3">
                    <div class="footer-payment">
                        <img alt="" src="<%=ctxPath%>/user/assets/img/payment.png">
                    </div>
                </div>
            </div>
        </div>
    </div>
    <!-- footer botton area end -->
</footer>
<!-- footer area end -->

<!--All jQuery, Third Party Plugins & Activation (main.js) Files-->
<script src="<%=ctxPath%>/user/assets/js/vendor/modernizr-3.6.0.min.js"></script>
<!-- Jquery Min Js -->
<script src="<%=ctxPath%>/user/assets/js/vendor/jquery-3.3.1.min.js"></script>
<!-- Popper Min Js -->
<script src="<%=ctxPath%>/user/assets/js/vendor/popper.min.js"></script>
<!-- Bootstrap Min Js -->
<script src="<%=ctxPath%>/user/assets/js/vendor/bootstrap.min.js"></script>
<!-- Plugins Js-->
<script src="<%=ctxPath%>/user/assets/js/plugins.js"></script>
<!-- Ajax Mail Js -->
<script src="<%=ctxPath%>/user/assets/js/ajax-mail.js"></script>
<!-- Active Js -->
<script src="<%=ctxPath%>/user/assets/js/main.js"></script>
</body>

</html>
