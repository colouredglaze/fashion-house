<%--
  Created by IntelliJ IDEA.
  User: ZhangLin
  Date: 2020/11/27
  Time: 22:44
--%>
<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<%@include file="../header.jsp" %>
<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta content="width=device-width, initial-scale=1.0" name="viewport">
    <meta content="ie=edge" http-equiv="X-UA-Compatible">
    <title>用户信息</title>
    <link rel="icon" href="<%=ctxPath%>/static/favicon.ico" mce_href="<%=ctxPath%>/static/favicon.ico" type="image/x-icon" />

    <!-- Bootstrap CSS -->
    <link href="<%=ctxPath%>/user/assets/css/bootstrap.min.css" rel="stylesheet">
    <!-- Font-Awesome CSS -->
    <link href="<%=ctxPath%>/user/assets/css/font-awesome.min.css" rel="stylesheet">
    <!-- IonIcon CSS -->
    <link href="<%=ctxPath%>/user/assets/css/ionicons.min.css" rel="stylesheet">
    <!-- Plugins CSS -->
    <link href="<%=ctxPath%>/user/assets/css/plugins.css" rel="stylesheet">
    <!-- Main Style CSS -->
    <link href="<%=ctxPath%>/user/assets/css/style.css" rel="stylesheet">
    <style>
        .avatar {
            width: 70px;
            height: 70px;
            border: 1px #000 solid;
            text-align: center;
            border-radius: 50%;
            line-height: 70px;
            font-size: 30px;
        }
    </style>
</head>

<body>

<!-- header area start -->
<header>
    <!-- main menu area start -->
    <div class="header-main sticky">
        <div class="container custom-container">
            <div class="row align-items-center position-relative">
                <div class="col-lg-2 col-md-6 col-6 position-static">
                    <div class="logo">
                        <a href="<%=ctxPath%>/jsp/user/index.jsp">
                            <img alt="Brand logo" src="<%=ctxPath%>/user/assets/img/logo/gues_welcome.jpg">
                        </a>
                    </div>
                </div>
                <div class="col-lg-8 d-none d-lg-block position-static">
                    <div class="main-header-inner">
                        <div class="main-menu">
                            <nav id="mobile-menu">
                                <ul>
                                    <li><a href="<%=ctxPath%>/for">首页</a></li>
                                    <li><a href="<%=ctxPath%>/forA">商品</a></li>
                                    <li><a href="<%=ctxPath%>/Cart/userCartItem">购物车</a></li>
                                    <li><a href="<%=ctxPath%>/Address/allAddress">地址</a></li>
                                    <li><a href="<%=ctxPath%>/userInfo">个人信息</a></li>
                                    <li><a href="<%=ctxPath%>/Order/">订单</a></li>
                                    <li><a href="<%=ctxPath%>/logout.do">退出</a></li>
                                </ul>
                            </nav>
                        </div>
                    </div>
                </div>
                <div class="col-12 d-block d-lg-none">
                    <div class="mobile-menu"></div>
                </div>
            </div>
        </div>
    </div>
    <!-- main menu area end -->
</header>
<!-- header area end -->

<div style="margin-top: 20px" class="checkout-page-wrapper section-padding">
    <div class="container custom-container">
        <div class="row">
            <!-- Checkout Billing Details -->
            <div class="col-lg-12">
                <div class="checkout-billing-details-wrap">
<%--                    <a href="<%=ctxPath%>/userInfo">--%>
<%--                        <img class="avatar" align="middle" src="/img/user/icon/<%=user.getUid()%>.jpg">--%>
<%--                    </a>--%>
                    <h2 style="text-align: center">个人信息</h2>
                    <div class="billing-form-wrap">
                        <div class="single-input-item">
                            <label class="required" for="f_name">账号</label>
                            <input name="uid" id="f_name" placeholder="Name" required readonly type="text"
                                   value="${sessionScope.user.uid}"/>
                        </div>
                        <form action="<%=ctxPath%>/updateUser" method="post">
                            <div class="single-input-item">
                                <label class="required mt-20" for="street-address">用户名</label>
                                <input name="uname" id="street-address" placeholder="Nickname" required type="text"
                                       value="${reUser.uname}"/>
                            </div>
                            <button style="margin-top: 20px;" class="check-btn sqr-btn all-btn" type="submit">确定
                            </button>
                        </form>
                    </div>

                    <h2 style="text-align: center;margin-top: 30px;">修改密码</h2>

                    <div class="billing-form-wrap">
                        <form action="<%=ctxPath%>/updateUser" method="post" onsubmit="return isequal()">
                            <div class="single-input-item">
                                <div id="display"
                                     style="color: red;margin-top: 20px;text-align: center;font-size: 30px;">${msg}</div>
                            </div>
                            <div class="single-input-item">
                                <label class="required" for="f_name">原密码</label>
                                <input name="p_password" placeholder="Original Password" required type="password"/>
                            </div>
                            <div class="single-input-item">
                                <label class="required" for="f_name">新密码</label>
                                <input id="n_password" name="n_password" placeholder="New Password" required type="password"/>
                            </div>
                            <div class="single-input-item">
                                <label class="required" for="f_name">确认新密码</label>
                                <input id="r_password" name="r_password" placeholder="Confirm New Password" required type="password"/>
                            </div>
                            <button style="margin-top: 20px;" class="check-btn sqr-btn all-btn" type="submit">确定</button>
                        </form>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
<!-- checkout main wrapper end -->

<!-- footer area start -->
<footer>
    <!-- footer botton area start -->
    <div class="footer-bottom-area">
        <div class="container custom-container">
            <div class="row align-items-center">
                <div class="col-md-4 order-1">
                    <div class="footer-social-link">
                        <a href="#"><i class="fa fa-wechat"></i></a>
                        <a href="tencent://AddContact/?fromId=45&fromSubId=1&subcmd=all&uin=506031313"><i class="fa fa-qq"></i></a>
                        <a href="#"><i class="fa fa-weibo"></i></a>
                    </div>
                </div>
                <div class="col-md-4 order-3 order-md-2">
                    <div class="copyright-text text-center">
                        <p>版权<a href="#" title="self">@colouredglaze</a>保留所有权利</p>
                    </div>
                </div>
                <div class="col-md-4 ml-auto order-2 order-md-3">
                    <div class="footer-payment">
                        <img alt="" src="<%=ctxPath%>/user/assets/img/payment.png">
                    </div>
                </div>
            </div>
        </div>
    </div>
    <!-- footer botton area end -->
</footer>
<!-- footer area end -->


<!--All jQuery, Third Party Plugins & Activation (main.js) Files-->
<script src="<%=ctxPath%>/user/assets/js/vendor/modernizr-3.6.0.min.js"></script>
<!-- Jquery Min Js -->
<script src="<%=ctxPath%>/user/assets/js/vendor/jquery-3.3.1.min.js"></script>
<!-- Popper Min Js -->
<script src="<%=ctxPath%>/user/assets/js/vendor/popper.min.js"></script>
<!-- Bootstrap Min Js -->
<script src="<%=ctxPath%>/user/assets/js/vendor/bootstrap.min.js"></script>
<!-- Plugins Js-->
<script src="<%=ctxPath%>/user/assets/js/plugins.js"></script>
<!-- Ajax Mail Js -->
<script src="<%=ctxPath%>/user/assets/js/ajax-mail.js"></script>
<!-- Active Js -->
<script src="<%=ctxPath%>/user/assets/js/main.js"></script>

<%--检测密码是否相等--%>
<script>
    function isequal() {
        var a=$("#n_password").val();
        var b=$("#r_password").val();
        if(a===b){
            return true;
        }else{
            $("#display").html("两次输入密码不相同");
            return false;
        }
    }
</script>


</body>

</html>

