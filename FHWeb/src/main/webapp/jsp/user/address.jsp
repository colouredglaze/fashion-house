<%--
  Created by IntelliJ IDEA.
  User: ZhangLin
  Date: 2020/11/27
  Time: 20:53
--%>
<%@ page language="java" contentType="text/html; charset=UTF-8"
         pageEncoding="UTF-8"%>
<%@include file="../header.jsp"%>
<!DOCTYPE html>
<html class="no-js" lang="en">
<head>
    <meta charset="utf-8">
    <meta content="IE=edge" http-equiv="X-UA-Compatible">
    <meta content="width=device-width, initial-scale=1" name="viewport">
    <meta content="meta description" name="description">

    <!-- Site title -->
    <title>地址信息</title>
     <!-- Favicon -->
<link rel="icon" href="<%=ctxPath%>/static/favicon.ico" mce_href="<%=ctxPath%>/static/favicon.ico" type="image/x-icon" />

    <!-- Bootstrap CSS -->
    <link href="<%=ctxPath%>/user/assets/css/bootstrap.min.css" rel="stylesheet">
    <!-- Font-Awesome CSS -->
    <link href="<%=ctxPath%>/user/assets/css/font-awesome.min.css" rel="stylesheet">
    <!-- IonIcon CSS -->
    <link href="<%=ctxPath%>/user/assets/css/ionicons.min.css" rel="stylesheet">
    <!-- Plugins CSS -->
    <link href="<%=ctxPath%>/user/assets/css/plugins.css" rel="stylesheet">
    <!-- Main Style CSS -->
    <link href="<%=ctxPath%>/user/assets/css/style.css" rel="stylesheet">
</head>

<body>
<!-- header area start -->
<header>
    <!-- main menu area start -->
    <div class="header-main sticky">
        <div class="container custom-container">
            <div class="row align-items-center position-relative">
                <div class="col-lg-2 col-md-6 col-6 position-static">
                    <div class="logo">
                        <a href="<%=ctxPath%>/jsp/user/index.jsp">
                            <img alt="品牌标志" src="<%=ctxPath%>/user/assets/img/logo/gues_welcome.jpg">
                        </a>
                    </div>
                </div>
                <div class="col-lg-8 d-none d-lg-block position-static">
                    <div class="main-header-inner">
                        <div class="main-menu">
                            <nav id="mobile-menu">
                                <ul>
                                    <li><a href="<%=ctxPath%>/for">首页</a></li>
                                    <li><a href="<%=ctxPath%>/forA">商品</a></li>
                                    <li><a href="<%=ctxPath%>/Cart/userCartItem">购物车</a></li>
                                    <li><a href="<%=ctxPath%>/Address/allAddress">地址</a></li>
                                    <li><a href="<%=ctxPath%>/userInfo">个人信息</a></li>
                                    <li><a href="<%=ctxPath%>/Order/">订单</a></li>
                                    <li><a href="<%=ctxPath%>/logout.do">退出</a></li>
                                </ul>
                            </nav>
                        </div>
                    </div>
                </div>
                <div class="col-12 d-block d-lg-none">
                    <div class="mobile-menu"></div>
                </div>
            </div>
        </div>
    </div>
    <!-- main menu area end -->
</header>
<!-- header area end -->

<!-- cart main wrapper start -->
<div class="cart-main-wrapper section-padding" style="height: 800px;">
    <div class="container custom-container">
        <div class="row">
            <div class="col-lg-12">
                <!-- Cart Table Area -->
                <div class="cart-table table-responsive">
                    <table class="table table-bordered">
                        <thead>
                        <tr>
                            <th class="pro-price">编号</th>
                            <th class="pro-price">收货地址</th>
                            <th class="pro-price">收件人姓名</th>
                            <th class="pro-price">收件人电话</th>
                            <th class="pro-price">修改</th>
                            <th class="pro-price">删除</th>
                        </tr>
                        <c:set value="1" var="i"/>
                        <c:forEach items="${addressList}" var="address">

                        <tbody>
                        <tr>
                            <td class="pro-price">${i}</td>
                            <td class="pro-price" id="address${address.rid}">${address.address}</td>
                            <td class="pro-price" id="rname${address.rid}">${address.rname}</td>
                            <td class="pro-price" id="rphone${address.rid}">${address.rphone}</td>
                            <td class="pro-price"><a id="${address.rid}" class="check-btn sqr-btn " onclick="updateAddress(this)" href="javascript:void(0);">编辑地址</a></td>
                            <td class="pro-price"><a id="${address.rid}" style="color: black;" onclick="deleteAddress(this)" href="javascript:void(0);"><i class="fa fa-trash-o"></i></a></td>
                        </tr>
                        </tbody>

                        <c:set value="${i+1}" var="i"/>
                        </c:forEach>

                    </table>
                </div>
                <a style="text-align: center;margin-top: 40px;" class="check-btn sqr-btn all-btn" href="<%=ctxPath%>/jsp/user/address-add.jsp"><i class="fa fa-edit"></i>增加地址</a>
            </div>
        </div>
    </div>
</div>
<!-- cart main wrapper end -->

<!-- footer area start -->
<footer>
    <!-- footer botton area start -->
    <div class="footer-bottom-area">
        <div class="container custom-container">
            <div class="row align-items-center">
                <div class="col-md-4 order-1">
                    <div class="footer-social-link">
                        <a href="#"><i class="fa fa-wechat"></i></a>
                        <a href="tencent://AddContact/?fromId=45&fromSubId=1&subcmd=all&uin=506031313"><i class="fa fa-qq"></i></a>
                        <a href="#"><i class="fa fa-weibo"></i></a>
                    </div>
                </div>
                <div class="col-md-4 order-3 order-md-2">
                    <div class="copyright-text text-center">
                        <p>版权<a href="#" title="self">@colouredglaze</a>. 保留所有权利</p>
                    </div>
                </div>
                <div class="col-md-4 ml-auto order-2 order-md-3">
                    <div class="footer-payment">
                        <img alt="" src="<%=ctxPath%>/user/assets/img/payment.png">
                    </div>
                </div>
            </div>
        </div>
    </div>
    <!-- footer botton area end -->
</footer>
<!-- footer area end -->

<!--All jQuery, Third Party Plugins & Activation (main.js) Files-->
<script src="<%=ctxPath%>/user/assets/js/vendor/modernizr-3.6.0.min.js"></script>
<!-- Jquery Min Js -->
<script src="<%=ctxPath%>/user/assets/js/vendor/jquery-3.3.1.min.js"></script>
<!-- Popper Min Js -->
<script src="<%=ctxPath%>/user/assets/js/vendor/popper.min.js"></script>
<!-- Bootstrap Min Js -->
<script src="<%=ctxPath%>/user/assets/js/vendor/bootstrap.min.js"></script>
<!-- Plugins Js-->
<script src="<%=ctxPath%>/user/assets/js/plugins.js"></script>
<!-- Ajax Mail Js -->
<script src="<%=ctxPath%>/user/assets/js/ajax-mail.js"></script>
<!-- Active Js -->
<script src="<%=ctxPath%>/user/assets/js/main.js"></script>

<script>
    //删除一条收货地址
    function deleteAddress(event) {
        if (confirm("确定删除这条地址吗？")){
            location.href="<%=ctxPath%>/Address/deleteAddress?rid="+event.getAttribute("id");
        }else{
            alert("恭喜你做了正确的决定");
        }
    }

    //修改收获
    function updateAddress(event) {
        var rid=event.getAttribute("id");
        var address=$("#address"+rid).text();
        var rname=$("#rname"+rid).text();
        var rphone=$("#rphone"+rid).text();
        location.href="<%=ctxPath%>/jsp/user/address-add.jsp?rid="+rid+
            "&address="+encodeURI( address)+
            "&rname="+encodeURI(rname)+
            "&rphone="+rphone;
    }

</script>

</body>

</html>

