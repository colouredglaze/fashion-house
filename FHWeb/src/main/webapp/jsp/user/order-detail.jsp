<%--
  Created by IntelliJ IDEA.
  User: ZhangLin
  Date: 2020/11/28
  Time: 21:51
--%>
<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<%@include file="../header.jsp"%>
<!DOCTYPE html>
<html class="no-js" lang="en">
<head>
    <meta charset="utf-8">
    <meta content="IE=edge" http-equiv="X-UA-Compatible">
    <meta content="width=device-width, initial-scale=1" name="viewport">
    <meta content="meta description" name="description">

    <!-- Site title -->
    <title>订单详情</title>
    <!-- Favicon -->
    <link rel="icon" href="<%=ctxPath%>/static/favicon.ico" mce_href="<%=ctxPath%>/static/favicon.ico" type="image/x-icon" />

    <!-- Bootstrap CSS -->
    <link href="<%=ctxPath%>/user/assets/css/bootstrap.min.css" rel="stylesheet">
    <!-- Font-Awesome CSS -->
    <link href="<%=ctxPath%>/user/assets/css/font-awesome.min.css" rel="stylesheet">
    <!-- IonIcon CSS -->
    <link href="<%=ctxPath%>/user/assets/css/ionicons.min.css" rel="stylesheet">
    <!-- Plugins CSS -->
    <link href="<%=ctxPath%>/user/assets/css/plugins.css" rel="stylesheet">
    <!-- Main Style CSS -->
    <link href="<%=ctxPath%>/user/assets/css/style.css" rel="stylesheet">
</head>

<body>
<!-- header area start -->
<header>
    <!-- main menu area start -->
    <div class="header-main sticky">
        <div class="container custom-container">
            <div class="row align-items-center position-relative">
                <div class="col-lg-2 col-md-6 col-6 position-static"></div>
                <div class="col-lg-8 d-none d-lg-block position-static">
                    <div class="main-header-inner">
                        <div class="main-menu">
                            <nav id="mobile-menu">
                                <ul>
                                    <li><a href="<%=ctxPath%>/for">首页</a></li>
                                    <li><a href="<%=ctxPath%>/forA">商品</a></li>
                                    <li><a href="<%=ctxPath%>/Cart/userCartItem">购物车</a></li>
                                    <li><a href="<%=ctxPath%>/Address/allAddress">地址</a></li>
                                    <li><a href="<%=ctxPath%>/userInfo">个人信息</a></li>
                                    <li><a href="<%=ctxPath%>/Order/">订单</a></li>
                                    <li><a href="<%=ctxPath%>/logout.do">退出</a></li>
                                </ul>
                            </nav>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <!-- main menu area end -->
</header>
<!-- header area end -->

<!-- checkout main wrapper start -->
<div class="checkout-page-wrapper">
    <div class="container custom-container">
        <div class="row">
            <!-- Order Summary Details -->
            <div class="col-lg-12">
                <div class="order-summary-details">
                    <h2>订单详情</h2>
                    <div class="order-summary-content">
                        <!-- Order Summary Table -->
                        <div class="order-summary-table table-responsive text-center">
                            <table class="table table-bordered">
                                <thead><tr><th>订购人</th><th>${list.get(0).uname}</th></tr></thead>
                                <thead><tr><th>收货地址</th><th>${list.get(0).address}</th></tr></thead>
                                <thead><tr><th>收货人</th><th>${list.get(0).rname}</th></tr></thead>
                                <thead><tr><th>收货电话</th><th>${list.get(0).rphone}</th></tr></thead>
                                <thead><tr><th>订单状态</th><th id="${list.get(0).statu}">${list.get(0).statu}</th></tr></thead>
                                <thead><tr><th>下单时间</th><th id="rep1">${list.get(0).placed}</th></tr></thead>
                                <thead><tr><th>发货时间</th><th id="rep2">${list.get(0).deliver}</th></tr></thead>
                                <thead><tr><th>送达时间</th><th id="rep3">${list.get(0).handover}</th></tr></thead>
                                <tfoot><tr><td>支付金额</td><td><strong>￥${list.get(0).payment}</strong></td></tr></tfoot>
                            </table>
                        </div>

                        <div style="margin-bottom: 50px;font-weight: bold;" class="single-input-item">
                            <h2>订单包含的商品</h2>
                            <div class="row">
                                <div class="col-lg-12">
                                    <!-- Cart Table Area -->
                                    <div class="cart-table table-responsive">
                                        <table class="table table-bordered">
                                            <thead>
                                            <tr>
                                                <th class="pro-thumbnail">缩略图</th>
                                                <th class="pro-title">商品名称</th>
                                                <th class="pro-price">价格</th>
                                                <th class="pro-price">购买数量</th>
                                                <th class="pro-price">品牌</th>
                                                <th class="pro-price">尺寸</th>
                                                <th class="pro-price">材质</th>
                                            </tr>
                                            <tbody>
                                            <c:forEach items="${list.get(1)}" var="goods">
                                            <tr>
                                                <td class="pro-thumbnail"><img alt="Product" class="img-fluid" src="/img/user/product/${goods.number}.jpg" width="99" height="176"/></td>
                                                <td class="pro-price">${goods.name}</td>
                                                <td class="pro-price"><span style="color: red;">￥${goods.price}</span></td>
                                                <td class="pro-price"><div style="color: red;">${goods.pcount}</div></td>
                                                <td class="pro-price">${goods.brand}</td>
                                                <td class="pro-price">${goods.size}</td>
                                                <td class="pro-price">${goods.texture}</td>
                                            </tr>
                                            </c:forEach>
                                            </tbody>
                                        </table>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <div class="order-payment-method">
                            <div class="summary-footer-area">
                                <div class="custom-control custom-checkbox mb-20"></div>
                                <button class="all-btn check-btn sqr-btn" onclick="forOrder()">确认</button>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
<!-- checkout main wrapper end -->

<!-- footer area start -->
<footer>
    <!-- footer botton area start -->
    <div class="footer-bottom-area">
        <div class="container custom-container">
            <div class="row align-items-center">
                <div class="col-md-4 order-1">
                    <div class="footer-social-link">
                        <a href="#"><i class="fa fa-wechat"></i></a>
                        <a href="tencent://AddContact/?fromId=45&fromSubId=1&subcmd=all&uin=506031313"><i class="fa fa-qq"></i></a>
                        <a href="#"><i class="fa fa-weibo"></i></a>
                    </div>
                </div>
                <div class="col-md-4 order-3 order-md-2">
                    <div class="copyright-text text-center">
                        <p>版权 <a href="#" title="self">私有</a>保留所有权利
                    </div>
                </div>
                <div class="col-md-4 ml-auto order-2 order-md-3">
                    <div class="footer-payment">
                        <img alt="" src="<%=ctxPath%>/user/assets/img/payment.png">
                    </div>
                </div>
            </div>
        </div>
    </div>
    <!-- footer botton area end -->
</footer>
<!-- footer area end -->

<!--All jQuery, Third Party Plugins & Activation (main.js) Files-->
<script src="<%=ctxPath%>/user/assets/js/vendor/modernizr-3.6.0.min.js"></script>
<!-- Jquery Min Js -->
<script src="<%=ctxPath%>/user/assets/js/vendor/jquery-3.3.1.min.js"></script>
<!-- Popper Min Js -->
<script src="<%=ctxPath%>/user/assets/js/vendor/popper.min.js"></script>
<!-- Bootstrap Min Js -->
<script src="<%=ctxPath%>/user/assets/js/vendor/bootstrap.min.js"></script>
<!-- Plugins Js-->
<script src="<%=ctxPath%>/user/assets/js/plugins.js"></script>
<!-- Ajax Mail Js -->
<script src="<%=ctxPath%>/user/assets/js/ajax-mail.js"></script>
<!-- Active Js -->
<script src="<%=ctxPath%>/user/assets/js/main.js"></script>

<script>
    //改变页面颜色
    window.onload=function () {
        $("#待发货").css("color","red");
        $("#已发货").css("color","#ff9000");
        $("#已送达").css("color","#31ff58");
        $("#rep1").text($("#rep1").text().replace("T"," "));
        $("#rep2").text($("#rep2").text().replace("T", " "));
        $("#rep3").text($("#rep3").text().replace("T", " "));
    };

    function forOrder() {
        location.href="<%=ctxPath%>/Order/";
    }
</script>

</body>

</html>

