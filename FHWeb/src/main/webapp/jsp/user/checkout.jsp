<%@ page import="com.colouredglaze.entity.CartItemAndGoods" %>
<%@ page import="java.util.List" %>
<%--
  Created by IntelliJ IDEA.
  User: ZhangLin
  Date: 2020/11/27
  Time: 10:44
--%>
<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<%@include file="../header.jsp"%>
<!DOCTYPE html>
<html class="no-js" lang="en">

<head>
    <meta charset="utf-8">
    <meta content="IE=edge" http-equiv="X-UA-Compatible">
    <meta content="width=device-width, initial-scale=1" name="viewport">
    <meta content="meta description" name="description">
    <!-- Site title -->
    <title>结账</title>
    <link rel="icon" href="<%=ctxPath%>/static/favicon.ico" mce_href="<%=ctxPath%>/static/favicon.ico" type="image/x-icon" />
    <!-- Favicon -->
    <!-- Bootstrap CSS -->
    <link href="<%=ctxPath%>/user/assets/css/bootstrap.min.css" rel="stylesheet">
    <!-- Font-Awesome CSS -->
    <link href="<%=ctxPath%>/user/assets/css/font-awesome.min.css" rel="stylesheet">
    <!-- IonIcon CSS -->
    <link href="<%=ctxPath%>/user/assets/css/ionicons.min.css" rel="stylesheet">
    <!-- Plugins CSS -->
    <link href="<%=ctxPath%>/user/assets/css/plugins.css" rel="stylesheet">
    <!-- Main Style CSS -->
    <link href="<%=ctxPath%>/user/assets/css/style.css" rel="stylesheet">
</head>

<body>
<!-- header area start -->
<header>
    <!-- main menu area start -->
    <div class="header-main sticky">
        <div class="container custom-container">
            <div class="row align-items-center position-relative">
                <div class="col-lg-2 col-md-6 col-6 position-static"></div>
                <div class="col-lg-8 d-none d-lg-block position-static">
                    <div class="main-header-inner">
                        <div class="main-menu">
                            <nav id="mobile-menu">
                                <ul>
                                    <li><a href="<%=ctxPath%>/for">首页</a></li>
                                    <li><a href="<%=ctxPath%>/forA">商品</a></li>
                                    <li><a href="<%=ctxPath%>/Cart/userCartItem">购物车</a></li>
                                    <li><a href="<%=ctxPath%>/Address/allAddress">地址</a></li>
                                    <li><a href="<%=ctxPath%>/userInfo">个人信息</a></li>
                                    <li><a href="<%=ctxPath%>/Order/">订单</a></li>
                                    <li><a href="<%=ctxPath%>/logout.do">退出</a></li>
                                </ul>
                            </nav>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <!-- main menu area end -->
</header>
<!-- header area end -->

<!-- checkout main wrapper start -->
<div class="checkout-page-wrapper">
    <div class="container custom-container">
        <div class="row">
            <!-- Order Summary Details -->
            <div class="col-lg-12">
                <div class="order-summary-details">
                    <h2>你的订单概要</h2>
                    <div class="order-summary-content">
                        <!-- Order Summary Table -->
                        <div class="order-summary-table table-responsive text-center">
                            <table class="table table-bordered" style="table-layout:fixed;">
                                <thead>
                                <tr>
                                    <th>产品展示</th>
                                    <th>大小</th>
                                    <th>总计</th>
                                </tr>
                                </thead>

                                <c:set value="0" var="sum"/>
                                <%
                                    //设置每次订单的数据
                                    @SuppressWarnings("unchecked")
                                    List<Object> objectList=(List<Object>) request.getAttribute("allList");
                                    @SuppressWarnings("unchecked")
                                    List<CartItemAndGoods> cartItemAndGoodsArrayList = (List<CartItemAndGoods>) objectList.get(0);
                                    request.getSession().setAttribute("cartItemAndGoodsArrayList",cartItemAndGoodsArrayList);
                                %>
                                <!--计算总数设置id-->
                                <c:forEach items="${allList.get(0)}" var="cart" varStatus="gs">
                                <c:set value="${sum+cart.price*cart.pcount}" var="sum"/>
                                <tbody>
                                <tr>
                                    <td><a href="<%=ctxPath%>/Goods/detail.do?number=${cart.number}">${cart.name}<strong> ×
                                        ${cart.pcount}</strong></a></td>
                                    <td>${cart.size}</td>
                                    <td>￥${cart.pcount*cart.price}</td>
                                </tr>
                                </tbody>
                                </c:forEach>
                                <tfoot>
                                <tr>
                                    <td>总金额</td>
                                    <td></td>
                                    <td><strong>￥${sum}</strong></td>
                                </tr>
                                </tfoot>
                            </table>
                        </div>

                        <div style="margin-bottom: 50px;font-weight: bold;" class="single-input-item">
                            <label class="required" for="address">选择收货地址</label>
                            <select id="address" name="country nice-select">
                                <c:forEach items="${allList.get(1)}" var="address">
                                <option value="${address.rid}">
                                        收货地址：${address.address}&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
                                        收件人姓名：${address.rname}&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
                                        收件人电话：${address.rphone}</option>
                                </c:forEach>
                            </select>
                        </div>

                        <div class="order-payment-method">
                            <div class="single-payment-method show">
                                <div class="payment-method-name">
                                    <div class="custom-control custom-radio">
                                        <input checked class="custom-control-input" id="cashon" name="paymentmethod"
                                               type="radio" value="cash"/>
                                        <label class="custom-control-label" for="cashon">货到付款</label>
                                    </div>
                                </div>
                            </div>
                            <div class="summary-footer-area">
                                <div class="custom-control custom-checkbox mb-20">
                                    <input class="custom-control-input startOrders" id="terms" required type="checkbox"/>
                                    <label class="custom-control-label" for="terms">我已阅读并同意网站的条款和条件。</label>
                                </div>
                                <button class="all-btn check-btn sqr-btn" onclick="startOrder()">下订单</button>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
<!-- checkout main wrapper end -->

<!-- footer area start -->
<footer>
    <!-- footer botton area start -->
    <div class="footer-bottom-area">
        <div class="container custom-container">
            <div class="row align-items-center">
                <div class="col-md-4 order-1">
                    <div class="footer-social-link">
                        <a href="#"><i class="fa fa-wechat"></i></a>
                        <a href="tencent://AddContact/?fromId=45&fromSubId=1&subcmd=all&uin=506031313"><i class="fa fa-qq"></i></a>
                        <a href="#"><i class="fa fa-weibo"></i></a>
                    </div>
                </div>
                <div class="col-md-4 order-3 order-md-2">
                    <div class="copyright-text text-center">
                        <p>版权 <a href="#" title="self">私有</a>保留所有权利
                    </div>
                </div>
                <div class="col-md-4 ml-auto order-2 order-md-3">
                    <div class="footer-payment">
                        <img alt="" src="<%=ctxPath%>/user/assets/img/payment.png">
                    </div>
                </div>
            </div>
        </div>
    </div>
    <!-- footer botton area end -->
</footer>
<!-- footer area end -->

<!--All jQuery, Third Party Plugins & Activation (main.js) Files-->
<script src="<%=ctxPath%>/user/assets/js/vendor/modernizr-3.6.0.min.js"></script>
<!-- Jquery Min Js -->
<script src="<%=ctxPath%>/user/assets/js/vendor/jquery-3.3.1.min.js"></script>
<!-- Popper Min Js -->
<script src="<%=ctxPath%>/user/assets/js/vendor/popper.min.js"></script>
<!-- Bootstrap Min Js -->
<script src="<%=ctxPath%>/user/assets/js/vendor/bootstrap.min.js"></script>
<!-- Plugins Js-->
<script src="<%=ctxPath%>/user/assets/js/plugins.js"></script>
<!-- Ajax Mail Js -->
<script src="<%=ctxPath%>/user/assets/js/ajax-mail.js"></script>
<!-- Active Js -->
<script src="<%=ctxPath%>/user/assets/js/main.js"></script>

<script>
    function startOrder() {
        var a=1;
        if ($(".startOrders").is(":checked")){
            if (${allList.get(1).size()}>0){
                a = $("#address").val();
                location.href="<%=ctxPath%>/Order/createOrderAndOrderItem?address_id="+a;
            }else{
                alert("请添加地址");
                location.href="<%=ctxPath%>/jsp/user/address-add.jsp";
            }
        }else{
            alert("请同意网站协议");
        }
    }
</script>

</body>

</html>
