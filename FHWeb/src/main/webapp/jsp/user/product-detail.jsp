<%--
  Created by IntelliJ IDEA.
  User: ZhangLin
  Date: 2020/11/25
  Time: 14:04
--%>
<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<%@include file="../header.jsp"%>
<!DOCTYPE html>
<html class="no-js" lang="en">

<head>
    <meta charset="utf-8">
    <meta content="IE=edge" http-equiv="X-UA-Compatible">
    <meta content="width=device-width, initial-scale=1" name="viewport">
    <meta content="meta description" name="description">

    <!-- Site title -->
    <title>商品详情</title>
    <!-- Favicon -->
    <link rel="icon" href="<%=ctxPath%>/static/favicon.ico" mce_href="<%=ctxPath%>/static/favicon.ico" type="image/x-icon" />

    <!-- Bootstrap CSS -->
    <link href="<%=ctxPath%>/user/assets/css/bootstrap.min.css" rel="stylesheet">
    <!-- Font-Awesome CSS -->
    <link href="<%=ctxPath%>/user/assets/css/font-awesome.min.css" rel="stylesheet">
    <!-- IonIcon CSS -->
    <link href="<%=ctxPath%>/user/assets/css/ionicons.min.css" rel="stylesheet">
    <!-- Plugins CSS -->
    <link href="<%=ctxPath%>/user/assets/css/plugins.css" rel="stylesheet">
    <!-- Main Style CSS -->
    <link href="<%=ctxPath%>/user/assets/css/style.css" rel="stylesheet">
</head>

<body>
<!-- header area start -->
<header>
    <!-- main menu area start -->
    <div class="header-main sticky">
        <div class="container custom-container">
            <div class="row align-items-center position-relative">
                <div class="col-lg-2 col-md-6 col-6 position-static">
                    <div class="logo">
                        <a href="<%=ctxPath%>/jsp/user/index.jsp">
                            <img alt="Brand logo" src="<%=ctxPath%>/user/assets/img/logo/gues_welcome.jpg">
                        </a>
                    </div>
                </div>
                <div class="col-lg-8 d-none d-lg-block position-static">
                    <div class="main-header-inner">
                        <div class="main-menu">
                            <nav id="mobile-menu">
                                <ul>
                                    <li><a href="<%=ctxPath%>/for">首页</a></li>
                                    <li><a href="<%=ctxPath%>/forA">商品</a></li>
                                    <li><a href="<%=ctxPath%>/Cart/userCartItem">购物车</a></li>
                                    <li><a href="<%=ctxPath%>/Address/allAddress">地址</a></li>
                                    <li><a href="<%=ctxPath%>/userInfo">个人信息</a></li>
                                    <li><a href="<%=ctxPath%>/Order/">订单</a></li>
                                    <li><a href="<%=ctxPath%>/logout.do">退出</a></li>
                                </ul>
                            </nav>
                        </div>
                    </div>
                </div>
                <div class="col-12 d-block d-lg-none">
                    <div class="mobile-menu"></div>
                </div>
            </div>
        </div>
    </div>
    <!-- main menu area end -->
</header>
<!-- header area end -->

<!-- page main wrapper start -->
<div class="product-details-wrapper section-padding">
    <div class="container custom-container">
        <div class="row">
            <div class="col-lg-12">
                <!-- product details inner end -->
                <div class="product-details-inner">
                    <div class="row">
                        <div class="col-lg-4">
                            <div class="product-large-slider mb-20">
                                <!--"/img" 是映射的本地虚拟路径-->
                                <img alt="" src="/img/user/product/${goods.number}.jpg"/>
                            </div>
                        </div>
                        <div class="col-lg-8">
                            <div class="product-details-des">
                                <h3>${goods.name}</h3>
                                <div class="price-box">
                                    <span class="regular-price">￥${goods.price}</span>
                                </div>
                                <p style="margin-top: 20px;margin-bottom: 20px">&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
                                    ${goods.describe}
                                </p>

                                <div class="availability mb-20">
                                    <h5>材质:</h5><span style="color:#000000;">${goods.texture}</span>
                                </div>

                                <div class="pro-size mb-20">
                                    <h5>尺寸 :</h5>
                                        <select class="nice-select" id="size">
                                            <c:forEach items="${goods.size}" var="goodsize" begin="0" end="${goods.size.size()}" varStatus="gs">
                                            <option value="${goodsize}">${goodsize}</option>
                                            </c:forEach>
                                        </select>
                                </div>

                                <div class="quantity-cart-box d-flex align-items-center mb-20">
                                    <div class="quantity">
                                        <div class="pro-qty">
                                            <input id="pcount" type="text" value="1" readonly>
                                        </div>
                                    </div>
                                    <div class="sqr-btn">
                                        <a onclick="addCartItem()" href="#">加入购物车</a>
                                    </div>
                                </div>

                                <div class="availability mb-20">
                                    <h5>可用性:</h5>

                                    <c:if test="${goods.count>0}">
                                        <span>有现货</span>
                                    </c:if>

                                    <c:if test="${goods.count==0}">
                                        <span style="color: red">无货</span>
                                    </c:if>
                                </div>

                                <div class="availability mb-20">
                                    <h5>供应商:</h5><span style="color:#000000;">${goods.brand}</span>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
                <!-- product details inner end -->

            </div>
        </div>
    </div>
</div>
<!-- page main wrapper end -->

<!-- footer area start -->
<footer>
    <!-- footer botton area start -->
    <div class="footer-bottom-area">
        <div class="container custom-container">
            <div class="row align-items-center">
                <div class="col-md-4 order-1">
                    <div class="footer-social-link">
                        <a href="#"><i class="fa fa-wechat"></i></a>
                        <a href="tencent://AddContact/?fromId=45&fromSubId=1&subcmd=all&uin=506031313"><i class="fa fa-qq"></i></a>
                        <a href="#"><i class="fa fa-weibo"></i></a>
                    </div>
                </div>
                <div class="col-md-4 order-3 order-md-2">
                    <div class="copyright-text text-center">
                        <p>版权<a href="#" title="self">@colouredglaze</a>保留所有权利</p>
                    </div>
                </div>
                <div class="col-md-4 ml-auto order-2 order-md-3">
                    <div class="footer-payment">
                        <img alt="" src="<%=ctxPath%>/user/assets/img/payment.png">
                    </div>
                </div>
            </div>
        </div>
    </div>
    <!-- footer botton area end -->
</footer>
<!-- footer area end -->

<!--All jQuery, Third Party Plugins & Activation (main.js) Files-->
<script src="<%=ctxPath%>/user/assets/js/vendor/modernizr-3.6.0.min.js"></script>
<!-- Jquery Min Js -->
<script src="<%=ctxPath%>/user/assets/js/vendor/jquery-3.3.1.min.js"></script>
<!-- Popper Min Js -->
<script src="<%=ctxPath%>/user/assets/js/vendor/popper.min.js"></script>
<!-- Bootstrap Min Js -->
<script src="<%=ctxPath%>/user/assets/js/vendor/bootstrap.min.js"></script>
<!-- Plugins Js-->
<script src="<%=ctxPath%>/user/assets/js/plugins.js"></script>
<!-- Ajax Mail Js -->
<script src="<%=ctxPath%>/user/assets/js/ajax-mail.js"></script>
<!-- Active Js -->
<script src="<%=ctxPath%>/user/assets/js/main.js"></script>

<!--一次添加多件商品到购物车-->
<script>
    function addCartItem() {
        let size=document.getElementById("size")
        let selectedIndex = size.selectedIndex;
        location.href="<%=ctxPath%>/Cart/goodsAdd?uid=${sessionScope.user.uid}&number=${goods.number}&pcount="+$("#pcount").val()+"&size="+size.options[selectedIndex].value;

    }
</script>

</body>

</html>
