<%--
  Created by IntelliJ IDEA.
  User: ZhangLin
  Date: 2020/11/27
  Time: 19:18
--%>
<%@ page language="java" contentType="text/html; charset=UTF-8"
         pageEncoding="UTF-8"%>
<%@include file="../header.jsp"%>
<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta content="width=device-width, initial-scale=1.0" name="viewport">
    <meta content="ie=edge" http-equiv="X-UA-Compatible">
    <title>地址添加</title>
    <link rel="icon" href="<%=ctxPath%>/static/favicon.ico" mce_href="<%=ctxPath%>/static/favicon.ico" type="image/x-icon" />

    <!-- Bootstrap CSS -->
    <link href="<%=ctxPath%>/user/assets/css/bootstrap.min.css" rel="stylesheet">
    <!-- Font-Awesome CSS -->
    <link href="<%=ctxPath%>/user/assets/css/font-awesome.min.css" rel="stylesheet">
    <!-- IonIcon CSS -->
    <link href="<%=ctxPath%>/user/assets/css/ionicons.min.css" rel="stylesheet">
    <!-- Plugins CSS -->
    <link href="<%=ctxPath%>/user/assets/css/plugins.css" rel="stylesheet">
    <!-- Main Style CSS -->
    <link href="<%=ctxPath%>/user/assets/css/style.css" rel="stylesheet">
</head>

<body>

<!-- header area start -->
<header>
    <!-- main menu area start -->
    <div class="header-main sticky">
        <div class="container custom-container">
            <div class="row align-items-center position-relative">
                <div class="col-lg-2 col-md-6 col-6 position-static">
                    <div class="logo">
                        <a href="<%=ctxPath%>/jsp/user/index.jsp">
                            <img alt="Brand logo" src="<%=ctxPath%>/user/assets/img/logo/gues_welcome.jpg">
                        </a>
                    </div>
                </div>
                <div class="col-lg-8 d-none d-lg-block position-static">
                    <div class="main-header-inner">
                        <div class="main-menu">
                            <nav id="mobile-menu">
                                <ul>
                                    <li><a href="<%=ctxPath%>/for">首页</a></li>
                                    <li><a href="<%=ctxPath%>/forA">商品</a></li>
                                    <li><a href="<%=ctxPath%>/Cart/userCartItem">购物车</a></li>
                                    <li><a href="<%=ctxPath%>/Address/allAddress">地址</a></li>
                                    <li><a href="<%=ctxPath%>/userInfo">个人信息</a></li>
                                    <li><a href="<%=ctxPath%>/Order/">订单</a></li>
                                    <li><a href="<%=ctxPath%>/logout.do">退出</a></li>
                                </ul>
                            </nav>
                        </div>
                    </div>
                </div>
                <div class="col-12 d-block d-lg-none">
                    <div class="mobile-menu"></div>
                </div>
            </div>
        </div>
    </div>
    <!-- main menu area end -->
</header>
<!-- header area end -->

<div style="margin-top: 50px" class="checkout-page-wrapper section-padding">
    <div class="container custom-container">
        <div class="row">
            <!-- Checkout Billing Details -->
            <div class="col-lg-12">
                <div class="checkout-billing-details-wrap">
                    <h2 style="text-align: center">添加地址</h2>
                    <div class="billing-form-wrap">
                        <form action="<%=ctxPath%>/Address/addupdateAddress" method="post">
                                <div class="single-input-item">
                                    <label class="required" for="f_name">收件人姓名</label>
                                    <input name="rname" id="f_name" placeholder="Name" required type="text" value="${param.rname}"/>
                                </div>

                            <div class="single-input-item">
                                <label class="required mt-20" for="street-address">收货人地址</label>
                                <input name="address" id="street-address" placeholder="Address" required type="text" value="${param.address}"/>
                            </div>

                            <div class="single-input-item">
                                <label class="required mt-20" for="phone">收货人电话</label>
                                <input name="rphone" id="phone" placeholder="Phone" required type="text" value="${param.rphone}"/>
                            </div>
                            <input style="display: none;" name="rid" value="${param.rid}"/>
                            <input style="display: none;" name="uid" value="${sessionScope.user.uid}"/>
                            <button style="margin-top: 20px;" class="check-btn sqr-btn all-btn" type="submit">确定</button>
                        </form>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
<!-- checkout main wrapper end -->

<!-- footer area start -->
<footer>
    <!-- footer botton area start -->
    <div class="footer-bottom-area">
        <div class="container custom-container">
            <div class="row align-items-center">
                <div class="col-md-4 order-1">
                    <div class="footer-social-link">
                        <a href="#"><i class="fa fa-wechat"></i></a>
                        <a href="tencent://AddContact/?fromId=45&fromSubId=1&subcmd=all&uin=506031313"><i class="fa fa-qq"></i></a>
                        <a href="#"><i class="fa fa-weibo"></i></a>
                    </div>
                </div>
                <div class="col-md-4 order-3 order-md-2">
                    <div class="copyright-text text-center">
                        <p>版权<a href="#" title="self">@colouredglaze</a>保留所有权利</p>
                    </div>
                </div>
                <div class="col-md-4 ml-auto order-2 order-md-3">
                    <div class="footer-payment">
                        <img alt="" src="<%=ctxPath%>/user/assets/img/payment.png">
                    </div>
                </div>
            </div>
        </div>
    </div>
    <!-- footer botton area end -->
</footer>
<!-- footer area end -->
<script>

    function GetQueryString(name) {
        var reg =new RegExp("(^|&)" + name + "=([^&]*)(&|$)");

        var r = window.location.search.substr(1).match(reg);

        if (r !=null)

            return decodeURI(r[2]);

        return null;

    }

</script>
<!--All jQuery, Third Party Plugins & Activation (main.js) Files-->
<script src="<%=ctxPath%>/user/assets/js/vendor/modernizr-3.6.0.min.js"></script>
<!-- Jquery Min Js -->
<script src="<%=ctxPath%>/user/assets/js/vendor/jquery-3.3.1.min.js"></script>
<!-- Popper Min Js -->
<script src="<%=ctxPath%>/user/assets/js/vendor/popper.min.js"></script>
<!-- Bootstrap Min Js -->
<script src="<%=ctxPath%>/user/assets/js/vendor/bootstrap.min.js"></script>
<!-- Plugins Js-->
<script src="<%=ctxPath%>/user/assets/js/plugins.js"></script>
<!-- Ajax Mail Js -->
<script src="<%=ctxPath%>/user/assets/js/ajax-mail.js"></script>
<!-- Active Js -->
<script src="<%=ctxPath%>/user/assets/js/main.js"></script>

</body>

</html>
