<%--
  Created by IntelliJ IDEA.
  User: ZhangLin
  Date: 2020/11/24
  Time: 19:33
--%>
<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<%@include file="header.jsp" %>
<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta content="width=device-width, initial-scale=1.0" name="viewport">
    <meta content="ie=edge" http-equiv="X-UA-Compatible">
    <title>注册</title>

    <link rel="icon" href="<%=ctxPath%>/static/favicon.ico" mce_href="<%=ctxPath%>/static/favicon.ico" type="image/x-icon" />

    <!-- Bootstrap CSS -->
    <link href="<%=ctxPath%>/user/assets/css/bootstrap.min.css" rel="stylesheet">
    <!-- Font-Awesome CSS -->
    <link href="<%=ctxPath%>/user/assets/css/font-awesome.min.css" rel="stylesheet">
    <!-- IonIcon CSS -->
    <link href="<%=ctxPath%>/user/assets/css/ionicons.min.css" rel="stylesheet">
    <!-- Plugins CSS -->
    <link href="<%=ctxPath%>/user/assets/css/plugins.css" rel="stylesheet">
    <!-- Main Style CSS -->
    <link href="<%=ctxPath%>/user/assets/css/style.css" rel="stylesheet">
</head>

<body>
<div style="height: 100px;"></div>
<!-- Register Content Start -->
<div class="container center-block text-center">
    <div class="login-reg-form-wrap signup-form">
        <h1 style="margin-bottom: 30px;font-weight: bold">时装邮购系统</h1>
        <h2>注册</h2>
        <form action="<%=ctxPath%>/register.do" method="post" onsubmit="return forward()">
            <div class="single-input-item">
                <input name="uid" placeholder="请输入手机号码" required type="text"
                       pattern="^1(3|4|5|6|9|7|8)\d{9}$"/>
            </div>
            <div class="single-input-item">
                <input name="uname" placeholder="请输入用户名" required type="text"/>
            </div>
            <div class="single-input-item">
                <input id="password" name="password" placeholder="输入密码" required type="password"/>
            </div>
            <div class="single-input-item">
                <input id="repassword" name="repassword" placeholder="重复密码" required type="password"/>
            </div>
            <div class="single-input-item">
                <input id="verifyCode" name="verifyCode" placeholder="请输入短信验证码"/>
            </div>
            <div id="display" style="color: red;margin-top: 20px;">${msg}</div>
            <div class="single-input-item" style="width: 100%">
                <button type="button" class="sendVerifyCode sqr-btn" style="width: 100%">获取短信验证码</button>
            </div>
            <div class="single-input-item" style="width: 100%">
                <button class="sqr-btn" style="width: 100%">注册</button>
            </div>
        </form>
        <div class="single-input-item" style="width: 100%">
            <a href="<%=ctxPath%>/jsp/login.jsp">
                <button class="sqr-btn" style="width: 100%">登录</button>
            </a>
        </div>
    </div>
</div>
<!-- Register Content End -->

<!-- footer area start -->
<footer>
    <!-- footer botton area start -->
    <div class="footer-bottom-area">
        <div class="container custom-container">
            <div class="row align-items-center">
                <div class="col-md-4 order-1">
                    <div class="footer-social-link">
                        <a href="#"><i class="fa fa-wechat"></i></a>
                        <a href="tencent://AddContact/?fromId=45&fromSubId=1&subcmd=all&uin=506031313"><i class="fa fa-qq"></i></a>
                        <a href="#"><i class="fa fa-weibo"></i></a>
                    </div>
                </div>
                <div class="col-md-4 order-3 order-md-2">
                    <div class="copyright-text text-center">
                        <p>版权<a href="#" title="self">@colouredglaze</a>保留所有权利</p>
                    </div>
                </div>
                <div class="col-md-4 ml-auto order-2 order-md-3">
                    <div class="footer-payment">
                        <img alt="" src="<%=ctxPath%>/user/assets/img/payment.png">
                    </div>
                </div>
            </div>
        </div>
    </div>
    <!-- footer botton area end -->
</footer>
<!-- footer area end -->

<!--All jQuery, Third Party Plugins & Activation (main.js) Files-->
<script src="<%=ctxPath%>/user/assets/js/vendor/modernizr-3.6.0.min.js"></script>
<!-- Jquery Min Js -->
<script src="<%=ctxPath%>/user/assets/js/vendor/jquery-3.3.1.min.js"></script>
<!-- Popper Min Js -->
<script src="<%=ctxPath%>/user/assets/js/vendor/popper.min.js"></script>
<!-- Bootstrap Min Js -->
<script src="<%=ctxPath%>/user/assets/js/vendor/bootstrap.min.js"></script>
<!-- Plugins Js-->
<script src="<%=ctxPath%>/user/assets/js/plugins.js"></script>
<!-- Ajax Mail Js -->
<script src="<%=ctxPath%>/user/assets/js/ajax-mail.js"></script>
<!-- Active Js -->
<script src="<%=ctxPath%>/user/assets/js/main.js"></script>

<script>
    //短信验证码
    $(function () {
        //两分钟点击限制
        var waitTime = 120;
        function time(ele) {
            if (waitTime === 0) {
                ele.disabled=false;
                ele.innerHTML = "获取短信验证码";
                waitTime = 120;// 恢复计时
            } else {
                ele.disabled=true;
                ele.innerHTML = waitTime + "秒后可以重新发送";
                waitTime--;
                setTimeout(function() {
                    time(ele)// 关键处-定时循环调用
                }, 1000)
            }
        }

        //发送验证码
        $(".sendVerifyCode").on("click", function () {
            var uid = $("input[name=uid]").val();
            if (uid===""){
                alert("请输入手机号码");
                return;
            }
            $.ajax({
                url: "<%=ctxPath%>/sendSms",
                async: true,
                data: {"uid": uid},
                type: "POST",
                dataType: "TEXT",
                success: function (message) {
                    if (message !== "success") {
                        alert("发送验证码失败");
                    }
                }
            });
            time(this);
        });
    });

    //密码判重
    function forward() {
        var a = $("#password").val();
        var b = $("#repassword").val();
        if ($("#verifyCode").val()===""){
            $("#display").html("请输入验证码");
            return false;
        }
        if (a === b) {
            return true;
        } else {
            $("#display").html("两次输入密码不相同");
            return false;
        }
    }
</script>
</body>

</html>
