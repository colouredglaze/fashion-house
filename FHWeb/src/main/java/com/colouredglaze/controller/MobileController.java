package com.colouredglaze.controller;

import com.colouredglaze.entity.TbGoods;
import com.colouredglaze.entity.TbUser;
import com.colouredglaze.service.GoodsService;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import lombok.RequiredArgsConstructor;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import java.util.List;

/**
 * @author ZhangLin
 * @version 1.0
 * @date 2020/11/29 19:41
 */
@RestController
@RequiredArgsConstructor
@RequestMapping("/mobile")
@Api(description = "移动端测试")
public class MobileController {

    private final  GoodsService goodsService;

    @ApiOperation("所有商品")
    @GetMapping("AllGoods")
    public List<TbGoods> allGoods() {
        //        return new ModelAndView("/admin/product-list", "list", list);
        return goodsService.selectList();
    }

    @ApiOperation("测试")
    @GetMapping("/hello")
    public TbUser hello(String id){
        TbUser tbUser = new TbUser();
        tbUser.setUname("测试");
        tbUser.setUid("38381717110");
        tbUser.setPassword("123456789");
        tbUser.setRole(1);
        return tbUser;
    }


}
