package com.colouredglaze.controller;

import com.colouredglaze.entity.TbGoods;
import com.colouredglaze.entity.TbSupplier;
import com.colouredglaze.entity.TbUser;
import com.colouredglaze.service.GoodsService;
import com.colouredglaze.service.SupplierService;
import com.colouredglaze.utils.Image;
import io.swagger.annotations.Api;
import lombok.RequiredArgsConstructor;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.multipart.MultipartFile;
import org.springframework.web.servlet.ModelAndView;

import javax.servlet.http.HttpServletRequest;
import java.util.Arrays;
import java.util.List;

/**
 * @author ZhangLin
 * @version 1.0
 * @date 2020/11/25 12:49
 */
@Controller
@RequiredArgsConstructor
@RequestMapping("/Goods")
@Api(description = "商品管理")
public class GoodsController {

    private final  GoodsService goodsService;

    private final  SupplierService supplierService;

    //查看所有商品
    @RequestMapping("AllGoods")
    public ModelAndView allGoods() {
        List<TbGoods> list = goodsService.selectList();
        return new ModelAndView("/admin/product-list", "list", list);
    }

    //查看商品详情
    @RequestMapping("/detail.do")
    public ModelAndView goodsDetail(String number, HttpServletRequest request) {
        TbUser tbUser = (TbUser) request.getSession().getAttribute("user");
        TbGoods goods = goodsService.selectOne(number);
        if (tbUser.getRole() == 0) {
            return new ModelAndView("/user/product-detail", "goods", goods);
        } else {
            return new ModelAndView("/admin/product-detail", "goods", goods);
        }
    }

    //编辑商品
    @RequestMapping(value = "/goodsEdit", method = RequestMethod.GET)
    public ModelAndView goodsEdit(String number) {
        TbGoods goods = goodsService.selectOne(number);
        return new ModelAndView("/admin/product-edit", "goods", goods);
    }

    //编辑商品
    @RequestMapping(value = "goodsEdit", method = RequestMethod.POST)
    public ModelAndView goodsEditSubmit(String number, Double price, int count, String describe) {
        TbGoods tbGoods = goodsService.selectOne(number);
        tbGoods.setPrice(price);
        tbGoods.setCount(count);
        tbGoods.setDescribe(describe);
        goodsService.updateOne(tbGoods);
        return new ModelAndView("/admin/product-edit", "goods", tbGoods);
    }

    //查询获取商品编号
    public String selectGoodsNumber() {
        StringBuilder s = new StringBuilder();
        Integer integer = goodsService.selectGoodsNumber() + 1;
        String sTmp = String.valueOf(integer);
        s.append("0".repeat(Math.max(0, 8 - sTmp.length())));
        s.append(sTmp);
        return s.toString();
    }

    //添加商品
    @RequestMapping(value = "/productAdd", method = RequestMethod.GET)
    public ModelAndView productAdd() {
        List<TbSupplier> list = supplierService.selectAllSupplier();
        ModelAndView modelAndView = new ModelAndView("admin/product-add");
        modelAndView.addObject("number", selectGoodsNumber());
        modelAndView.addObject("supplierList", list);
        return modelAndView;
    }

    @RequestMapping(value = "/productAdd", method = RequestMethod.POST)
    public ModelAndView productAddSubmit(MultipartFile index, String number, Integer count, Double price, String name, String size, String brand, String texture, String describe) {

        List<String> sizeSplit = Arrays.asList(size.split(",").clone());

        TbGoods tbGoods = TbGoods.QueryBuild()
                .number(number)
                .count(count)
                .price(price)
                .name(name)
                .size(sizeSplit)
                .brand(brand)
                .texture(texture)
                .describe(describe);

        //插入图片
        try {
            String imagePath = Image.imageSave(index, number);
            if (imagePath == null) {
                return new ModelAndView("/admin/product-add", "msg", "插入图片错误");
            }
        } catch (Exception e) {
            e.printStackTrace();
        }

        Integer integer = goodsService.insertOne(tbGoods);
        return new ModelAndView("redirect:/Goods/detail.do", "number", number);
    }

}
