package com.colouredglaze.controller;

import com.colouredglaze.entity.*;
import com.colouredglaze.service.OrderService;
import lombok.RequiredArgsConstructor;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.servlet.ModelAndView;

import javax.servlet.http.HttpServletRequest;
import java.text.SimpleDateFormat;
import java.time.LocalDateTime;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;

import static com.colouredglaze.utils.DateConvert.convertDateToLDT;

/**
 * @author ZhangLin
 * @version 1.0
 * @date 2020/11/28 21:20
 */
@Controller
@RequiredArgsConstructor
@RequestMapping("/Order")
public class OrderController {

    private final  OrderService orderService;

    //查询个人或状态所有订单
    @RequestMapping("/")
    public ModelAndView selectOrder(HttpServletRequest request, String statu) {
        TbUser user = (TbUser) request.getSession().getAttribute("user");
        if (user.getRole() == 0) {
            String uid = user.getUid();
            List<OrderAndAddress> orderAndAddressList = orderService.selectOrderAndAddress(uid);
            return new ModelAndView("/user/order", "orderAndAddressList", orderAndAddressList);
        } else if (user.getRole() == 1) {
            List<TbOrder> orderList = orderService.selectOrder(statu);
            ModelAndView modelAndView = new ModelAndView("/admin/order-list");
            modelAndView.addObject("orderList", orderList);
            modelAndView.addObject("statu", statu);
            return modelAndView;
        } else {
            return new ModelAndView("/login", "msg", "请勿进行非法操作");
        }

    }

    //删除某条订单及其属性,外键会自动删除其下的订单商品条目
    @RequestMapping("/deleteOrder")
    public ModelAndView deleteOrder(String oid) {
        Integer integer = 0;
        integer = orderService.deleteOrder(oid);
        return new ModelAndView("redirect:/Order/");
    }

    //未处理
    @RequestMapping(value = "/orderProcess", method = RequestMethod.GET)
    public ModelAndView orderProcess(String oid) {
        return getOrderInfo(oid, "admin/order-process");
    }

    //处理某条订单
    @RequestMapping(value = "/orderProcess", method = RequestMethod.POST)
    public ModelAndView orderProcessSubmit(String oid, String statu) {
        TbOrder order = orderService.selectOneOrder(oid);
        LocalDateTime timeTmp = convertDateToLDT(new Date());
        if (statu.equals("已发货")) {
            order.setDeliver(timeTmp);
        } else if (statu.equals("已送达")) {
            order.setHandover(timeTmp);
        }
        order.setStatu(statu);
        Integer integer = orderService.updateOrderStatu(order);
        return getOrderInfo(oid, "admin/order-process");
    }

    //查询某条订单详细记录
    @RequestMapping("/selectOrderDetail")
    public ModelAndView selectOrderDetail(String oid, HttpServletRequest request) {
        TbUser tbUser = (TbUser) request.getSession().getAttribute("user");
        Integer role = tbUser.getRole();
        if (role == 0) {
            return getOrderInfo(oid, "user/order-detail");
        } else if (role == 1) {
            return getOrderInfo(oid, "admin/order-detail");
        } else {
            return new ModelAndView("/login", "msg", "请勿进行非法操作");
        }
    }

    //获取详细信息
    private ModelAndView getOrderInfo(String oid, String viewName) {
        List<Object> list = orderService.selectOrderDetail(oid);
        return new ModelAndView(viewName, "list", list);
    }

    //插入订单及其记录
    @RequestMapping("/createOrderAndOrderItem")
    public ModelAndView createOrderAndOrderItem(Integer address_id, HttpServletRequest request) {
        @SuppressWarnings("unchecked")
        List<CartItemAndGoods> cartItemAndGoods = (List<CartItemAndGoods>) request.getSession().getAttribute("cartItemAndGoodsArrayList");
        //uid用户id
        TbUser user = (TbUser) request.getSession().getAttribute("user");
        String uid = user.getUid();
        //oid订单id
        SimpleDateFormat dateFormat = new SimpleDateFormat("yyyyMMddHHmm");
        String oid = uid.substring(uid.length() - 4) + dateFormat.format(new Date());
        //statu订单状态
        String statu = "待发货";
        //payment支付总金额
        Double payment = 0.0;
        //placed下单时间
        LocalDateTime placed = convertDateToLDT(new Date());

        //OrderItem对象
        List<TbOrderItem> orderItemList = new ArrayList<>();
        for (CartItemAndGoods cta : cartItemAndGoods) {
            TbOrderItem orderItem = new TbOrderItem();
            orderItem.setNumber(cta.getNumber());
            orderItem.setOid(oid);
            orderItem.setPcount(cta.getPcount());
            orderItem.setPrice(cta.getPrice());
            orderItem.setSize(cta.getSize());
            orderItemList.add(orderItem);
            //计算总价
            payment += (cta.getPrice() * cta.getPcount());
        }

        //Order对象
        TbOrder tbOrder = TbOrder.QueryBuild().uid(uid).oid(oid).
                statu(statu).addressId(address_id).payment(payment).placed(placed);

        List<Object> list = new ArrayList<>();

        assert false;
        list.add(tbOrder);
        list.add(orderItemList);

        Integer integer = orderService.createOrderAndOrderItem(list);

        //清除购物车session
        request.getSession().removeAttribute("cartItemAndGoodsArrayList");

        return new ModelAndView("redirect:/Order/");
    }

}
