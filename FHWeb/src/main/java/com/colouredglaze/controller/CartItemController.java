package com.colouredglaze.controller;

import com.colouredglaze.entity.CartItemAndGoods;
import com.colouredglaze.entity.TbAddress;
import com.colouredglaze.entity.TbCartItem;
import com.colouredglaze.entity.TbUser;
import com.colouredglaze.service.AddressService;
import com.colouredglaze.service.CartItemService;
import lombok.RequiredArgsConstructor;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.servlet.ModelAndView;

import javax.servlet.http.HttpServletRequest;
import java.util.ArrayList;
import java.util.List;

/**
 * @author ZhangLin
 * @version 1.0
 * @date 2020/11/26 18:35
 */

@Controller
@RequiredArgsConstructor
@RequestMapping("/Cart")
public class CartItemController {

    private final  CartItemService itemService;

    private final  AddressService addressService;

    //检测是否有此购物车信息
    private List<Integer> haveCount(String uid, String number){
        List<Integer> count;
        TbCartItem cartItem=TbCartItem.QueryBuild().uid(uid).number(number);
        try{
            count=itemService.selectCartCount(cartItem);
        }catch (Exception e){
            throw new RuntimeException("haveCount方法异常");
//            e.printStackTrace();
        }
        return count;
    }

    //添加到购物车
    @RequestMapping("/goodsAdd")
    public ModelAndView goodsAdd(String uid, String number, Integer pcount,String size){
        List<Integer> count=haveCount(uid,number);
        Integer add;
        if(count.get(0)!=0){
            CartItemAndGoods cartItemAndGoodsQueryResult = itemService.selectCartItemByGoods(uid,number);

            Integer pcountFlag = cartItemAndGoodsQueryResult.getPcount();

            String querySizeResult = cartItemAndGoodsQueryResult.getSize();
            String updateSize = querySizeResult+","+size+"*"+pcount;
            TbCartItem cartItem=TbCartItem.QueryBuild().uid(uid).number(number).size(updateSize).pcount(pcount+count.get(0)).rid(count.get(1));
            add=itemService.addGoodsCount(cartItem);
        }else{
            String updateSize = size+"*"+pcount;
            TbCartItem cartItem=TbCartItem.QueryBuild().uid(uid).number(number).size(updateSize).pcount(pcount+count.get(0));
            add=itemService.addCartItem(cartItem);
        }
        return new ModelAndView("redirect:/Cart/userCartItem","uid",uid);
    }

    //转到购物车页面
    @RequestMapping("/userCartItem")
    public ModelAndView forCartItem(HttpServletRequest request){
        TbUser user =(TbUser)request.getSession().getAttribute("user");
        String uid=user.getUid();
        List<CartItemAndGoods> cartItemList=itemService.selectCartItem(uid);
        return new ModelAndView("/user/cart","cartItemList",cartItemList);
    }

    //结算页面
    @RequestMapping("/forCart")
    public ModelAndView forCart(HttpServletRequest request){
        List<Object> allList= new ArrayList<>();
        TbUser user =(TbUser)request.getSession().getAttribute("user");
        String uid=user.getUid();
        //购物车列表
        List<CartItemAndGoods> cartItemList=itemService.selectCartItem(uid);
        //收货地址列表
        TbAddress address=TbAddress.QueryBuild().uid(uid);
        List<TbAddress> addressList=addressService.selectAllAddress(address);
        allList.add(cartItemList);
        allList.add(addressList);
        return new ModelAndView("/user/checkout","allList",allList);
    }

    //删除购物车中的商品
    @RequestMapping("/deleteCartItem")
    public ModelAndView changeCartItemGoodsPcount(Integer rid,HttpServletRequest request){
        Integer i=itemService.deleteCartItem(rid);
        TbUser user =(TbUser)request.getSession().getAttribute("user");
        String uid=user.getUid();
        return new ModelAndView("redirect:/Cart/userCartItem","uid",uid);
    }

}
