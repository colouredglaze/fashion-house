import org.junit.Test;
import org.springframework.lang.Nullable;

import java.io.*;
import java.net.HttpURLConnection;
import java.net.MalformedURLException;
import java.net.URL;
import java.nio.charset.StandardCharsets;



/**
 * @author ZhangLin
 * @version 1.0
 * @date 2020/3/3 22:13
 */
public class MyTest {

    /**
     * Http post请求
     * @param httpUrl 连接
     * @param param 参数
     * @return
     */
    /**
     * Http get请求
     * @param httpUrl 连接
     * @return 响应数据
     */
    public static String doGet(String httpUrl){
        //链接
        HttpURLConnection connection = null;
        InputStream is = null;
        BufferedReader br = null;
        StringBuffer result = new StringBuffer();
        try {
            //创建连接
            URL url = new URL(httpUrl);
            connection = (HttpURLConnection) url.openConnection();
            //设置请求方式
            connection.setRequestMethod("GET");
            //设置连接超时时间
            connection.setReadTimeout(15000);
            //开始连接
            connection.connect();
            //获取响应数据
            if (connection.getResponseCode() == 200) {
                //获取返回的数据
                is = connection.getInputStream();
                if (null != is) {
                    br = new BufferedReader(new InputStreamReader(is, "UTF-8"));
                    String temp = null;
                    while (null != (temp = br.readLine())) {
                        result.append(temp);
                    }
                }
            }
        } catch (IOException e) {
            e.printStackTrace();
        } finally {
            if (null != br) {
                try {
                    br.close();
                } catch (IOException e) {
                    e.printStackTrace();
                }
            }
            if (null != is) {
                try {
                    is.close();
                } catch (IOException e) {
                    e.printStackTrace();
                }
            }
            //关闭远程连接
            connection.disconnect();
        }
        return result.toString();
    }

    @Test
    public void testOrder(){
        //try {
        //    String resource="mybatis-config.xml";
        //    InputStream= Resources.getResourceAsStream(resource);
        //    SqlSessionFactory sqlSessionFactory=new SqlSessionFactoryBuilder().build(inputStream);
        //    SqlSession sqlSession=sqlSessionFactory.openSession();
        //    TbOrderBaseMapper orderBaseMapper=sqlSession.getMapper(TbOrderBaseMapper.class);
        //    List<TbOrder> tbOrders=orderBaseMapper.selectOrderAndAddress2();
        //    for (TbOrder tbOrder : tbOrders) {
        //        System.out.println(tbOrder);
        //    }
        //} catch (IOException e) {
        //    e.printStackTrace();
        //}

//        doPost("https://api.day.app/pDNw7sPLyxT6Bsdgg4pGUU/数据存储: 另一个我: [聊天记录]","数据存储: 另一个我: [聊天记录]");
//        String message = doGet("https://space.bilibili.com/589533168", "");
//        String message = doGet("http://localhost:8081/FHWeb_war/", "");
//        String message = doGet("https://api.day.app/pDNw7sPLyxT6Bsdgg4pGUU/", "");
        String message = doGet("http://192.168.101.4:8080/eHRTyjXTBNggsFDkZtw3mK/这是一个测试");
        System.out.println(message);
//        System.out.println(message2);

    }
}
