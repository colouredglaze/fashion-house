package com.colouredglaze.config;

import java.io.FileWriter;
import java.io.IOException;

/**
 * @author UNENCRYPTED
 */
public class AlipayConfig {
    /**
     * 应用ID,您的APPID，收款账号既是您的APPID对应支付宝账号
     */
    public static String app_id = "2016110300788749";

    /**
     * 商户私钥，您的PKCS8格式RSA2私钥
     */
    public static String merchant_private_key = "MIIEvgIBADANBgkqhkiG9w0BAQEFAASCBKgwggSkAgEAAoIBAQCngDnGb5kr7pPJ2Zlh1wGtif3QOk40MVM5/nHCZl3Yh/t59ViI5LwGupTnweeapgtZnpfu5H8e/Aag0/p/MqjQxRxzzdnTQtVUGa9lGECj0Nb4EjkebGieAr84XtyF+yfAjVDbPL7NGR/NmtZQCa9MXEcuoVBEvQjNS0S3DOTs+dAd5NJMgTCR4GFjnqPrUh/rNhlUvFBX4yK/RzFNkMnBN6/Kx/WzWSRW70oqvXuP84cXxf2CTvyvkBgzDWY7AJdEiGj1EEBUr/rV8Zzi6+CHTddsKfdBo4C04oL5NkPwqjT8SVwxeE8XBHuqk8DJBc6zoYUqi/LUl0eDkxC/mIYHAgMBAAECggEAQID6vtY/MFHiC/uGO81m0gOwAL5TW3obSNwZxvlQ7o//Ygn/nEgpuJWlvlJH9KA5Arw2WkMYSUBY9Rps/wrtQ+iEenwthFPaf1ac2tuqGOvVvRaXdIXfC+qW5Qhy+A2uU1KYbrVH7Qfd3XnhSlVQcqGer/BjTvVVLGnt/mgyyKcTeFQmFrBFbxHQvDzbiqdnOoZFjhMvUM77sah7dg8hV3jowyPg+Oc+SNKdleUzhFtMA+DMzJwbgldW6i7V99p9p6Di1rTLqcZRliPH+6myJJiMGa6jIxo8cvjD1Sr1CdKplQPUdXraZN+f52f5E3RpNSrX4pYzDYiAV0+bK4JgwQKBgQDe4jQEe7NOzlUQZfkS1hYs10tpwqKXtK1mwfxfd3AYB9BhT+M/ErImnNi2fHiFedPZGv2ps0TtkcIfGP9y52lWn71Ix/L87AopzBfsH3ssYF3JDsu7u5D8h4rsbwgaHRTRQZDIlBySektWxESn8zztMfpnFxC6EiH2/LwTSm74NwKBgQDAY2+9X/HZaY8sQDEaIC3z6QkN8RN6pBRGtFvzYqtM2hAXR2tLmzvIg4cVbizYnTSsOxh0Ibgo5X/6Q1INTph5huKXxX4hHkGGVmtehq8wcmCpVXLgUQx+4+lhsXKb89e70Xfyr5psqheSVjud4dcdlITiAzCrzyRfCKK/GtRYsQKBgQCBAlG1npxFKy0VltC61Gx1fBDMoLpYMV6FMheQcqPRZQR6rzEMGxg/PV47EoT4TXIQIo1dIsTs9gd01JVXvxa76y426HV/bGjdlKLsK7SyFP10GTpX6dOcOCBzhjkcVGTuUpMpZoG7T0o4aNaMmEBV0y3rafBmo4RBYDzEtbrRtQKBgE4NCVwu/JNTu/4b3M/YlkwKEt5RG7H5tzAoI9XOBekb3sNS+jimNc7XbYpINHF89c2Z/AXBWFFfKliBz1wOstU639rrCIyvFa5GiTDEO3bQMF8Ch6RxvTwNgrVbpD+Y4cEU7jDLBT9okj+QNIeNBBTAGFkJKknJs9vKSRx26gdBAoGBAIeXLJGsRiI0olVjEEkofxDpZ02Hll8ZsrO1fNycNjbUV/j6GeQTANBX0+8OVQx9qo9gQYXqReKCTeJmKunYH5P/w46A8hmKv0oAtWIdzPyyww7PvmObjiHNqJw9dwdXtdQk2nSof/I2PguISTt7QqliYV+UB3cK0/wckPxMPMeA";

    /**
     * 支付宝公钥,查看地址：https://openhome.alipay.com/platform/keyManage.htm 对应APPID下的支付宝公钥。
     */
    public static String alipay_public_key = "MIIBIjANBgkqhkiG9w0BAQEFAAOCAQ8AMIIBCgKCAQEAp4A5xm+ZK+6TydmZYdcBrYn90DpONDFTOf5xwmZd2If7efVYiOS8BrqU58HnmqYLWZ6X7uR/HvwGoNP6fzKo0MUcc83Z00LVVBmvZRhAo9DW+BI5HmxongK/OF7chfsnwI1Q2zy+zRkfzZrWUAmvTFxHLqFQRL0IzUtEtwzk7PnQHeTSTIEwkeBhY56j61If6zYZVLxQV+Miv0cxTZDJwTevysf1s1kkVu9KKr17j/OHF8X9gk78r5AYMw1mOwCXRIho9RBAVK/61fGc4uvgh03XbCn3QaOAtOKC+TZD8Ko0/ElcMXhPFwR7qpPAyQXOs6GFKovy1JdHg5MQv5iGBwIDAQAB";

    /**
     * 服务器异步通知页面路径  需http://格式的完整路径，不能加?id=123这类自定义参数，必须外网可以正常访问
     */
    public static String notify_url = "http://payservice.test.utools.club/FHWeb_war_exploded/pay/notify_url";

    /**
     * 页面跳转同步通知页面路径 需http://格式的完整路径，不能加?id=123这类自定义参数，必须外网可以正常访问
     */
    public static String return_url = "http://payservice.test.utools.club/FHWeb_war_exploded/pay/return_url";

    /**
     * 签名方式
     */
    public static String sign_type = "RSA2";

    /**
     * 字符编码格式
     */
    public static String charset = "utf-8";

    /**
     * 支付宝网关
     */
    public static String gatewayUrl = "https://openapi.alipaydev.com/gateway.do";

    /**
     * 支付宝网关
     */
    public static String log_path = "C:\\";

    /**
     * 写日志，方便测试（看网站需求，也可以改成把记录存入数据库）
     * @param sWord 要写入日志里的文本内容
     */
    public static void logResult(String sWord) {
        FileWriter writer = null;
        try {
            writer = new FileWriter(log_path + "alipay_log_" + System.currentTimeMillis()+".txt");
            writer.write(sWord);
        } catch (Exception e) {
            e.printStackTrace();
        } finally {
            if (writer != null) {
                try {
                    writer.close();
                } catch (IOException e) {
                    e.printStackTrace();
                }
            }
        }
    }
}
