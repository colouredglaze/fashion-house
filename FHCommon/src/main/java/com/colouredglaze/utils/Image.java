package com.colouredglaze.utils;

import org.springframework.web.multipart.MultipartFile;

import java.io.File;
import java.io.IOException;

/**
 * @version 1.0
 * @date 2020/12/2 20:42
 */
public class Image {

    //保存图片，并且返回图片路径
    public static String imageSave(MultipartFile image, String num) throws IllegalStateException, IOException {
        //保存数据库的路径（这里就是在自己tomcat的webapps下创建的路径，绝对路径）
        String sqlPath = null;
        //定义文件保存的本地路径（我的台式机绝对路径）
//        String localPath="D:\\TomCat\\apache-tomcat-9.0.39\\webapps\\img\\user\\product\\";
        //定义文件保存的本地路径（我的笔记本绝对路径）
        String localPath = "E:\\WorkSoft\\tomcat\\apache-tomcat-9.0.87-windows-x64\\apache-tomcat-9.0.87\\webapps\\img\\user\\product\\";
        String sliderPath = "E:\\WorkSoft\\tomcat\\apache-tomcat-9.0.87-windows-x64\\apache-tomcat-9.0.87\\webapps\\img\\user\\slider\\";
        //定义 文件名
        String filename = null;
        if (!image.isEmpty()) {
            //生成uuid作为文件名称
            //String uuid = UUID.randomUUID().toString().replaceAll("-","");
            //获得文件类型（可以判断如果不是图片，禁止上传）
            //String contentType=image.getContentType();
            //获得文件后缀名
            //assert contentType != null;
            //String suffixName=contentType.substring(contentType.indexOf("/")+1);
            String suffixName = "jpg";
            //得到 文件名
            filename = num + "." + suffixName;
            System.out.println(filename);
            sqlPath = localPath + filename;
            System.out.println(sqlPath);
            //文件保存路径
            image.transferTo(new File(sqlPath));
//            sqlPath = sliderPath + filename;
//            System.out.println(sqlPath);
//            image.transferTo(new File(sqlPath));
        }
        //把图片的相对路径保存至数据库
        return filename;
    }

}
