package com.colouredglaze.utils;

import com.aliyuncs.CommonRequest;
import com.aliyuncs.CommonResponse;
import com.aliyuncs.DefaultAcsClient;
import com.aliyuncs.IAcsClient;
import com.aliyuncs.exceptions.ClientException;
import com.aliyuncs.http.MethodType;
import com.aliyuncs.profile.DefaultProfile;
import org.json.JSONObject;

/**
 * @author ZhangLin
 * @version 1.0
 * @date 2020/11/30 22:18
 */
public class AliyunSms {
    public static JSONObject sendSmsUtil(String uid, String code) {
        DefaultProfile profile = DefaultProfile.getProfile("cn-hangzhou", "LTAI4FdK7L9uMCj8NCGXZ2tY179", "stmK3DkKzUP16zfhD8B6G4IEKLqEnZ179");
        IAcsClient client = new DefaultAcsClient(profile);

        CommonRequest request = new CommonRequest();
        request.setMethod(MethodType.POST);
        request.setDomain("dysmsapi.aliyuncs.com");
        request.setVersion("2017-05-25");
        request.setAction("SendSms");
        request.putQueryParameter("RegionId", "cn-hangzhou");
        request.putQueryParameter("PhoneNumbers", uid);
        request.putQueryParameter("SignName", "时装邮购系统");
        request.putQueryParameter("TemplateCode", "SMS_179215578");
        request.putQueryParameter("TemplateParam", "{\"code\":\""+code+"\"}");
        try {
            CommonResponse response = client.getCommonResponse(request);
            return new JSONObject(response.getData());
        } catch (ClientException e) {
            e.printStackTrace();
        }
        return null;
    }
}
