package com.colouredglaze.service;

import com.colouredglaze.mapper.TbSupplierMapper;
import com.colouredglaze.entity.TbSupplier;
import lombok.AllArgsConstructor;
import org.springframework.stereotype.Service;

import java.util.List;

/**
 * @author ZhangLin
 * @version 1.0
 * @date 2020/12/2 17:36
 */
@Service
@AllArgsConstructor
public class SupplierService {

    final private TbSupplierMapper supplierMapper;

    public List<TbSupplier> selectAllSupplier() {
        List<TbSupplier> list = null;
        try {
            list = supplierMapper.queryTbSupplier(null);
        } catch (Exception e) {
            e.printStackTrace();
        }
        return list;
    }

}
