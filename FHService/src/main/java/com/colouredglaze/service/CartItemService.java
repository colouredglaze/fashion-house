package com.colouredglaze.service;

import com.colouredglaze.mapper.TbCartItemMapper;
import com.colouredglaze.entity.CartItemAndGoods;
import com.colouredglaze.entity.TbCartItem;
import lombok.AllArgsConstructor;
import org.springframework.stereotype.Service;

import java.util.ArrayList;
import java.util.List;

/**
 * @author ZhangLin
 * @version 1.0
 * @date 2020/11/26 18:48
 */
@Service
@AllArgsConstructor
public class CartItemService {

    final private TbCartItemMapper cartItemMapper;

    //查询是否存在购物车的商品数量
    public List<Integer> selectCartCount(TbCartItem cartItem){
        List<Integer> count=new ArrayList<>();
        try{
            List<TbCartItem> list=cartItemMapper.queryTbCartItem(cartItem);
            if(list!=null && list.size()>1){
                throw new RuntimeException("selectCartCount方法查询出了多条数据");
            }
            if (list!=null && list.size()==1) {
                count.add(list.get(0).getPcount());
                count.add(list.get(0).getRid());
            }else {
                count.add(0);
            }
        }catch (Exception e){
            throw new RuntimeException("selectCartCount方法异常");
        }
        return count;
    }

    //修改购物记录商品数量
    public Integer addGoodsCount(TbCartItem cartItem){
        int i=0;
        try {
            i=cartItemMapper.updateTbCartItem(cartItem);
        }catch (Exception e){
            throw new RuntimeException("addGoodsCount方法错误");
        }
        return i;
    }

    //增加购物车
    public Integer addCartItem(TbCartItem cartItem){
        int i=0;
        try{
            i=cartItemMapper.insertTbCartItem(cartItem);
        }catch (Exception e){
            throw new RuntimeException("addCartItem方法错误");
        }
        return i;
    }

    //查询当前角色的购物车
    public List<CartItemAndGoods> selectCartItem(String uid){
        List<CartItemAndGoods> list=null;
        try{
            list=cartItemMapper.queryCartItemAndGoods(uid);
        }catch (Exception e){
            throw new RuntimeException("selectCartItem方法错误");
        }
        return list;
    }

    //查询当前角色的购物车
    public CartItemAndGoods selectCartItemByGoods(String uid,String number){
        CartItemAndGoods cartItemAndGoods=null;
        try{
            cartItemAndGoods = cartItemMapper.queryCartItemAndGoodsByGoods(uid,number);
        }catch (Exception e){
            throw new RuntimeException("selectCartItem方法错误");
        }
        return cartItemAndGoods;
    }

    //删除一项
    public Integer deleteCartItem(Integer rid){
        Integer i = null;
        try{
            i=cartItemMapper.deleteCartItem(rid);
        }catch (Exception e){
//            throw new RuntimeException("deleteCartItem方法错误");
            e.printStackTrace();
        }
        return i;
    }

}
