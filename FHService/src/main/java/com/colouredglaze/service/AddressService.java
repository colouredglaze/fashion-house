package com.colouredglaze.service;

import com.colouredglaze.mapper.TbAddressMapper;
import com.colouredglaze.entity.TbAddress;
import lombok.AllArgsConstructor;
import org.springframework.stereotype.Service;

import java.util.List;

/**
 * @author ZhangLin
 * @version 1.0
 * @date 2020/11/27 19:32
 */
@Service
@AllArgsConstructor
public class AddressService {

    final private TbAddressMapper addressMapper;

    //添加一条地址
    public Integer insertAddress(TbAddress address){
        Integer integer = 0;
        try{
            integer=addressMapper.insertTbAddress(address);
        }catch (Exception e){
            throw new RuntimeException("insertAddress方法错误");
        }
        return integer;
    }

    //更行一条地址
    public Integer updateAddress(TbAddress address){
        Integer integer=0;
        try{
            integer=addressMapper.updateTbAddress(address);
        }catch (Exception e){
            throw new RuntimeException("upadteAddress方法错误");
        }
        return integer;
    }

    //查询所有地址
    public List<TbAddress> selectAllAddress(TbAddress address){
        List<TbAddress> list=null;
        try{
            list=addressMapper.queryTbAddress(address);
        }catch (Exception e){
            throw new RuntimeException("selectAllAddress方法错误");
        }
        return list;
    }

    //删除一条地址
    public Integer deleteAddress(Integer rid){
        Integer integer = 0;
        try{
            integer=addressMapper.deleteAddress(rid);
        }catch (Exception e){
            throw new RuntimeException("deleteAddress方法错误");
        }
        return integer;
    }




}
