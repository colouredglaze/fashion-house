package com.colouredglaze.service;

import com.colouredglaze.mapper.TbCartItemMapper;
import com.colouredglaze.mapper.TbOrderItemMapper;
import com.colouredglaze.mapper.TbOrderMapper;
import com.colouredglaze.entity.*;
import lombok.AllArgsConstructor;
import org.springframework.stereotype.Service;

import java.util.ArrayList;
import java.util.List;

/**
 * @author ZhangLin
 * @version 1.0
 * @date 2020/11/28 22:38
 */
@Service
@AllArgsConstructor
public class OrderService {

    final private TbOrderMapper orderMapper;

    final private TbOrderItemMapper orderItemMapper;

    final private TbCartItemMapper cartItemMapper;

    //查询用户订单
    public List<OrderAndAddress> selectOrderAndAddress(String uid){
        List<OrderAndAddress> list=null;
        try{
            list=orderMapper.selectOrderAndAddress(uid);
        }catch (Exception e){
            throw new RuntimeException("selectOrderAndAddress方法错误");
        }
        return list;
    }

    //查询单条订单
    public TbOrder selectOneOrder(String oid) {
        List<TbOrder> list = null;
        TbOrder tbOrder = TbOrder.QueryBuild().oid(oid);
        try {
            list = orderMapper.queryTbOrder(tbOrder);
        } catch (Exception e) {
            throw new RuntimeException("selectOneOrder方法错误");
        }
        return list.get(0);
    }

    //查询所有订单
    public List<TbOrder> selectOrder(String statu){
        List<TbOrder> list=null;
        TbOrder tbOrder=TbOrder.QueryBuild().statu(statu);
        try{
            list=orderMapper.queryTbOrder(tbOrder);
        }catch (Exception e){
            throw new RuntimeException("selectOrder方法错误");
        }
        return list;
    }

    //更新订单状态
    public Integer updateOrderStatu(TbOrder order) {
        Integer integer = 0;
        try {
            integer = orderMapper.updateTbOrder(order);
        } catch (Exception e) {
            throw new RuntimeException("updateOneOrder方法错误");
        }
        return integer;
    }

    //删除某条订单
    public Integer deleteOrder(String oid){
        Integer integer=0;
        try{
            integer=orderMapper.deleteOrder(oid);
        }catch (Exception e){
            throw new RuntimeException("deleteOrder方法错误");
        }
        return integer;
    }

    //查询详细订单
    public List<Object> selectOrderDetail(String oid){
        List<Object> list=new ArrayList<>();
        OrderAndUserAndAddress orderAndUserAndAddress=null;
        List<GoodsAndOrderItem> orderAndAddressList=null;
        try{
            orderAndUserAndAddress=orderMapper.selectOrderAndUserAndAddress(oid);
            orderAndAddressList=orderItemMapper.selectOrderItemAndGoods(oid);
            list.add(orderAndUserAndAddress);
            list.add(orderAndAddressList);
        }catch (Exception e){
            throw new RuntimeException("selectOrderDetail方法错误");
        }
        return list;
    }

    //生成订单及订单项
    public Integer createOrderAndOrderItem(List<Object> list){
        Integer integer=0;
        TbOrder tbOrder= (TbOrder) list.get(0);
        @SuppressWarnings("unchecked")
        List<TbOrderItem> orderItemList= (List<TbOrderItem>) list.get(1);
        try{
            //插入订单
            integer+=orderMapper.insertTbOrder(tbOrder);
            for(TbOrderItem tbOrderItem:orderItemList){
                //插入订单对应商品项
                integer+=orderItemMapper.insertTbOrderItem(tbOrderItem);
            }
            //删除购物车
            integer+=cartItemMapper.deleteUserCartItem(tbOrder.getUid());
        }catch (Exception e){
            throw new RuntimeException("createOrderAndOrderItem方法错误");
        }
        return integer;
    }



}
