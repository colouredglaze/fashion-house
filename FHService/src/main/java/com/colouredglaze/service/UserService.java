package com.colouredglaze.service;

import com.colouredglaze.mapper.TbUserMapper;
import com.colouredglaze.entity.TbUser;
import lombok.AllArgsConstructor;
import org.springframework.stereotype.Service;

import java.util.List;

/**
 * @author ZhangLin
 * @version 1.0
 * @date 2020/11/24 18:59
 */
@Service
@AllArgsConstructor
public class UserService {

    final private TbUserMapper userMapper;

    public TbUser selectOne(TbUser Entity){
        List<TbUser> list=userMapper.queryTbUser(Entity);
        if(list!=null && list.size()>1){
            throw new RuntimeException("selectOne方法查询出了多条数据");
        }
        if(list==null){
            return null;
        }
        if(list.size()==0){
            return null;
        }
        return list.get(0);
    }

    public int insertOne(TbUser Entity){
        int fl=userMapper.insertTbUser(Entity);
        if(fl>1){
            throw new RuntimeException("insertOne方法插入了多条数据");
        }
        return fl;
    }

    //更新用户数据
    public int updateUser(TbUser user){
        int i=0;
        try{
            i=userMapper.updateTbUser(user);
        }catch (Exception e){
            throw new RuntimeException("updateUser方法出错");
        }
        return i;
    }


}
