package com.colouredglaze.service;

import com.colouredglaze.mapper.TbRecommendedMapper;
import com.colouredglaze.entity.TbRecommended;
import lombok.AllArgsConstructor;
import org.springframework.stereotype.Service;

import java.util.List;

/**
 * @author ZhangLin
 * @version 1.0
 * @date 2020/11/29 19:42
 */
@Service
@AllArgsConstructor
public class RecommendedService {

    final private TbRecommendedMapper tbRecommendedMapper;

    //查询推荐商品
    public List<TbRecommended> selectRe(){
        List<TbRecommended> tbRecommendedList=null;
        try {
            tbRecommendedList=tbRecommendedMapper.queryTbRecommended(null);
        }catch (Exception e){
            throw new RuntimeException("selectRe方法错误");
        }
        return tbRecommendedList;
    }


}
