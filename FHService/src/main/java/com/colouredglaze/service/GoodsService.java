package com.colouredglaze.service;

import com.colouredglaze.entity.TbSize;
import com.colouredglaze.mapper.TbGoodsMapper;
import com.colouredglaze.entity.TbGoods;
import lombok.AllArgsConstructor;
import org.springframework.stereotype.Service;

import java.util.List;

/**
 * @author ZhangLin
 * @version 1.0
 * @date 2020/11/25 12:50
 */
@Service
@AllArgsConstructor
public class GoodsService {

    final private TbGoodsMapper goodsMapper;

    //全部商品查询
    public List<TbGoods> selectList(){
        return goodsMapper.queryTbGoods(null);
    }

    //插入单个商品
    public Integer insertOne(TbGoods tbGoods) {
        Integer integer = 0;
        try {
            integer = goodsMapper.insertTbGoods(tbGoods);
            for (String s : tbGoods.getSize()) {
                goodsMapper.insertTbSize(new TbSize()
                        .setSize(s)
                        .setGoods(tbGoods.getNumber()));
            }
        } catch (Exception e) {
            e.printStackTrace();
        }
        return integer;
    }

    //单个商品查询
    public TbGoods selectOne(String number){
        if(number == null||number.equals("")){
            throw new RuntimeException("根据number查询时，number不能为null");
        }
        try{
//            TbGoods tbGoods=TbGoods.QueryBuild().number(number);
            return goodsMapper.queryTbGoodsLimit(number);
        }catch (Exception e){
            throw new RuntimeException(e);
        }
    }

    //更新商品信息
    public Integer updateOne(TbGoods goods) {
        Integer integer = 0;
        try {
            integer = goodsMapper.updateTbGoods(goods);
        } catch (Exception e) {
            throw new RuntimeException("updateOne方法错误");
        }
        return integer;
    }

    //获取商品编号
    public Integer selectGoodsNumber() {
        Integer integer = 0;
        try {
            integer = goodsMapper.selectGoodsNumber();
        } catch (Exception e) {
            e.printStackTrace();
            //throw new RuntimeException("selectGoodsNumber方法错误");
        }
        return integer;
    }


}

